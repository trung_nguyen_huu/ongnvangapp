app.controller('RefundRequestsCtrl', ['$scope', 'refundRequestsService', '$log', '$state',
'$uibModal', '$window', '$httpParamSerializerJQLike',
function($scope, refundRequestsService, $log, $state, $uibModal, $window, $httpParamSerializerJQLike){

  $scope.refund_requests = refundRequestsService.refund_requests;
  $scope.list_permissions = refundRequestsService.list_permissions;
  $scope.pageConfig = refundRequestsService.pageConfig;

  $scope.pageChanged = function() {
    refundRequestsService.index(getParams());
  };

  $scope.q = {};
  $scope.search = function(invalid){
    if(invalid){
      return;
    }

    $scope.pageConfig.currentPage = 1;
    refundRequestsService.index(getParams());
  };

  $scope.loadController = true;
  $scope.$watch('q', function(){
    if($scope.loadController == true){
      $scope.loadController = false;
      return;
    }

    $scope.pageConfig.currentPage = 1;
    refundRequestsService.index(getParams());
  }, true);

  $scope.onChoseFilterByDay = function(filterByDay){
    if(filterByDay == ""){
      $scope.q.created_at_gteq = null;
      $scope.q.created_at_lteq = null;
    }

    if(filterByDay == 'in_today'){
      $scope.q.created_at_gteq = new Date();
      $scope.q.created_at_lteq = new Date();
    }

    if(filterByDay == 'in_this_week'){
      $scope.q.created_at_gteq = Date.today().last().monday();
      $scope.q.created_at_lteq = Date.today().next().sunday();
    }

    if(filterByDay == 'in_this_month'){
      $scope.q.created_at_gteq = Date.today().clearTime().moveToFirstDayOfMonth();
      $scope.q.created_at_lteq = Date.today().clearTime().moveToLastDayOfMonth();
    }

  };

  $scope.clearSearch = function(){
    angular.copy({}, $scope.q);
    $scope.filterByDay = '';
    $scope.search();
  };

  $scope.accept = function(refund_request){
    var are_use_sure = $window.confirm('Ban chac chap nhan yeu cau nay ko?');
    if (are_use_sure) {
      refundRequestsService.accept(refund_request);
    }
  };

  $scope.deny = function(refund_request){
    var are_use_sure = $window.confirm('Ban chac tu choi yeu cau nay ko?');
    if (are_use_sure) {
      refundRequestsService.deny(refund_request);
    }
  };

  function getParams(){
    return {
      page: $scope.pageConfig.currentPage,
      q: $scope.q
    };
  };

}]);
