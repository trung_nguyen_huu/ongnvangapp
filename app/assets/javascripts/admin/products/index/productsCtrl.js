app.controller('ProductsCtrl', ['$scope', 'productsService', '$log', '$state', '$window', '$httpParamSerializerJQLike',
function($scope, productsService, $log, $state, $window, $httpParamSerializerJQLike){
  $scope.products = productsService.products;
  $scope.allProducts = productsService.allProducts;
  $scope.pageConfig = productsService.pageConfig;
  $scope.list_permissions = productsService.list_permissions;

  $scope.pageChanged = function() {
    productsService.getListProducts(getParams());
  };

  $scope.q = {};
  $scope.search = function(){
    $scope.pageConfig.currentPage = 1;
    productsService.getListProducts(getParams());
  };

  $scope.onSelectProductName = function(item, model){
    $scope.search();
    // $state.go("editProduct", { id: item.id })
  };

  $scope.get_excel = function(){
    var q = {
      q: $scope.q,
    };
    $window.open('admin/products/get_excel.xlsx?' + $httpParamSerializerJQLike(q), '_blank'); 
    angular.copy({}, $scope.q);
  };

  $scope.checkOnOutOfStock = function(){
    if($scope.q.out_of_stock){
      $scope.q.variants_count_in_stock_eq = 0;
    }else{
      $scope.q.variants_count_in_stock_eq = undefined;
    }
  };


  $scope.clearSearch = function(autocompleteId){
    $scope.selectedProduct = undefined;
    $scope.$broadcast('angucomplete-alt:clearInput', autocompleteId);

    angular.copy({}, $scope.q);
    $scope.search();
  };

  function getParams(){
    return {
      page: $scope.pageConfig.currentPage,
      q: $scope.q
    };
  };

  $scope.delete = function(product){
    productsService.delete(product);
  };

}]);
