app.controller('newProductCtrl', ['$scope', 'productsService', '$log', '$state', 'Notification',
function($scope, productsService, $log, $state, Notification){
  $scope.product = {
    variants_attributes: [getNewVariant()],
  };

  $scope.newVariant = function(){
    $scope.product.variants_attributes.push(getNewVariant());
  };

  $scope.removeVariant = function(variant){
    var index = $scope.product.variants_attributes.indexOf(variant);
    $scope.product.variants_attributes.splice(index, 1);
  };

  $scope.create = function(invalid){
    $scope.product.isCreating = true;
    console.log("product: ", $scope.product);
    if(invalid){
      return;
    }

    productsService.create($scope.product).success(function(){
      $state.go('listingProducts');
      Notification.success("Them moi thanh cong");
    }).finally(function(){
      $scope.product.isCreating = false;
    });
  };

  $scope.cancel = function(){
    $state.go('listingProducts');
  };

  function getNewVariant(){
    var yesterday = new Date();
    yesterday.setDate(yesterday.getDate() - 1);
    return {
      price: 0,
      discount_amount: 0,
      sale_from_date: new Date(),
      sale_to_date: yesterday,
    };
  };

}]);
