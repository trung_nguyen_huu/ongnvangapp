app.controller('editProductCtrl', ['$scope', 'productsService', '$log', '$state', 'Notification',
function($scope, productsService, $log, $state, Notification){
  $scope.product = productsService.product; 
  _.each($scope.product.variants_attributes, function(variant){
    if(variant.sale_from_date){
      variant.sale_from_date = new Date(variant.sale_from_date);
    }

    if(variant.sale_to_date){
      variant.sale_to_date = new Date(variant.sale_to_date);
    }
  });

  $scope.newVariant = function(){
    $scope.product.variants_attributes.push(getNewVariant());
  };

  $scope.removeVariant = function(variant){
    if(!variant.id){
      var index = $scope.product.variants_attributes.indexOf(variant);
      $scope.product.variants_attributes.splice(index, 1);
    }
    variant._destroy = true;
  };

  $scope.update = function(invalid){
    console.log("product: ", $scope.product);
    $scope.product.isCreating = true;
    if(invalid){
      return;
    }

    productsService.update($scope.product).success(function(){
      $state.go('listingProducts');
      Notification.success("Cap nhat thanh cong");
    }).finally(function(){
      $scope.product.isCreating = false;
    });
  };

  $scope.cancel = function(){
    $state.go('listingProducts');
  };

  $scope.formValid = function(valid){
    undeleted_variants = $scope.product.variants_attributes.filter(function(variant){
      return !variant._destroy;
    });

    if(valid && undeleted_variants.length >= 1){
      return true;
    }

    return false;
  }
  function getNewVariant(){
    var yesterday = new Date();
    yesterday.setDate(yesterday.getDate() - 1);
    return {
      price: 0,
      discount_amount: 0,
      sale_from_date: new Date(),
      sale_to_date: yesterday,
    };
  };

}]);
