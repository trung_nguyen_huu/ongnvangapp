app.controller('profileCtrl', ['$scope', 'Notification', 'usersService', function($scope, Notification, usersService){
  $scope.user = usersService.currentUser;
  $scope.update_info = function(invalid){
    if(invalid){
      return;
    }

    $scope.user.isUpdating = true;
    usersService.update_info($scope.user).success(function(){
      Notification.success("Cap nhat thong tin thanh cong");
      $scope.user.current_password = null;
      $scope.user.password = null;
      $scope.user.password_confirmation = null;
      $scope.userPasswordForm.$setPristine()
    })
    .error(function(errors){
      angular.forEach(errors, function(error){
        Notification.error(error);
      });
    })
    .finally(function(){
      $scope.user.isUpdating = false;
    });
  }
}]);
