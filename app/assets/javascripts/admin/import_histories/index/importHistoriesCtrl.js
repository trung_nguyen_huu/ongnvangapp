app.controller('ImportHistoriesCtrl', ['$scope', 'importHistoriesService', '$log', '$state', '$window', '$httpParamSerializerJQLike',
function($scope, importHistoriesService, $log, $state, $window, $httpParamSerializerJQLike){
  $scope.dateOptions = {
    formatYear: 'yy',
    startingDay: 1
  };


  $scope.import_histories = importHistoriesService.import_histories;
  $scope.list_permissions = importHistoriesService.list_permissions;
  $scope.pageConfig = importHistoriesService.pageConfig;

  $scope.pageChanged = function() {
    importHistoriesService.index(getParams());
  };

  $scope.q = {};
  $scope.search = function(invalid){
    console.log("in search");
    if(invalid){
      return;
    }

    $scope.pageConfig.currentPage = 1;
    importHistoriesService.index(getParams());
  };

  $scope.clearSearch = function(){
    angular.copy({}, $scope.q);
    $scope.search();
  };

  $scope.delete = function(import_history){
    importHistoriesService.destroy(import_history);
  };


  $scope.get_excel = function(){
    var q = {
      q: $scope.q,
    };
    $window.open('admin/import_histories/get_excel.xlsx?' + $httpParamSerializerJQLike(q), '_blank'); 
    angular.copy({}, $scope.q);
  };

  function getParams(){
    return {
      page: $scope.pageConfig.currentPage,
      q: $scope.q
    };
  };

}]);
