app.controller('otherImportItemModalCtrl', ['$scope', '$uibModalInstance',
'other_import_item', 'otherImportItemService', 'import_history', 'importHistoriesService', 'allOtherItems',
function ($scope, $uibModalInstance, other_import_item, otherImportItemService, import_history, importHistoriesService, allOtherItems) {
  $scope.moneyOptions = {
    prefix: '',
    suffix: '₫',
    precision: 2,
    allowNegative: false,
  };

  $scope.other_import_item = {};
  $scope.import_history = {};
  $scope.allOtherItems = allOtherItems;

  angular.copy(other_import_item, $scope.other_import_item);
  angular.copy(import_history, $scope.import_history);

  $scope.save = function (invalid) {
    if(invalid){
      return;
    }

    if(!$scope.import_history.id){
      promise = importHistoriesService.create($scope.import_history);
      promise.then(function(){
        saveOtherImportItemAndCloseModal(true);
      });
    }
    else{
      saveOtherImportItemAndCloseModal(false);
    }

  };

  function saveOtherImportItemAndCloseModal(isNewImportHistory){
    otherImportItemService.save($scope.import_history, $scope.other_import_item).success(function(){
      result = {
        import_history: $scope.import_history,
        other_import_item: $scope.other_import_item,
        isNewImportHistory: isNewImportHistory,
      };

      $uibModalInstance.close(result);
    });
  }

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };

}]);
