app.controller('importItemModalCtrl', ['$scope', '$uibModalInstance',
'import_item', 'import_history', 'importHistoriesService', 'importItemsService', 'allVariants', 'allSuppliers', 'currentUser',
function ($scope, $uibModalInstance, import_item, import_history, importHistoriesService, importItemsService, allVariants, allSuppliers, currentUser) {

  function initData(){
    $scope.moneyOptions = {
      prefix: '',
      suffix: '₫',
      precision: 2,
      allowNegative: false,
    };

    $scope.import_item = {};
    $scope.import_history = {};
    $scope.allVariants = [];
    $scope.allSuppliers = [];
    $scope.currentUser = currentUser;
    angular.copy(import_item, $scope.import_item);
    angular.copy(import_history, $scope.import_history);

    angular.copy(allVariants, $scope.allVariants);
    angular.copy(allSuppliers, $scope.allSuppliers);
  }

  initData();

  $scope.$watch('import_item.variant', function(newSelectedVariant, oldValue){
    if(newSelectedVariant != oldValue){
      $scope.import_item.price_per_item = newSelectedVariant.price;
      $scope.import_item.discount_per_item = newSelectedVariant.discount_amount;
      if(newSelectedVariant.cost){
        $scope.import_item.cost_per_item = newSelectedVariant.cost;
      }
    }
  });

  $scope.save = function (invalid) {
    if(invalid){
      return;
    }

    if(!$scope.import_history.id){
      promise = importHistoriesService.create($scope.import_history);
      promise.then(function(){
        saveImportItemAndCloseModal(true);
      });
    }
    else{
      saveImportItemAndCloseModal(false);
    }

  };

  function saveImportItemAndCloseModal(isNewImportHistory){
    if($scope.import_item.supplier){
      $scope.import_item.supplier_id = $scope.import_item.supplier.id;
    }
    $scope.import_item.variant_id = $scope.import_item.variant.id;

    importItemsService.save($scope.import_history, $scope.import_item).success(function(){
      result = {
        import_history: $scope.import_history,
        import_item: $scope.import_item,
        isNewImportHistory: isNewImportHistory,
      };

      $uibModalInstance.close(result);
    });
  }

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };

}]);
