app.controller('newImportHistoryCtrl', ['$scope', 'importHistoriesService', '$log', '$state', 'productsService','suppliersService', '$uibModal', 'importItemsService', 'Notification',
function($scope, importHistoriesService, $log, $state, productsService, suppliersService, $uibModal, importItemsService, Notification){

  $scope.allVariants = productsService.allVariants;
  $scope.allSuppliers = suppliersService.allSuppliers;

  $scope.import_history = importHistoriesService.import_history;

  $scope.addImportItem = function(){
    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: 'admin/import_histories/shared/_import_item_modal.html',
      controller: 'importItemModalCtrl',
      size: 'lg',
      resolve: {
        import_item: function () {
          return defaultImportItem();
        },
        import_history: function(){
          return $scope.import_history;
        },
        allSuppliers: function(){
          return $scope.allSuppliers;
        },
        allVariants: function(){
          return $scope.allVariants;
        },
        currentUser: function(){
          return $scope.currentUser;
        }
      }
    });

    modalInstance.result.then(function (result) {
      angular.merge($scope.import_history, result.import_history);
      $scope.import_history.import_items_attributes.unshift(result.import_item);
      if(result.isNewImportHistory){
        $state.go("editImportHistory", {id: $scope.import_history.id});
      }
    });
  };

  $scope.editImportItem = function(item){
    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: 'admin/import_histories/shared/_import_item_modal.html',
      controller: 'importItemModalCtrl',
      size: 'lg',
      resolve: {
        import_item: function () {
          return item;
        },
        import_history: function(){
          return $scope.import_history;
        },
        allSuppliers: function(){
          return $scope.allSuppliers;
        },
        allVariants: function(){
          return $scope.allVariants;
        },
        currentUser: function(){
          return $scope.currentUser;
        }
      }
    });

    modalInstance.result.then(function (result) {
      angular.merge($scope.import_history, result.import_history);
      angular.merge(item, result.import_item);
    });
  };
  
  $scope.deleteImportItem = function(item){
    importItemsService.delete($scope.import_history, item);
  }

  $scope.save = function(){
    if(!$scope.import_history.id){
      return;
    }
    $scope.import_history.delivery_fee_total  = $scope.import_history.delivery_fee_total || 0;

    importHistoriesService.update($scope.import_history).success(function(){
      Notification.success("Cap nhat thanh cong");
    });
  }

  $scope.export_prices = function(){
    if(!$scope.import_history.id){
      return;
    }

    importHistoriesService.export_prices($scope.import_history).then(function(){
      Notification.success("Cap nhat gia toi san pham thanh cong");
    });
  };

  function defaultImportItem(){
    default_import_item = {
      quantity: 1,
      cost_per_item: 0,
      other_fee: 0,
      price_per_item: $scope.allVariants[0].price,
      discount_per_item: $scope.allVariants[0].discount_amount,
      discount_per_item: 0,
      variant: $scope.allVariants[0],
      // supplier: $scope.allSuppliers[0],
      supplier: null,
    };

    if($scope.allVariants[0].cost){
      default_import_item.cost_per_item = $scope.allVariants[0].cost;
    }

    return default_import_item;
  }
}]);
