app.controller('otherItemManagerCtrl', ['$scope', 'importHistoriesService', '$log', '$state', 'productsService','suppliersService', 
'$uibModal', 'otherItemsService', 'otherImportItemService',
function($scope, importHistoriesService, $log, $state, productsService, suppliersService, $uibModal, otherItemsService, otherImportItemService){
  $scope.addOtherItem = function(){
    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: 'admin/import_histories/shared/_other_import_item_modal.html',
      controller: 'otherImportItemModalCtrl',
      size: 'lg',
      resolve: {
        other_import_item: function () {
          return {};
        },
        import_history: function(){
          return $scope.import_history;
        },
        allOtherItems: function(){
          return otherItemsService.all().then(function(result){
            return result.data;
          });
        },
      }
    });

    modalInstance.result.then(function (result) {
      angular.merge($scope.import_history, result.import_history);
      $scope.import_history.other_import_items_attributes.unshift(result.other_import_item);
      if(result.isNewImportHistory){
        $state.go("editImportHistory", {id: $scope.import_history.id});
      }
    });
  };

  $scope.editOtherImportItem = function(item){
    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: 'admin/import_histories/shared/_other_import_item_modal.html',
      controller: 'otherImportItemModalCtrl',
      size: 'lg',
      resolve: {
        other_import_item: function () {
          return item;
        },
        import_history: function(){
          return $scope.import_history;
        },
        allOtherItems: function(){
          return otherItemsService.all().then(function(result){
            return result.data;
          });
        },
      }
    });

    modalInstance.result.then(function (result) {
      angular.merge($scope.import_history, result.import_history);
      angular.merge(item, result.other_import_item);
    });
  };
  
  $scope.deleteOtherImportItem = function(item){
    otherImportItemService.delete($scope.import_history, item);
  }
}]);
