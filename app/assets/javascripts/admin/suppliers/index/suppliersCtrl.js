app.controller('SuppliersCtrl', ['$scope', 'suppliersService', '$log', '$state', 'productsService',
function($scope, suppliersService, $log, $state, productsService){
  $scope.suppliers = suppliersService.suppliers;
  $scope.pageConfig = suppliersService.pageConfig;

  $scope.allProducts = productsService.allProducts;

  $scope.pageChanged = function() {
    suppliersService.index(getParams());
  };

  $scope.q = {};
  $scope.search = function(invalid){
    console.log("in search");
    if(invalid){
      return;
    }

    $scope.pageConfig.currentPage = 1;
    suppliersService.index(getParams());
  };

  $scope.clearSearch = function(){
    angular.copy({}, $scope.q);
    $scope.search();
  };

  $scope.delete = function(supplier){
    suppliersService.destroy(supplier);
  };

  function getParams(){
    return {
      page: $scope.pageConfig.currentPage,
      q: $scope.q
    };
  };

}]);
