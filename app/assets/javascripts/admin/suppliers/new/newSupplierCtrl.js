app.controller('newSupplierCtrl', ['$scope', 'suppliersService', '$log', '$state', 'productsService',
function($scope, suppliersService, $log, $state, productsService){
  $scope.supplier = {
    phones_attributes: [{}],
    selectedProducts: [],
  };

  $scope.allProducts = productsService.allProducts;

  $scope.create = function(invalid){

    $scope.supplier.isCreating = true;
    if(invalid){
      return;
    }

    function getId(item){
      return item.id;
    };
    $scope.supplier.product_ids = $scope.supplier.selectedProducts.map(getId);
    console.log("supplier: ", $scope.supplier);

    suppliersService.create($scope.supplier).success(function(){
      $state.go('listingSuppliers');
    }).finally(function(){
      $scope.supplier.isCreating = false;
    });
  };

  $scope.addPhone = function(){
    $scope.supplier.phones_attributes.push({
    });
  };

  $scope.removePhone = function(phone){
    var index = $scope.supplier.phones_attributes.indexOf(phone);
    $scope.supplier.phones_attributes.splice(index, 1);
  };

  $scope.cancel = function(){
    $state.go('listingSuppliers');
  };
}]);
