app.controller('editSupplierCtrl', ['$scope', 'suppliersService', '$log', '$state', 'productsService',
function($scope, suppliersService, $log, $state, productsService){
  $scope.supplier = suppliersService.supplier;
  $scope.supplier.selectedProducts = $scope.supplier.products;
  $scope.allProducts = productsService.allProducts;

  $scope.update = function(invalid){
    $scope.supplier.isCreating = true;
    if(invalid){
      return;
    }

    function getId(item){
      return item.id;
    };
    $scope.supplier.product_ids = $scope.supplier.selectedProducts.map(getId);
    console.log("supplier: ", $scope.supplier);

    suppliersService.update($scope.supplier).success(function(){
      $state.go('listingSuppliers');
    }).finally(function(){
      $scope.supplier.isCreating = false;
    });
  };

  $scope.addPhone = function(){
    $scope.supplier.phones_attributes.push({
    });
  };

  $scope.removePhone = function(phone){
    phone._destroy = true;
  };

  $scope.cancel = function(){
    $state.go('listingSuppliers');
  };

}]);
