app.controller('CustomersCtrl', ['$scope', 'customersService', '$log', '$state',
'$uibModal', '$window', '$httpParamSerializerJQLike', 'staffsService',
function($scope, customersService, $log, $state, $uibModal, $window, $httpParamSerializerJQLike, staffsService){

  $scope.customers = customersService.customers;
  $scope.list_permissions = customersService.list_permissions;
  $scope.allStaffs = staffsService.allStaffs;
  $scope.pageConfig = customersService.pageConfig;

  $scope.pageChanged = function() {
    customersService.index(getParams());
  };

  $scope.q = {};
  $scope.search = function(invalid){
    if(invalid){
      return;
    }

    if($scope.q.customer_of_seller){
      $scope.q.orders_customer_id_eq = $scope.q.customer_of_seller.id;
    }

    $scope.pageConfig.currentPage = 1;
    customersService.index(getParams());
  };

  $scope.loadController = true;
  $scope.$watch('q', function(){
    if($scope.loadController == true){
      $scope.loadController = false;
      return;
    }

    $scope.search(false);
  }, true);

  $scope.clearSearch = function(){
    angular.copy({}, $scope.q);
    $scope.search();
  };

  $scope.get_excel = function(){
    if($scope.q.customer_of_seller){
      $scope.q.orders_customer_id_eq = $scope.q.customer_of_seller.id;
    }

    var q = {
      q: $scope.q,
    };
    $window.open('admin/customers/get_excel.xlsx?' + $httpParamSerializerJQLike(q), '_blank'); 
    angular.copy({}, $scope.q);
  };

  $scope.delete = function(customer){
    customersService.destroy(customer);
  };

  function getParams(){
    return {
      page: $scope.pageConfig.currentPage,
      q: $scope.q
    };
  };

}]);
