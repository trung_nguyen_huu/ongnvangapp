app.controller('newCustomerCtrl', ['$scope', 'customersService', '$log', '$state', 'Notification',
function($scope, customersService, $log, $state, Notification){
  $scope.customer = {};

  $scope.create = function(invalid){
    console.log("customer: ", $scope.customer);

    if(invalid){
      return;
    }

    $scope.customer.isCreating = true;
    customersService.create($scope.customer).success(function(){
      $state.go('listingCustomers');
      Notification.success("Them moi thanh cong");
    }).finally(function(){
      $scope.customer.isCreating = false;
    });
  };

  $scope.cancel = function(){
    $state.go('listingCustomers');
  };

}]);
