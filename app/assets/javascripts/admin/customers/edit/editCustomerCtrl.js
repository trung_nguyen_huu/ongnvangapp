app.controller('editCustomerCtrl', ['$scope', 'customersService', '$log', '$state', 'Notification',
function($scope, customersService, $log, $state, Notification){

  $scope.customer = customersService.customer;

  $scope.update = function(invalid){
    console.log("customer: ", $scope.customer);

    $scope.customer.isCreating = true;
    if(invalid){
      return;
    }

    customersService.update($scope.customer).success(function(){
      $state.go('listingCustomers');
      Notification.success("cap nhat thanh cong");
    }).finally(function(){
      $scope.customer.isCreating = false;
    });
  };

  $scope.cancel = function(){
    $state.go('listingCustomers');
  };

  $scope.toggle_per = function(){
    $scope.customer.permissions.can_update = !$scope.customer.permissions.can_update;
  }
}]);
