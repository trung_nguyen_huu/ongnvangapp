app.controller('OrdersCtrl', ['$scope', 'ordersService', '$log', '$state', '$httpParamSerializerJQLike', '$window',
'staffsService',
function($scope, ordersService, $log, $state, $httpParamSerializerJQLike, $window, staffsService){
  $scope.dateOptions = {
    formatYear: 'yy',
    startingDay: 1
  };

  $scope.orders = ordersService.orders;
  $scope.allStaffs = staffsService.allStaffs;
  $scope.pageConfig = ordersService.pageConfig;
  $scope.list_permissions = ordersService.list_permissions;

  $scope.pageChanged = function() {
    ordersService.index(getParams());
  };

  $scope.q = {};
  $scope.search = function(invalid){
    if(invalid){
      return;
    }

    if($scope.q.order_of_seller){
      $scope.q.seller_id_eq = $scope.q.order_of_seller.id;
    }

    $scope.pageConfig.currentPage = 1;
    ordersService.index(getParams());
  };

  $scope.loadController = true;
  $scope.$watch('q', function(){
    if($scope.loadController == true){
      $scope.loadController = false;
      return;
    }

    $scope.search(false);
  }, true);

  $scope.clearSearch = function(){
    angular.copy({}, $scope.q);
    $scope.search();
  };

  $scope.get_excel = function(){
    if($scope.q.order_of_seller){
      $scope.q.seller_id_eq = $scope.q.order_of_seller.id;
    }
    var q = {
      q: $scope.q,
    };
    $window.open('admin/orders/get_excel.xlsx?' + $httpParamSerializerJQLike(q), '_blank'); 
    angular.copy({}, $scope.q);
  };

  $scope.delete = function(order){
    ordersService.destroy(order);
  };

  function getParams(){
    return {
      page: $scope.pageConfig.currentPage,
      q: $scope.q
    };
  };

}]);
