app.controller('newOrderCtrl', ['$scope', 'ordersService', '$log', '$state',
'productsService', 'lineItemsService', 'customersService', '$uibModal', 'Notification',
function($scope, ordersService, $log, $state, productsService, lineItemsService, customersService, $uibModal, Notification){
  $scope.allVariants = productsService.allVariants;

  if($state.current.name == 'editOrder'){
    $scope.order = ordersService.order;
  }

  if($state.current.name == 'newOrder'){
    $scope.order = {
      can_edit_by_current_user: true,
      line_items_attributes: [],
      permissions: {
        can_create: true,
        can_read: true,
        can_update: true,
        can_destroy: true,
      }
    };

    ordersService.order = $scope.order;
  }

  $scope.addLineItem = function(){
    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: 'admin/orders/shared/_line_item_modal.html',
      controller: 'lineItemModalCtrl',
      size: 'lg',
      resolve: {
        line_item: function () {
          return defaultLineItem();
        },
        order: function(){
          return $scope.order;
        },
        allVariants: function(){
          return productsService.getAllVariants().then(function(result){
            return result.data;
          });
        },
      }
    });

    modalInstance.result.then(function (result) {
      angular.merge($scope.order, result.order);
      $scope.order.line_items_attributes.unshift(result.line_item);
      if(result.isNewOrder){
        $state.go("editOrder", {id: $scope.order.number});
      }
    });
  };

  function defaultLineItem(){
    return {
      quantity: 1,
    };
  }

  $scope.editLineItem = function(line_item){
    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: 'admin/orders/shared/_line_item_modal.html',
      controller: 'lineItemModalCtrl',
      size: 'lg',
      resolve: {
        line_item: function () {
          return line_item;
        },
        order: function(){
          return $scope.order;
        },
        allVariants: function(){
          return productsService.getAllVariants().then(function(result){
            return result.data;
          });
        },
      }
    });

    modalInstance.result.then(function (result) {
      angular.merge($scope.order, result.order);
      angular.merge(line_item, result.line_item);
    });
  };

  $scope.deleteLineItem = function(line_item){
    lineItemsService.delete($scope.order, line_item);
    var index = $scope.order.line_items_attributes.indexOf(line_item);
    $scope.order.line_items_attributes.splice(index, 1);
  };

  //Save order
  $scope.save = function(){
    if(!$scope.order.permissions.can_update){
      ordersService.update_note($scope.order).success(function(){
          Notification.success('Cap nhat don hang thanh cong');
      });
    }

    if($scope.order.seller){
      $scope.order.seller_id = $scope.order.seller.id;
    }
    $scope.order.delivery_fee_total = $scope.order.delivery_fee_total || 0;

    ordersService.save($scope.order).success(function(){
        Notification.success('Cap nhat don hang thanh cong');
    });
  };

  $scope.can_save_order = function(order){
    return order.can_edit_by_current_user && (order.customer ? true : false);
  }

  //Tinh toan item total tren frontsite
  $scope.itemTotal = function(item){
    if(item.variant){
      return item.price_with_sale*item.quantity;
    }
    return 0;
  };

  //Quan ly customer here
  //Ham tim kiem customer
  $scope.searchCustomer = function(value){
    return customersService.index({
        page: 1,
        all: false,
        q: {
          phone_cont: value,
          name_cont_any: value,
          number_cont: value,
          m: 'or',
        }
      }).then(function(result){
        $scope.customers = result.data.customers;
      });
  };

  $scope.addNewCustomer = function () {
    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: 'admin/orders/shared/_customer_modal.html',
      controller: 'customerModalCtrl',
      size: 'lg',
      resolve: {
        customer: function () {
          return {};
        }
      }
    });

    modalInstance.result.then(function (customer) {
      $scope.order.customer = customer;
    });
  };

  $scope.editCustomer = function () {
    if(!$scope.order.customer){
      return;
    }

    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: 'admin/orders/shared/_customer_modal.html',
      controller: 'customerModalCtrl',
      size: 'lg',
      resolve: {
        customer: function () {
          return $scope.order.customer;
        }
      }
    });

    modalInstance.result.then(function (customer) {
      $scope.order.customer = customer;
    });
  };
}]);
