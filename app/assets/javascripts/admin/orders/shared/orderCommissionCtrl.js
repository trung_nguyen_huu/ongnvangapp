app.controller('orderCommissionCtrl', ['$scope', 'ordersService', 'commissionsService', '$window', 'Notification',
function($scope, ordersService, commissionsService, $window, Notification){
  $scope.commission =  defaultCommission();

  if ($scope.order.commission){
    $scope.commission = $scope.order.commission;
  }

  $scope.saveCommission = function(invalid){
    if(invalid){
      return;
    }

    if($scope.commission.amount == 0){
      Notification.error("Số tiền phải > 0");
      return;
    }

    if(!$scope.order.seller_id && !$scope.order.seller){
      Notification.error("Vui lòng chọn người bán trước khi nhập chiết khấu");
      return;
    }

    if(!$scope.order.seller_id){
      $scope.order.seller_id = $scope.order.seller.id;
      ordersService.save($scope.order).success(function(){
        Notification.success('Cap nhat don hang thanh cong');
        callSaveCommission();
      });
    }
    else{
      callSaveCommission();
    }
  };

  function callSaveCommission(){
    if(!$scope.commission.id){
      updateOrCreate = commissionsService.create;
    }
    else{
      updateOrCreate = commissionsService.update;
    }

    updateOrCreate($scope.commission, $scope.order).success(function(){
      Notification.success('Cập nhật chiết khấu thành công');
    });
  };

  $scope.destroyCommission = function(){
    if($scope.commission.id){
      if($window.confirm("Bạn có chắc xoá chiết khấu của nhn viên này không ?")){
        commissionsService.destroy($scope.commission, $scope.order).success(function(){
          Notification.success('Xoá chiết khấu thành công');
          angular.copy(defaultCommission(), $scope.commission);
        });
      }
    }
  };

  function defaultCommission(){
    return {
      amount: 0,
      permissions: {
        can_create: true,
        can_update: true,
        can_destroy: true,
      }
    };
  };
}]);
