app.controller('lineItemModalCtrl', ['$scope', '$uibModalInstance',
'line_item', 'order', 'ordersService', 'lineItemsService', 'allVariants', 'Notification',
function ($scope, $uibModalInstance, line_item, order, ordersService, lineItemsService, allVariants, Notification) {

  function initData(){
    $scope.moneyOptions = {
      prefix: '',
      suffix: '₫',
      precision: 2,
      allowNegative: false,
    };

    $scope.line_item = {};
    $scope.order = {};
    $scope.allVariants = [];

    angular.copy(line_item, $scope.line_item);
    angular.copy(order, $scope.order);
    angular.copy(allVariants, $scope.allVariants);
  }

  initData();

  $scope.save = function (invalid) {
    if(invalid){
      return;
    }

    if(!$scope.order.id){
      if($scope.order.seller){
        $scope.order.seller_id = $scope.order.seller.id;
      }
      $scope.order.delivery_fee_total = $scope.order.delivery_fee_total || 0;

      promise = ordersService.create($scope.order);
      promise.then(function(){
        saveLineItemAndCloseModal(true);
      });
    }
    else{
      saveLineItemAndCloseModal(false);
    }

  };

  function saveLineItemAndCloseModal(isNewOrder){
    lineItemsService.save($scope.order, $scope.line_item).success(function(){
      result = {
        order: $scope.order,
        line_item: $scope.line_item,
        isNewOrder: isNewOrder,
      };

      $uibModalInstance.close(result);
    }).error(function(){
      Notification.error("Khong du so luong");
    });
  }

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };

}]);
