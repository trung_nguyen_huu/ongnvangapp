app.controller('customerModalCtrl', ['$scope', '$uibModalInstance',  'customersService', 'customer',
function($scope, $uibModalInstance, customersService, customer){
  $scope.customer = customer;

  $scope.save = function(invalid){
    $scope.customer.isCreating = true;
    if(invalid){
      return;
    }

    customersService.create($scope.customer).success(function(data){
      angular.copy(data, $scope.customer);
      $uibModalInstance.close($scope.customer);
    }).finally(function(){
      $scope.customer.isCreating = false;
    });
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
}]);
