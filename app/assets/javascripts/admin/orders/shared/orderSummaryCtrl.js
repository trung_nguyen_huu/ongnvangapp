app.controller('orderSummaryCtrl', ['$scope', 'ordersService',
'productsService', 'lineItemsService', 'customersService', 'staffsService', '$window',
function($scope, ordersService, productsService, lineItemsService, customersService, staffsService, $window){
  $scope.allStaffs = staffsService.allStaffs;

  $scope.order.seller = _.find($scope.allStaffs, function(staff){
    return staff.id == $scope.order.seller_id;
  });

  $scope.ship = function(){
    ordersService.ship($scope.order, {
      order: {
        delivery_date: (new Date()),
      }
    });
  };

  $scope.received = function(){
    ordersService.received($scope.order, {
      order: {
        receive_date: (new Date()),
      }
    });
  };

  $scope.refund = function(){
    var confirmed = $window.confirm('Bạn có chắc ko?');

    if (confirmed) {
      ordersService.refund($scope.order, {
        refund_date: (new Date()),
      });
    }
  };
}]);
