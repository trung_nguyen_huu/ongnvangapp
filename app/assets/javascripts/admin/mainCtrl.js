app.controller('MainCtrl', ['$scope', 'usersService', function($scope, usersService){
  usersService.getCurrentUser().then(function(){
    $scope.currentUser = usersService.currentUser;
    $scope.currentUser.admin = function(){
      return $scope.currentUser.role == 'admin';
    };

    $scope.currentUser.is_admin = function(){
      return $scope.currentUser.role == 'admin';
    };
  });

  $scope.moneyOptions = {
    prefix: '',
    suffix: '₫',
    precision: 2,
    allowNegative: false,
  };
}]);
