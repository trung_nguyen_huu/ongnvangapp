app.controller('InventoriesCtrl', ['$scope', 'inventoriesService', '$log', '$state', 'productsService', '$uibModal', 'productsService', '$window', '$httpParamSerializerJQLike',
function($scope, inventoriesService, $log, $state, productsService, $uibModal, productsService, $window, $httpParamSerializerJQLike){
  $scope.inventories = inventoriesService.inventories;
  $scope.pageConfig = inventoriesService.pageConfig;
  $scope.summary = inventoriesService.summary;
  $scope.allProducts = productsService.allProducts;
  $scope.list_permissions = inventoriesService.list_permissions;

  $scope.pageChanged = function() {
    inventoriesService.index(getParams());
  };

  $scope.chiTietTonKho = function(inventory){
    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: 'admin/inventories/index/_inventory_modal.html',
      controller: 'InventoryModalCtrl',
      size: 'lg',
      resolve: {
        inventory: function () {
          return inventoriesService.show(inventory.id).then(function(){
            return inventoriesService.inventory;
          });
        }
      }
    });
  };

  $scope.q = {};
  $scope.search = function(invalid){
    console.log("in search");
    if(invalid){
      return;
    }

    $scope.pageConfig.currentPage = 1;
    inventoriesService.index(getParams());
  };

  $scope.clearSearch = function(){
    angular.copy({}, $scope.q);
    $scope.search();
  };

  $scope.onSelectProductName = function(item, model){
    $scope.search();
    // $state.go("editProduct", { id: item.id })
  };

  $scope.get_excel = function(){
    var q = {
      q: $scope.q,
    };
    $window.open('admin/inventories/get_excel.xlsx?' + $httpParamSerializerJQLike(q), '_blank'); 
    angular.copy({}, $scope.q);
  };

  function getParams(){
    return {
      page: $scope.pageConfig.currentPage,
      q: $scope.q
    };
  };

}]);

app.controller('InventoryModalCtrl', ['$scope', '$uibModalInstance', 'inventory', function ($scope, $uibModalInstance, inventory) {
  $scope.inventory = inventory;
}]);
