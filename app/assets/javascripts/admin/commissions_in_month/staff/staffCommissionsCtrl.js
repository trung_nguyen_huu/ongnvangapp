app.controller('staffCommissionsCtrl', ['$scope', 'commissionsService', '$state',
function($scope, commissionsService, $state){
  $scope.commissions = commissionsService.commissions;
  $scope.list_permissions = commissionsService.list_permissions;

  if($state.current.name == 'listingCommissions'){
    $scope.list_permissions.can_create = false;
  }

  $scope.pageConfig = commissionsService.pageConfig;

  $scope.pageChanged = function() {
    commissionsService.index(getParams(), $scope.staff);
  };

  $scope.q = {};
  $scope.search = function(invalid){
    console.log("in search");
    if(invalid){
      return;
    }

    $scope.pageConfig.currentPage = 1;
    commissionsService.index(getParams(), $scope.staff);
  };

  $scope.clearSearch = function(){
    angular.copy({}, $scope.q);
    $scope.search();
  };

  function getParams(){
    return {
      page: $scope.pageConfig.currentPage,
      q: $scope.q
    };
  };

}]);

