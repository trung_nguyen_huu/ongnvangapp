app.controller('CommissionsInMonthCtrl', ['$scope', 'commissionsInMonthService', '$log', '$state', 'staffsService', '$uibModal',
function($scope, commissionsInMonthService, $log, $state, staffsService, $uibModal){
  $scope.staff = staffsService.staff;
  $scope.commissions_in_month = commissionsInMonthService.commissions_in_month;
  $scope.list_permissions = commissionsInMonthService.list_permissions;

  $scope.pageConfig = commissionsInMonthService.pageConfig;

  $scope.pageChanged = function() {
    commissionsInMonthService.index(getParams(), $scope.staff);
  };

  $scope.q = {};
  $scope.search = function(invalid){
    console.log("in search");
    if(invalid){
      return;
    }

    $scope.pageConfig.currentPage = 1;
    commissionsInMonthService.index(getParams(), $scope.staff);
  };

  $scope.clearSearch = function(){
    angular.copy({}, $scope.q);
    $scope.search();
  };

  function getParams(){
    return {
      page: $scope.pageConfig.currentPage,
      q: $scope.q
    };
  };

}]);


// app.controller('CommissionModalCtrl', [ '$scope', '$uibModalInstance', 'commission', 'commissionsService', 'staff',
// function ($scope, $uibModalInstance, commission, commissionsService, staff) {
//   $scope.commission = {};
//   angular.copy(commission, $scope.commission);
//   $scope.staff = staff;
//
//   $scope.save = function () {
//     if($scope.commission.id){
//       commissionsService.update($scope.commission, $scope.staff).then(closeModal());
//     }
//     else{
//       commissionsService.create($scope.commission, $scope.staff).then(closeModal());
//     }
//   };
//
//   $scope.cancel = function () {
//     $uibModalInstance.dismiss('cancel');
//   };
//
//   function closeModal(){
//     $uibModalInstance.close($scope.commission);
//   }
//
//   $scope.moneyOptions = {
//     prefix: '',
//     suffix: '₫',
//     precision: 2,
//     allowNegative: false,
//   };
//
// }]);
