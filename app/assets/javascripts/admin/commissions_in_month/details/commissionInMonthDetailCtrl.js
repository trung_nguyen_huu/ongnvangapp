app.controller('commissionInMonthDetailCtrl', ['$scope', 'commissionsInMonthService',
  '$log', '$state', 'Notification', '$uibModal', 
function($scope, commissionsInMonthService, $log, $state, Notification, $uibModal){
  $scope.commission_in_month = commissionsInMonthService.commission_in_month;

  $scope.addPay = function(){
    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: 'admin/commissions_in_month/details/_commission_pay_modal.html',
      controller: 'CommissionPayModalCtrl',
      size: 'lg',
      resolve: {
        commission_pay: function () {
          return {
            amount: 0,
          };
        },
        commission_in_month: function(){
          return $scope.commission_in_month;
        },
      }
    });

    modalInstance.result.then(function (pay) {
      $scope.commission_in_month.ton_cuoi_ky = pay.commission_in_month.ton_cuoi_ky;
      $scope.commission_in_month.pays.push(pay);
      Notification.success('Thêm mới lần trả thành công');
    });
  };
  
  $scope.updatePay = function(commission_pay){
    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: 'admin/commissions_in_month/details/_commission_pay_modal.html',
      controller: 'CommissionPayModalCtrl',
      size: 'lg',
      resolve: {
        commission_pay: commission_pay,
        commission_in_month: function(){
          return $scope.commission_in_month;
        },
      }
    });

    modalInstance.result.then(function (pay) {
      $scope.commission_in_month.ton_cuoi_ky = pay.commission_in_month.ton_cuoi_ky;
      $scope.commission_in_month.pays.push(pay);
      Notification.success('Cập nhập lần trả thành công');
    });
  };

  $scope.destroyPay = function(commission_pay){
    commissionsInMonthService.destroyPay(commission_pay, $scope.commission_in_month).success(function(data){
      var index = $scope.commission_in_month.pays.indexOf(commission_pay);
      $scope.commission_in_month.pays.splice(index, 1);
      $scope.commission_in_month.ton_cuoi_ky = data.commission_in_month.ton_cuoi_ky;
      Notification.success('Xoá lần trả thành công');
    });
  };
}]);

app.controller('CommissionPayModalCtrl', ['$scope', '$uibModalInstance',  'commissionsInMonthService', 'commission_pay', 'commission_in_month',
function($scope, $uibModalInstance, commissionsInMonthService, commission_pay, commission_in_month){
  $scope.commission_pay = {};
  $scope.commission_in_month = {};
  angular.copy(commission_pay, $scope.commission_pay);
  angular.copy(commission_in_month, $scope.commission_in_month);

  $scope.save = function(invalid){
    $scope.commission_pay.isCreating = true;
    if(invalid){
      return;
    }

    successFunction = function(data) {
      $uibModalInstance.close(data);
    };

    finalFunction = function() {
      $scope.commission_pay.isCreating = false;
    };

    if($scope.commission_pay.id){
      commissionsInMonthService.updatePay($scope.commission_pay, $scope.commission_in_month).success(successFunction).finally(finalFunction);
    } else {
      commissionsInMonthService.createPay($scope.commission_pay, $scope.commission_in_month).success(successFunction).finally(finalFunction);
    }
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
}]);
