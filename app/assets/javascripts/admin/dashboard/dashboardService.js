app.factory("dashboardService", ['$http', function($http){
  var o = {
    charts: {},
  };

  o.staff = function(query){
    return $http.get("admin/dashboard/staff.json" + query).success(function(data){
      angular.extend(o.charts, data);
      console.log("charts: ", o);
    });
  };

  o.order = function(query){
    return $http.get("admin/dashboard/order.json" + query).success(function(data){
      angular.extend(o.charts, data);
      console.log("charts in order: ", o);
    });
  };

  o.staffWithFilter = function(filter){
    filter = filter || undefined;
    var query = "";
    if(filter && filter != {}){
      var query = "?frequency=" + filter.frequency + "&from_date=" + filter.from_date + "&to_date=" +filter.to_date + "&state=" + 
        filter.staffState;
    }

    return o.staff(query);
  };

  o.orderWithFilter = function(filter){
    filter = filter || undefined;
    var query = "";
    if(filter && filter != {}){
      var query = "?frequency=" + filter.frequency + "&from_date=" + filter.from_date + "&to_date=" +filter.to_date + "&state=" + 
        filter.orderState;
    }

    return o.order(query);
  };

  o.getAll = function(filter){
    filter = filter || undefined;
    var query = "";
    if(filter && filter != {}){
      var query = "?frequency=" + filter.frequency + "&from_date=" + filter.from_date + "&to_date=" +filter.to_date;
    }
    console.log("query: ", query);

    return o.staff(query).then(function(){
      return o.order(query);
    });
  };

  return o;
}]);
