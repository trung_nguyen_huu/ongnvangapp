app.controller('dashboardCtrl', ['$scope', 'Notification', 'usersService', 'dashboardService', function($scope, Notification, usersService, dashboardService){
  $scope.filter = {
    staffState: 'completed',
    orderState: 'completed',
  };

  $scope.dateOptions = {
    formatYear: 'yy',
    startingDay: 1
  };

  $scope.filter = function(invalid){
    if(invalid){
      return;
    }
    $scope.filter.isLoading = true;
    dashboardService.getAll($scope.filter)
    .then(reloadChart)
    .finally(function(){
      $scope.filter.isLoading = false;
    });
  };

  $scope.filterOrderChartByState = function(){
    $scope.filter.isLoading = true;
    dashboardService.orderWithFilter($scope.filter)
                    .then(reloadChart)
                    .finally(function(){
                      $scope.filter.isLoading = false;
                    });
  };

  $scope.filterStaffChartByState = function(){
    $scope.filter.isLoading = true;
    dashboardService.staffWithFilter($scope.filter)
                    .then(reloadChart)
                    .finally(function(){
                      $scope.filter.isLoading = false;
                    });
  };

  $scope.clearFilter = function(){
    angular.copy({frequency: 'month'}, $scope.filter);
    dashboardService.getAll().then(reloadChart);
  }

  function reloadChart(){
    $scope.number_of_orders_by_staff.xAxis.categories = $scope.charts.staff.number_of_orders_by_staff.x_axis;
    $scope.number_of_orders_by_staff.series = $scope.charts.staff.number_of_orders_by_staff.series;

    $scope.discount_amounts_by_staff.xAxis.categories = $scope.charts.staff.discount_amounts_by_staff.x_axis;
    $scope.discount_amounts_by_staff.series = $scope.charts.staff.discount_amounts_by_staff.series;

    $scope.debit_total_by_staff.xAxis.categories = $scope.charts.staff.debit_total_by_staff.x_axis;
    $scope.debit_total_by_staff.series = $scope.charts.staff.debit_total_by_staff.series;

    $scope.number_of_selled_products_by_staff.xAxis.categories = $scope.charts.staff.number_of_selled_products_by_staff.x_axis;
    $scope.number_of_selled_products_by_staff.series = $scope.charts.staff.number_of_selled_products_by_staff.series;

    $scope.order_chart_summary.xAxis.categories = $scope.charts.order.total_of_all_orders.x_axis;
    $scope.order_chart_summary.series = [
      {
        name: 'Doanh thu',
        data: $scope.charts.order.total_of_all_orders.data
      },
      {
        name: 'Tong loi nhuan',
        data: $scope.charts.order.total_of_debit.data
      },
      {
        name: 'Chi phi',
        data: $scope.charts.order.total_of_origin_price.data
      }

      ];

    $scope.number_of_orders.xAxis.categories = $scope.charts.order.number_of_orders.x_axis;
    $scope.number_of_orders.series = [{
      name: 'So luong',
      data: $scope.charts.order.number_of_orders.data
    }];
 
  };


  $scope.charts = dashboardService.charts;
  $scope.plotOptions = {
    line: {
      dataLabels: {
        enabled: true
      },
    }
  };
  $scope.options = {
    chart: {
      type: 'line',
    },
    plotOptions: $scope.plotOptions,
  };



  //B CHARTS BY STAFF
  $scope.number_of_orders_by_staff = {
    options: $scope.options,
    title: {
      text: 'So luong don huong duoc ban theo nhan vien',
    },
    xAxis: {
      categories: $scope.charts.staff.number_of_orders_by_staff.x_axis
    },
    yAxis: {
      title: {
        text: 'DON VI'
      },
    },
    series: $scope.charts.staff.number_of_orders_by_staff.series,
  };

  $scope.discount_amounts_by_staff = {
    options: $scope.options,
    title: {
      text: 'Chiet khau',
    },
    xAxis: {
      categories: $scope.charts.staff.discount_amounts_by_staff.x_axis
    },
    yAxis: {
      title: {
        text: 'VND'
      },
    },
    series: $scope.charts.staff.discount_amounts_by_staff.series,
  };

  if($scope.currentUser.is_admin()){
    $scope.debit_total_by_staff = {
      options: $scope.options,
      title: {
        text: 'Loi nhuan',
      },
      xAxis: {
        categories: $scope.charts.staff.debit_total_by_staff.x_axis
      },
      yAxis: {
        title: {
          text: 'VND'
        },
      },
      series: $scope.charts.staff.debit_total_by_staff.series,
    };
  }

  $scope.number_of_selled_products_by_staff = {
    options: $scope.options,
    title: {
      text: 'So luong san pham ban duoc boi nhan vien',
    },
    xAxis: {
      categories: $scope.charts.staff.number_of_selled_products_by_staff.x_axis
    },
    yAxis: {
      title: {
        text: 'DON VI'
      },
    },
    series: $scope.charts.staff.number_of_selled_products_by_staff.series,
  };
  //E CHARTS BY STAFF
  //
  //
  //B CHARTS BY ORDER
  $scope.order_chart_summary = {
    options: $scope.options,
    title: {
      text: 'Tong quat doanh thu va loi nhuan theo don hang',
    },
    xAxis: {
      categories: $scope.charts.order.total_of_all_orders.x_axis
    },
    yAxis: {
      title: {
        text: 'VND'
      },
    },
    series: [
      {
        name: 'Doanh thu',
        data: $scope.charts.order.total_of_all_orders.data
      },
      {
        name: 'Tong loi nhuan',
        data: $scope.charts.order.total_of_debit.data
      },
      {
        name: 'Chi phi',
        data: $scope.charts.order.total_of_origin_price.data
      }

    ],
  };

  $scope.number_of_orders = {
    options: $scope.options,
    title: {
      text: 'Tong so luong don hang',
    },
    xAxis: {
      categories: $scope.charts.order.number_of_orders.x_axis,
      crosshair: true
    },
    yAxis: {
      title: {
        text: 'DON VI'
      },
      min: 0,
    },
    series: [
      {
        name: 'So luong',
        data: $scope.charts.order.number_of_orders.data
      },
    ],
  };
  //E CHARTS BY ORDER
}]);

  // {
    //   doanh_thu_theo_nhan_vien: {
      //     title: 'Doanh thu theo nhan vien',
      //     subtle: 'Theo thang',
      //     y_axis_title: 'VND',
      //     x_axis: ['Jan', 'Feb', .....],
      //     series: [
        //       {
          //         name: 'Nhan vien 1',
          //         data: [10, 20, 30, ...],
          //       }
          //       {
            //         name: 'Nhan vien 2',
            //         data: [10, 20, 30, ...],
            //       }
            //     ],
          //   },
          //
          //   chiet_khau_theo_nhan_vien: {
            //     ....
            //   }
            // }

