app.directive('ngPermission', ['$compile', function($compile){

  return {
    restrict: 'A',
    scope:{
      "permissions": "=",
    },
    link: function(scope, element, attrs){
      var permissions = scope.permissions;
      if(!permissions.can_update){
        $(element).attr('disabled', 'disabled');
      }

      scope.$watch('permissions', function(){
        if(!permissions.can_update){
          $(element).attr('disabled', 'disabled');
        }
        if(permissions.can_update){
          $(element).removeAttr('disabled');
        }
      })
    }
  };

}]);
