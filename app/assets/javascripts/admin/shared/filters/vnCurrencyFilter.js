app.filter('vnCurrency', ['currencyFilter', function(currencyFilter){
  return function(amount){
    amount = amount || 0;
    return currencyFilter(amount, '', 0) + "₫";
  };

}]);
