app.filter('age', function() {
  function calculateAge(birthday) { // birthday is a date
  var tmp = birthday.split("-");
  tmp = tmp[2] + "-" + tmp[1] + "-" + tmp[0];
  tmp = new Date(tmp);

  var now = new Date();
  return now.getYear() - tmp.getYear();
}

return function(birthdate) { 
  return calculateAge(birthdate);
}; 
});
