app.factory('lineItemsService', ['$http', function($http){
  var o = {
  };

  o.create = function(order, line_item){
    var query = "admin/orders/"  + order.id + "/line_items.json";
    return $http.post(query, {
      line_item: line_item,
    }).success(function(data){
      angular.merge(line_item, data);
      angular.merge(order, line_item.order);
    });
  };

  o.update = function(order, line_item){
    var query = "admin/orders/"  + order.id + "/line_items/" + line_item.id +".json";
    return $http.put(query, {
      line_item: line_item,
    }).success(function(data){
      angular.merge(line_item, data);
      angular.merge(order, line_item.order);
    });
  };

  o.save = function(order, line_item){
    if(line_item.variant){
      line_item.variant_id = line_item.variant.id;
    }

    if(!line_item.id){
      return o.create(order, line_item);
    }
    else{
      return o.update(order, line_item);
    }
  };

  o.delete = function(order, line_item){
    var query = "admin/orders/"  + order.id + "/line_items/" + line_item.id +".json";
    return $http.delete(query).success(function(data){
      angular.merge(order, data.order)
    });
  };

  return o;
}]);
