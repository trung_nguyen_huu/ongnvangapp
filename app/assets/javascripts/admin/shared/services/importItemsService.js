app.factory('importItemsService', ['$http', function($http){
  var o = {
  };

  o.create = function(import_history, import_item){
    var query = "admin/import_histories/"  + import_history.id + "/import_items.json";
    return $http.post(query, {
      import_item: import_item,
    }).success(function(data){
      angular.merge(import_item, data);
      angular.merge(import_history, import_item.import_history);
    });
  };

  o.update = function(import_history, import_item){
    var query = "admin/import_histories/"  + import_history.id + "/import_items/" + import_item.id +".json";
    return $http.put(query, {
      import_item: import_item,
    }).success(function(data){
      angular.merge(import_item, data);
      angular.merge(import_history, import_item.import_history);
    });
  };

  o.save = function(import_history, import_item){
    if(!import_item.id){
      return o.create(import_history, import_item);
    }
    else{
      return o.update(import_history, import_item);
    }
  };

  o.delete = function(import_history, import_item){
    var query = "admin/import_histories/"  + import_history.id + "/import_items/" + import_item.id +".json";
    return $http.delete(query).success(function(data){
      var index = import_history.import_items_attributes.indexOf(import_item);
      import_history.import_items_attributes.splice(index, 1);
      angular.merge(import_history, data.import_history);
    });
  };

  return o;
}]);
