app.factory('otherImportItemService', ['$http', function($http){
  var o = {
  };

  o.create = function(import_history, other_import_item){
    var query = "admin/import_histories/"  + import_history.id + "/other_import_items.json";
    return $http.post(query, {
      other_import_item: other_import_item,
    }).success(function(data){
      angular.extend(other_import_item, data);
      angular.extend(import_history, other_import_item.import_history);
    });
  };

  o.update = function(import_history, other_import_item){
    var query = "admin/import_histories/"  + import_history.id + "/other_import_items/" + other_import_item.id +".json";
    return $http.put(query, {
      other_import_item: other_import_item,
    }).success(function(data){
      angular.extend(other_import_item, data);
      angular.extend(import_history, other_import_item.import_history);
    });
  };

  o.save = function(import_history, other_import_item){
    if(!other_import_item.id){
      return o.create(import_history, other_import_item);
    }
    else{
      return o.update(import_history, other_import_item);
    }
  };

  o.delete = function(import_history, other_import_item){
    var query = "admin/import_histories/"  + import_history.id + "/other_import_items/" + other_import_item.id +".json";
    return $http.delete(query).success(function(data){
      var index = import_history.other_import_items_attributes.indexOf(other_import_item);
      import_history.other_import_items_attributes.splice(index, 1);
      angular.merge(import_history, data.import_history);
    });
  };

  return o;
}]);
