app.factory('suppliersService', ['$http', function($http){
  var o = {
    suppliers: [],
    allSuppliers: [],
    supplier: {},
    pageConfig: {
      totalItems: 0,
      currentPage: 1,
      itemsPerPage: 10,
    }
  }

  o.index = function(params){
    params = params || {};
    var query = "admin/suppliers.json";

    var promise = $http.get(query, {params: params})
    .success(function(data){
      angular.copy(data.suppliers, o.suppliers);
      o.pageConfig.totalItems = data.total_items; o.pageConfig.currentPage = data.current_page; o.pageConfig.itemsPerPage = data.items_per_page;
    });

    return promise;
  };

  o.create = function(supplier){
    return $http.post("admin/suppliers.json", {
      supplier: supplier,
    });
  };
  
  o.update = function(supplier){
    return $http.put("admin/suppliers/" + supplier.id + ".json", {
      supplier: supplier,
    });
  };

  o.show = function(id){
    return $http.get("admin/suppliers/" + id + ".json").success(function(data){
      angular.copy(data, o.supplier);
    });
  };

  o.destroy = function(supplier){
    return $http.delete("admin/suppliers/" + supplier.id + ".json").success(function(){
      var index = o.suppliers.indexOf(supplier);
      o.suppliers.splice(index, 1);
    });
  };

  o.all = function(){
    return $http.get("admin/suppliers/all.json").success(function(data){
      angular.copy(data, o.allSuppliers);
    });
  }

  return o;
}]);
