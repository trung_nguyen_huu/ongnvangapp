app.factory('ordersService', ['$http', function($http){
  var o = {
    orders: [],
    order: {},
    list_permissions: {},
    pageConfig: {
      totalItems: 0,
      currentPage: 1,
      itemsPerPage: 10,
    }
  }

  o.index = function(params){
    params = params || {};
    var query = "admin/orders.json";

    var promise = $http.get(query, {params: params})
    .success(function(data){
      angular.copy(data.orders, o.orders);
      angular.copy(data.permissions, o.list_permissions);
      o.pageConfig.totalItems = data.total_items; o.pageConfig.currentPage = data.current_page; o.pageConfig.itemsPerPage = data.items_per_page;
    });

    return promise;
  };

  o.destroy = function(order){
    return $http.delete("admin/orders/" + order.id + ".json").success(function(){
      var index = o.orders.indexOf(order);
      o.orders.splice(index, 1);
    });
  };

  o.create = function(order){
    if(order.customer){
      order.customer_id = order.customer.id;
    }

    return $http.post("admin/orders.json", {
      order: order,
    }).success(function(data){
      data.line_items_attributes = order.line_items_attributes;
      angular.merge(order, data);
    });
  };
  
  o.update = function(order){
    if(order.customer){
      order.customer_id = order.customer.id;
    }

    return $http.put("admin/orders/" + order.id + ".json", {
      order: order,
    }).success(function(data){
      data.line_items_attributes = order.line_items_attributes;
      angular.merge(order, data);
    });
  };

  o.update_note = function(order){
    return $http.put("admin/orders/" + order.id + "/update_note.json", {
      order: order,
    }).success(function(data){
      data.line_items_attributes = order.line_items_attributes;
      angular.merge(order, data);
    });
  };

  o.save = function(order){
    if(!order.id){
      return o.create(order);
    }
    else{
      return o.update(order);
    }
  };

  o.show = function(id){
    return $http.get("admin/orders/" + id + ".json").success(function(data){
      angular.copy(data, o.order);
    });
  };

  o.checkStock = function(variant){
    return $http.get("admin/variants/" + variant.id + "/check_stock.json");
  };

  o.ship = function(order, params){
    return $http.put("admin/orders/" + order.number + "/ship.json", {
      order: params,
    }).success(function(data){
      angular.merge(order, data);
    });
  };

  o.received = function(order, params){
    return $http.put("admin/orders/" + order.number + "/received.json", {
      order: params,
    }).success(function(data){
      angular.merge(order, data);
    });
  };

  o.refund = function(order, params){
    return $http.put("admin/orders/" + order.number + "/refund.json", {
      order: params,
    }).success(function(data){
      angular.merge(order, data);
    });
  };

  return o;
}]);
