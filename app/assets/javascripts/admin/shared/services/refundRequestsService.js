app.factory('refundRequestsService', ['$http', function($http){
  var o = {
    refund_requests: [],
    refund_request: {},
    list_permissions: {},
    pageConfig: {
      totalItems: 0,
      currentPage: 1,
      itemsPerPage: 10,
    }
  }

  o.index = function(params){
    params = params || {};
    var query = "admin/refund_requests.json";

    var promise = $http.get(query, {params: params})
    .success(function(data){
      angular.copy(data.refund_requests, o.refund_requests);
      angular.copy(data.permissions, o.list_permissions);
      o.pageConfig.totalItems = data.total_items; o.pageConfig.currentPage = data.current_page; o.pageConfig.itemsPerPage = data.items_per_page;
    });

    return promise;
  };

  o.accept = function(refund_request){
    return $http.put("admin/refund_requests/" + refund_request.id + "/accept.json")
      .success(function(data){
        angular.merge(refund_request, data)
    });
  };

  o.deny = function(refund_request){
    return $http.put("admin/refund_requests/" + refund_request.id + "/deny.json")
      .success(function(data){
        angular.merge(refund_request, data)
    });
  };

  return o;
}]);
