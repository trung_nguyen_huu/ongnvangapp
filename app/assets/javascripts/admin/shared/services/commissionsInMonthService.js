app.factory('commissionsInMonthService', ['$http', function($http){
  var o = {
    commissions_in_month: [],
    commission_in_month: {},
    list_permissions: {},
    pageConfig: {
      totalItems: 0,
      currentPage: 1,
      itemsPerPage: 10,
    }
  }

  o.index = function(params, user){
    params = params || {};
    var url = "admin/staffs/" + user.id + "/commissions_in_month.json";

    var promise = $http.get(url, {params: params})
    .success(function(data){
      angular.copy(data.commissions_in_month, o.commissions_in_month);
      angular.copy(data.permissions, o.list_permissions);
      o.pageConfig.totalItems = data.total_items; o.pageConfig.currentPage = data.current_page; o.pageConfig.itemsPerPage = data.items_per_page;
    });

    return promise;
  };

  o.show = function(id, staff_id){
    var url = "admin/staffs/" + staff_id + "/commissions_in_month/" + id + ".json";
    return $http.get(url).success(function(data){
      angular.copy(data, o.commission_in_month);
    });
  };

  o.createPay = function(commission_pay, commission_in_month){
    var url = "admin/commissions_in_month/" + commission_in_month.id + "/commission_pays.json";
    return $http.post(url, {
      commission_pay: commission_pay,
    }).success(function(result){
      angular.extend(commission_pay, result);
    });
  };

  o.updatePay = function(commission_pay, commission_in_month){
    var url = "admin/commissions_in_month/" + commission_in_month.id + "/commission_pays/" + commission_pay.id + ".json";
    return $http.put(url, {
      commission_pay: commission_pay,
    }).success(function(result){
      angular.extend(commission_pay, result);
    });
  };

  o.destroyPay = function(commission_pay, commission_in_month){
    var url = "admin/commissions_in_month/" + commission_in_month.id + "/commission_pays/" + commission_pay.id + ".json";
    return $http.delete(url);
  };

  // o.commissions_of_current_user = function(params){
  //   params = params || {};
  //   var url = "admin/commissions/commissions_of_current_user.json";
  //
  //   var promise = $http.get(url, {params: params})
  //   .success(function(data){
  //     angular.copy(data.commissions, o.commissions);
  //     angular.copy(data.permissions, o.list_permissions);
  //     o.pageConfig.totalItems = data.total_items; o.pageConfig.currentPage = data.current_page; o.pageConfig.itemsPerPage = data.items_per_page;
  //   });
  //
  //   return promise;
  // };
  //
  // o.create = function(commission, user){
  //   var url = "admin/users/" + user.id + "/commissions.json";
  //   return $http.post(url, {
  //     commission: commission,
  //   }).success(function(result){
  //     angular.extend(commission, result);
  //   });
  // };
  //
  // o.update = function(commission, user){
  //   var url = "admin/users/" + user.id + "/commissions/" + commission.id + ".json";
  //   return $http.put(url, {
  //     commission: commission,
  //   }).success(function(result){
  //     angular.extend(commission, result);
  //   });
  // };
  //
  //
  // o.destroy = function(commission, user){
  //   var url = "admin/users/" + user.id + "/commissions/" + commission.id + ".json";
  //   return $http.delete(url).success(function(){
  //     var index = o.commissions.indexOf(commission);
  //     o.commissions.splice(index, 1);
  //   });
  // };

  return o;
}]);
