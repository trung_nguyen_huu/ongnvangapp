app.factory('otherItemsService', ['$http', function($http){
  var o = {
    other_items: [],
    allOtherItems: [],
    other_item: {},
    pageConfig: {
      totalItems: 0,
      currentPage: 1,
      itemsPerPage: 10,
    }
  }

  o.index = function(params){
    params = params || {};
    var query = "admin/other_items.json";

    var promise = $http.get(query, {params: params})
    .success(function(data){
      angular.copy(data.other_items, o.other_items);
      o.pageConfig.totalItems = data.total_items; o.pageConfig.currentPage = data.current_page; o.pageConfig.itemsPerPage = data.items_per_page;
    });

    return promise;
  };

  o.create = function(other_item){
    return $http.post("admin/other_items.json", {
      other_item: other_item,
    });
  };
  
  o.update = function(other_item){
    return $http.put("admin/other_items/" + other_item.id + ".json", {
      other_item: other_item,
    });
  };

  o.show = function(id){
    return $http.get("admin/other_items/" + id + ".json").success(function(data){
      angular.copy(data, o.other_item);
    });
  };

  o.destroy = function(other_item){
    return $http.delete("admin/other_items/" + other_item.id + ".json").success(function(){
      var index = o.other_items.indexOf(other_item);
      o.other_items.splice(index, 1);
    });
  };

  o.all = function(){
    return $http.get("admin/other_items/all.json").success(function(data){
      angular.copy(data, o.allOtherItems);
    });
  }

  return o;
}]);
