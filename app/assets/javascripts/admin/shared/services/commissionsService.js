app.factory('commissionsService', ['$http', function($http){
  var o = {
    commission: {},
    list_permissions: {},
  }

  o.create = function(commission, order){
    var url = "admin/orders/" + order.id + "/commissions.json";
    return $http.post(url, {
      commission: commission,
    }).success(function(result){
      angular.extend(commission, result);
    });
  };

  o.update = function(commission, order){
    var url = "admin/orders/" + order.id + "/commissions/" + commission.id + ".json";
    return $http.put(url, {
      commission: commission,
    }).success(function(result){
      angular.extend(commission, result);
    });
  };

  o.destroy = function(commission, order){
    var url = "admin/orders/" + order.id + "/commissions/" + commission.id + ".json";
    return $http.delete(url)
  };

  return o;
}]);
