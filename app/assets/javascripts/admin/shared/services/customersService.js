app.factory('customersService', ['$http', function($http){
  var o = {
    customers: [],
    customer: {},
    list_permissions: {},
    pageConfig: {
      totalItems: 0,
      currentPage: 1,
      itemsPerPage: 10,
    }
  }

  o.index = function(params){
    params = params || {};
    var query = "admin/customers.json";

    var promise = $http.get(query, {params: params})
    .success(function(data){
      angular.copy(data.customers, o.customers);
      angular.copy(data.permissions, o.list_permissions);
      o.pageConfig.totalItems = data.total_items; o.pageConfig.currentPage = data.current_page; o.pageConfig.itemsPerPage = data.items_per_page;
    });

    return promise;
  };

  o.destroy = function(customer){
    return $http.delete("admin/customers/" + customer.id + ".json").success(function(){
      var index = o.customers.indexOf(customer);
      o.customers.splice(index, 1);
    });
  };

  o.create = function(customer){
    return $http.post("admin/customers.json", {
      customer: customer,
    }).success(function(data){
      angular.extend(data, customer);
    });
  };
  
  o.update = function(customer){
    return $http.put("admin/customers/" + customer.id + ".json", {
      customer: customer,
    }).success(function(data){
      angular.extend(data, customer);
    });
  };

  o.show = function(id){
    return $http.get("admin/customers/" + id + ".json").success(function(data){
      angular.copy(data, o.customer);
    });
  };

  return o;
}]);
