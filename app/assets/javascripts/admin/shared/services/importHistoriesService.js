app.factory('importHistoriesService', ['$http', function($http){
  var o = {
    import_histories: [],
    import_history: {
      import_items_attributes: [],
      other_import_items_attributes: [],
      delivery_fee_total: 0,
    },
    list_permissions: {},
    pageConfig: {
      totalItems: 0,
      currentPage: 1,
      itemsPerPage: 10,
    }
  }

  o.clearData = function(){
    o.import_history = {
      import_items_attributes: [],
      other_import_items_attributes: [],
      delivery_fee_total: 0,
    };
  };

  o.index = function(params){
    params = params || {};
    var query = "admin/import_histories.json";

    var promise = $http.get(query, {params: params})
    .success(function(data){
      angular.copy(data.import_histories, o.import_histories);
      angular.copy(data.permissions, o.list_permissions);
      o.pageConfig.totalItems = data.total_items; o.pageConfig.currentPage = data.current_page; o.pageConfig.itemsPerPage = data.items_per_page;
    });

    return promise;
  };

  o.create = function(import_history){
    return $http.post("admin/import_histories.json", {
      import_history: import_history,
    }).success(function(data){
      // angular.extend(o.import_history, data);
      angular.extend(import_history, data);
    });
  };
  
  o.update = function(import_history){
    return $http.put("admin/import_histories/" + import_history.id + ".json", {
      import_history: import_history,
    }).success(function(data){
      angular.merge(import_history, data);
    });
  };

  o.show = function(id){
    return $http.get("admin/import_histories/" + id + ".json").success(function(data){
      angular.merge(o.import_history, data);
    });
  };

  o.destroy = function(import_history){
    return $http.delete("admin/import_histories/" + import_history.id + ".json").success(function(){
      var index = o.import_histories.indexOf(import_history);
      o.import_histories.splice(index, 1);
    });
  };

  o.export_prices = function(import_history){
    return $http.put("admin/import_histories/" + import_history.id + "/export_prices.json");
  };

  return o;
}]);
