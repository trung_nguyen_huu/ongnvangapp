app.factory('staffsService', ['$http', function($http){
  var o = {
    allStaffs: [],
    staffs: [],
    staff: {},
    roles: [],
    pageConfig: {
      totalItems: 0,
      currentPage: 1,
      itemsPerPage: 10,
    }
  }

  o.index = function(params){
    params = params || {};
    var query = "admin/staffs.json";

    var promise = $http.get(query, {params: params})
    .success(function(data){
      angular.copy(data.staffs, o.staffs);
      o.pageConfig.totalItems = data.total_items; o.pageConfig.currentPage = data.current_page; o.pageConfig.itemsPerPage = data.items_per_page;
    });

    return promise;
  };

  o.destroy = function(staff){
    return $http.delete("admin/staffs/" + staff.id + ".json").success(function(){
      var index = o.staffs.indexOf(staff);
      o.staffs.splice(index, 1);
    });
  };

  o.create = function(staff){
    return $http.post("admin/staffs.json", {
      staff: staff,
    }).success(function(data){
      angular.merge(staff, data);
    });
  };
  
  o.update = function(staff){
    return $http.put("admin/staffs/" + staff.id + ".json", {
      staff: staff,
    }).success(function(data){
      angular.merge(staff, data);
    });
  };

  o.show = function(id){
    return $http.get("admin/staffs/" + id + ".json").success(function(data){
      angular.copy(data, o.staff);
    });
  };

  o.all = function(params){
    params = params || {};
    return $http.get("admin/staffs/all.json?", {params: params}).success(function(data){
      angular.copy(data, o.allStaffs);
    });
  };

  o.get_roles = function(){
    return $http.get("admin/staffs/roles.json").success(function(data){
      angular.copy(data, o.roles);
    });
  };

  return o;
}]);
