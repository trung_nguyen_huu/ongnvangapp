app.factory('productsService', ['$http', function($http){
  var o = {
    products: [],
    allProducts: [],
    allVariants: [],
    list_permissions: {},
    product: {},
    pageConfig: {
      totalItems: 0,
      currentPage: 1,
      itemsPerPage: 10,
    }
  }

  o.getListProducts = function(params){
    params = params || {};
    var query = "admin/products.json";

    var promise = $http.get(query, {params: params})
    .success(function(data){
      angular.copy(data.products, o.products);
      angular.copy(data.permissions, o.list_permissions);
      o.pageConfig.totalItems = data.total_items; o.pageConfig.currentPage = data.current_page; o.pageConfig.itemsPerPage = data.items_per_page;
    });

    return promise;
  };

  o.all = function(){
    return $http.get("admin/products/all.json").success(function(data){
      angular.copy(data.products, o.allProducts);
    });
  };

  o.delete = function(product){
    return $http.delete("admin/products/" + product.id + ".json").success(function(){
      var index = o.products.indexOf(product);
      o.products.splice(index, 1);
    });
  };

  o.create = function(product){
    return $http.post("admin/products.json", {
      product: product,
    });
  };
  
  o.update = function(product){
    return $http.put("admin/products/" + product.id + ".json", {
      product: product,
    });
  };

  o.show = function(id){
    return $http.get("admin/products/" + id + ".json").success(function(data){
      angular.copy(data, o.product);
    });
  };


  o.getAllVariants = function(){
    return $http.get("admin/variants/all.json").success(function(data){
      angular.copy(data, o.allVariants);
    });
  }
  return o;
}]);
