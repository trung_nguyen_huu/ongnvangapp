app.factory('inventoriesService', ['$http', function($http){
  var o = {
    inventories: [],
    inventory: {},
    summary: {
      tong_von_ton_kho: 0,
      tong_gia_tri_ton_kho: 0,
      so_luong_ton_kho: 0,
    },
    pageConfig: {
      totalItems: 0,
      currentPage: 1,
      itemsPerPage: 10,
    },
    list_permissions: {},
  }

  o.index = function(params){
    params = params || {};
    var query = "admin/inventories.json";

    var promise = $http.get(query, {params: params})
    .success(function(data){
      angular.copy(data.inventories, o.inventories);
      angular.copy(data.permissions, o.list_permissions);

      o.summary.tong_von_ton_kho = data.tong_von_ton_kho;
      o.summary.tong_gia_tri_ton_kho = data.tong_gia_tri_ton_kho;
      o.summary.so_luong_ton_kho = data.so_luong_ton_kho;

      o.pageConfig.totalItems = data.total_items;
      o.pageConfig.currentPage = data.current_page;
      o.pageConfig.itemsPerPage = data.items_per_page;
    });

    return promise;
  };

  o.show = function(id){
    return $http.get("admin/inventories/" + id + ".json").success(function(data){
      angular.copy(data, o.inventory);
    });
  };

  return o;
}]);
