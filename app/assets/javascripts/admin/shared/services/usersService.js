app.factory('usersService', ['$http', '$q', function($http, $q){
  var o = {
  }

  o.getCurrentUser = function(id){
    if(!o.currentUser){
      return $http.get("admin/users/get_current_user.json").success(function(data){
        o.currentUser = {};
        angular.copy(data, o.currentUser);
      });
    }

    var deferred = $q.defer();
    deferred.resolve(o.currentUser);
    return deferred.promise;
  };

  o.changePassword = function(user){
    return $http.post("admin/users/update_password.json", {
      user: user,
    });
  };

  o.update_info = function(user){
    return $http.post("admin/users/update_info.json", {
      user: user,
    });
  };
  return o;
}]);
