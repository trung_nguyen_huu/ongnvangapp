var modules = ["ui.router",
              'templates',
              'ui.bootstrap',
              'ui.select',
              'angucomplete-alt',
              'angularSpinner',
              'angularSmartconfirm',
              'ngMessages',
              'ngMask',
              'localytics.directives',
              'xeditable',
              'ngAnimate',
              'ui-notification',
              'angular-loading-bar',
              'Devise',
              'highcharts-ng',
              'maskMoney',
              'checklist-model',
              ];

var app = angular.module('ongVangNHTApp', modules);

app.config(['$httpProvider', function($httpProvider) {
	$httpProvider.defaults.paramSerializer = '$httpParamSerializerJQLike';
}]);

app.config(['usSpinnerConfigProvider', function (usSpinnerConfigProvider) {
    usSpinnerConfigProvider.setDefaults({color: '#f7ed0d'});
}]);

app.run(['editableOptions', function(editableOptions) {
  editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
}]);

app.config(['NotificationProvider', function(NotificationProvider) {
        NotificationProvider.setOptions({
            startTop: 120,
        });
    }]);


app.config(['highchartsNGProvider', function (highchartsNGProvider) {
    highchartsNGProvider.lazyLoad();// will load hightcharts (and standalone framework if jquery is not present) from code.hightcharts.com
  }]);


app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$stateProvider
		.state("listingProducts", {
			url: "/products",
			templateUrl: 'admin/products/index/_index.html',
			controller: 'ProductsCtrl',
                        resolve: {
                          products: ['productsService', function(productsService){
                            return productsService.getListProducts();
                          }],
                          allProducts: ['productsService', function(productsService){
                            return productsService.all();
                          }],
                        }
		})
		.state("newProduct", {
			url: "/products/new",
			templateUrl: 'admin/products/new/_new.html',
			controller: 'newProductCtrl',
		})
		.state("editProduct", {
                  url: "/products/{id}/edit",
			templateUrl: 'admin/products/edit/_edit.html',
			controller: 'editProductCtrl',
                        resolve: {
                          products: ['productsService', '$stateParams', function(productsService, $stateParams){
                            return productsService.show($stateParams.id);
                          }],
                        }
		})

		.state("listingCustomers", {
			url: "/customers",
			templateUrl: 'admin/customers/index/_index.html',
			controller: 'CustomersCtrl',
                        resolve: {
                          products: ['customersService', function(customersService){
                            return customersService.index();
                          }],
                          allStaffs: ['staffsService', function(staffsService){
                            return staffsService.all();
                          }],
                        }
		})
		.state("newCustomer", {
			url: "/customers/new",
			templateUrl: 'admin/customers/new/_new.html',
			controller: 'newCustomerCtrl',
		})
		.state("editCustomer", {
                  url: "/customers/{id}/edit",
			templateUrl: 'admin/customers/edit/_edit.html',
			controller: 'editCustomerCtrl',
                        resolve: {
                          products: ['customersService', '$stateParams', function(customersService, $stateParams){
                            return customersService.show($stateParams.id);
                          }],
                        }
		})

                //suppliers
		.state("listingSuppliers", {
			url: "/suppliers",
			templateUrl: 'admin/suppliers/index/_index.html',
			controller: 'SuppliersCtrl',
                        resolve: {
                          products: ['suppliersService', function(suppliersService){
                            return suppliersService.index();
                          }],
                          all_products: ['productsService', function(productsService){
                            return productsService.all();
                          }],
                        }
		})
		.state("newSupplier", {
			url: "/suppliers/new",
			templateUrl: 'admin/suppliers/new/_new.html',
			controller: 'newSupplierCtrl',
                        resolve: {
                          all_products: ['productsService', function(productsService){
                            return productsService.all();
                          }],
                        }
		})
		.state("editSupplier", {
                  url: "/suppliers/{id}/edit",
			templateUrl: 'admin/suppliers/edit/_edit.html',
			controller: 'editSupplierCtrl',
                        resolve: {
                          products: ['suppliersService', '$stateParams', function(suppliersService, $stateParams){
                            return suppliersService.show($stateParams.id);
                          }],
                          all_products: ['productsService', function(productsService){
                            return productsService.all();
                          }],
                        }
		})

                //import histories
		.state("listingImportHistories", {
			url: "/import_histories",
			templateUrl: 'admin/import_histories/index/_index.html',
			controller: 'ImportHistoriesCtrl',
                        resolve: {
                          import_histories: ['importHistoriesService', function(importHistoriesService){
                            return importHistoriesService.index();
                          }],
                        }
		})
		.state("newImportHistory", {
			url: "/import_histories/new",
			templateUrl: 'admin/import_histories/new/_new.html',
			controller: 'newImportHistoryCtrl',
                        resolve: {
                          allVariants: ['productsService', function(productsService){
                            return productsService.getAllVariants();
                          }],
                          allSuppliers: ['suppliersService', function(suppliersService){
                            return suppliersService.all();
                          }],
                          allOtherItems: ['otherItemsService', function(otherItemsService){
                            return otherItemsService.all();
                          }],
                          import_history: ['importHistoriesService', function(importHistoriesService){
                            importHistoriesService.clearData();
                            return importHistoriesService.import_history;
                          }],

                        }
		})
		.state("editImportHistory", {
                  url: "/import_histories/{id}/edit",
			templateUrl: 'admin/import_histories/new/_new.html',
			controller: 'newImportHistoryCtrl',
                        resolve: {
                          allVariants: ['productsService', function(productsService){
                            return productsService.getAllVariants();
                          }],
                          allSuppliers: ['suppliersService', function(suppliersService){
                            return suppliersService.all();
                          }],
                          import_history: ['importHistoriesService', '$stateParams',
                          function(importHistoriesService, $stateParams){
                            return importHistoriesService.show($stateParams.id);
                          }],
                          allOtherItems: ['otherItemsService', function(otherItemsService){
                            return otherItemsService.all();
                          }],
                        }
		})

                //orders
		.state("listingOrders", {
			url: "/orders",
			templateUrl: 'admin/orders/index/_index.html',
			controller: 'OrdersCtrl',
                        resolve: {
                          orders: ['ordersService', function(ordersService){
                            return ordersService.index();
                          }],
                          allStaffs: ['staffsService', function(staffsService){
                            return staffsService.all();
                          }],
                        }
		})
		.state("newOrder", {
			url: "/orders/new",
			templateUrl: 'admin/orders/new/_new.html',
			controller: 'newOrderCtrl',
                        resolve: {
                          allVariants: ['productsService', function(productsService){
                            return productsService.getAllVariants();
                          }],
                          allStaffs: ['staffsService', function(staffsService){
                            var q = {
                              q: {
                                with_deleted: true
                              }
                            };
                            return staffsService.all(q);
                          }]
                        }
		})
		.state("editOrder", {
                  url: "/orders/{id}/edit",
			templateUrl: 'admin/orders/new/_new.html',
			controller: 'newOrderCtrl',
                        resolve: {
                          allVariants: ['productsService', function(productsService){
                            return productsService.getAllVariants();
                          }],
                          order: ['ordersService', '$stateParams', function(ordersService, $stateParams){
                            return ordersService.show($stateParams.id);
                          }],
                          allStaffs: ['staffsService', function(staffsService){
                            var q = {
                              q: {
                                with_deleted: true
                              }
                            };
                            return staffsService.all(q);
                          }],
                        }
		})

                //profile
		.state("profile", {
                  url: "/profile",
			templateUrl: 'admin/users/profile/_profile.html',
			controller: 'profileCtrl',
                        resolve: {
                          currentUser: ['usersService', function(usersService){
                            return usersService.getCurrentUser();
                          }]
                        }
		})

                //dashboard
		.state("dashboard", {
                  url: "/dashboard",
			templateUrl: 'admin/dashboard/_dashboard.html',
                        controller: 'dashboardCtrl',
                        resolve: {
                          allData: ['dashboardService', function(dashboardService){
                            return dashboardService.getAll();
                          }]
                        }
		})

                //staffs
		.state("listingStaffs", {
			url: "/staffs",
			templateUrl: 'admin/staffs/index/_index.html',
			controller: 'staffsCtrl',
                        resolve: {
                          staffs: ['staffsService', function(staffsService){
                            return staffsService.index();
                          }],
                          roles: ['staffsService', function(staffsService){
                            return staffsService.get_roles();
                          }]
                        }
		})

		.state("newStaff", {
			url: "/staffs/new",
			templateUrl: 'admin/staffs/new/_new.html',
			controller: 'newStaffCtrl',
                        resolve: {
                          roles: ['staffsService', function(staffsService){
                            return staffsService.get_roles();
                          }]
                        }
		})

		.state("editStaff", {
                  url: "/staffs/{id}/edit",
			templateUrl: 'admin/staffs/edit/_edit.html',
			controller: 'editStaffCtrl',
                        resolve: {
                          staff: ['staffsService', '$stateParams', 'commissionsInMonthService',
                          function(staffsService, $stateParams, commissionsInMonthService){
                            return staffsService.show($stateParams.id).then(function(result){
                              return commissionsInMonthService.index({}, staffsService.staff);
                            });
                          }],
                          roles: ['staffsService', function(staffsService){
                            return staffsService.get_roles();
                          }],
                        }
		})

		.state("commissionInMonthDetail", {
                  url: "/nhan-vien/{staff_id}/tien-hoa-hong/{id}",
			templateUrl: 'admin/commissions_in_month/details/_details.html',
			controller: 'commissionInMonthDetailCtrl',
                        resolve: {
                          commission_in_month: ['$stateParams', 'commissionsInMonthService',
                          function($stateParams, commissionsInMonthService){
                            return commissionsInMonthService.show($stateParams.id, $stateParams.staff_id);
                          }],
                        }
		})

                //commissions
		.state("listingCommissions", {
			url: "/tien-hoa-hong/",
			templateUrl: 'admin/commissions/staff/_index.html',
			controller: 'staffCommissionsCtrl',
                        resolve: {
                          commissions: ['commissionsService', function(commissionsService){
                            return commissionsService.commissions_of_current_user({});
                          }],
                        }
		})

		.state("listingInventories", {
			url: "/quan-ly-ton-kho",
			templateUrl: 'admin/inventories/index/_index.html',
			controller: 'InventoriesCtrl',
                        resolve: {
                          products: ['inventoriesService', function(inventoriesService){
                            return inventoriesService.index();
                          }],
                          allProducts: ['productsService', function(productsService){
                            return productsService.all();
                          }],
                        }
		})

		.state("listingRefundRequests", {
			url: "/refund_requests",
			templateUrl: 'admin/refund_requests/index/_index.html',
			controller: 'RefundRequestsCtrl',
                        resolve: {
                          refund_requests: ['refundRequestsService',
                            function(refundRequestsService){
                            return refundRequestsService.index();
                          }],
                        }
		});

	$urlRouterProvider.otherwise('/dashboard');

}]);
