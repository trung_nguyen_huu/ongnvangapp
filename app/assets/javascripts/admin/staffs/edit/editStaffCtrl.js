app.controller('editStaffCtrl', ['$scope', 'staffsService', '$log', '$state', 'Notification',
function($scope, staffsService, $log, $state, Notification){
  $scope.staff = staffsService.staff;
  $scope.roles = staffsService.roles;

  $scope.update = function(invalid){
    if(invalid){
      return;
    }

    $scope.staff.isCreating = true;
    staffsService.update($scope.staff).success(function(){
      $state.go('listingStaffs');
    })
    .error(function(errors){
      angular.forEach(errors, function(error){
        Notification.error(error);
      });
    })
    .finally(function(){
      $scope.staff.isCreating = false;
    });
  };

  $scope.cancel = function(){
    $state.go('listingStaffs');
  };
}]);
