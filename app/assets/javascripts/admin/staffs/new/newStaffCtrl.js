app.controller('newStaffCtrl', ['$scope', 'staffsService', '$log', '$state', 'Notification',
function($scope, staffsService, $log, $state, Notification){
  $scope.staff = {};
  $scope.roles = staffsService.roles;

  $scope.create = function(invalid){
    if(invalid){
      return;
    }

    $scope.staff.isCreating = true;
    staffsService.create($scope.staff).success(function(){
      $state.go('listingStaffs');
    })
    .error(function(errors){
      angular.forEach(errors, function(error){
        Notification.error(error);
      });
    })
    .finally(function(){
      $scope.staff.isCreating = false;
    });
  };

  $scope.cancel = function(){
    $state.go('listingStaffs');
  };
}]);
