app.controller('staffsCtrl', ['$scope', 'staffsService', '$log', '$state', 'Notification',
function($scope, staffsService, $log, $state, Notification){

  $scope.staffs = staffsService.staffs;
  $scope.roles = staffsService.roles;
  $scope.pageConfig = staffsService.pageConfig;


  $scope.pageChanged = function() {
    staffsService.index(getParams());
  };

  $scope.q = {};
  $scope.search = function(invalid){
    console.log("in search");
    if(invalid){
      return;
    }

    $scope.pageConfig.currentPage = 1;
    staffsService.index(getParams());
  };

  $scope.loadController = true;
  $scope.$watch('q', function(){
    if($scope.loadController == true){
      $scope.loadController = false;
      return;
    }

    $scope.search(false);
  }, true);

  $scope.clearSearch = function(){
    angular.copy({}, $scope.q);
    $scope.search();
  };

  $scope.delete = function(staff){
    staffsService.destroy(staff);
  };

  $scope.is_admin = function(staff){
    return _.some(staff.roles, function(role){
      return role.name == 'admin';
    });
  }

  function getParams(){
    return {
      page: $scope.pageConfig.currentPage,
      q: $scope.q
    };
  };

}]);
