json.extract! @current_user, :id, :email, :name, :created_at, :updated_at

json.role @current_user.highest_role
json.is_subadmin @current_user.subadmin?

json.permissions do 
  if current_user.seller?
    json.can_read_dashboard true
    json.can_read_order true
    json.can_read_product true
    json.can_read_customer true
    json.can_read_import_history false
    json.can_read_supplier false
    json.can_read_staff false
    json.can_read_commission true
    json.can_read_inventory false
    json.can_read_refund_request false
  end

  if current_user.subadmin?
    json.can_read_dashboard true
    json.can_read_order true
    json.can_read_product true
    json.can_read_customer true
    json.can_read_import_history true
    json.can_read_supplier false
    json.can_read_staff true
    json.can_read_commission true
    json.can_read_inventory false
    json.can_read_refund_request false
  end

  if current_user.admin?
    json.can_read_dashboard true
    json.can_read_order true
    json.can_read_product true
    json.can_read_customer true
    json.can_read_import_history true
    json.can_read_supplier true
    json.can_read_staff true
    json.can_read_commission false
    json.can_read_inventory true
    json.can_read_refund_request true
  end
end

json.number_of_not_examined_refund_requests NhtRequest.refund_requests.not_examined.count 
