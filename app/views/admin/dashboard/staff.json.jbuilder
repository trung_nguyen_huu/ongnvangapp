json.staff do
  json.number_of_orders_by_staff do 
    json.x_axis @number_of_orders_by_staff[:x_axis]
    if current_user.admin?
      json.series @number_of_orders_by_staff[:series]
    elsif current_user.staff?
      json.series @number_of_orders_by_staff[:series].select { |item| item[:name] == current_user.name }
    end
  end

  json.discount_amounts_by_staff do 
    json.x_axis @discount_amounts_by_staff[:x_axis]
    if current_user.admin?
      json.series @discount_amounts_by_staff[:series]
    elsif current_user.staff?
      json.series @discount_amounts_by_staff[:series].select { |item| item[:name] == current_user.name }
    end
  end

  if current_user.admin?
    json.debit_total_by_staff do 
      json.x_axis @debit_total_by_staff[:x_axis]
      if current_user.admin?
        json.series @debit_total_by_staff[:series]
      elsif current_user.staff?
        json.series @debit_total_by_staff[:series].select { |item| item[:name] == current_user.name }
      end
    end
  end

  json.number_of_selled_products_by_staff do 
    json.x_axis @number_of_selled_products_by_staff[:x_axis]
    if current_user.admin?
      json.series @number_of_selled_products_by_staff[:series]
    elsif current_user.staff?
      json.series @number_of_selled_products_by_staff[:series].select { |item| item[:name] == current_user.name }
    end
  end
end
