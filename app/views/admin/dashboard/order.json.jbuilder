json.order do
  json.number_of_orders do 
    json.x_axis @number_of_orders[:x_axis]
    json.data @number_of_orders[:data]
  end

  json.total_of_all_orders do 
    json.x_axis @total_of_all_orders[:x_axis]
    json.data @total_of_all_orders[:data]
  end

  json.total_of_debit do 
    json.x_axis @total_of_debit[:x_axis]
    json.data @total_of_debit[:data]
  end

  json.total_of_origin_price do 
    json.x_axis @total_of_origin_price[:x_axis]
    json.data @total_of_origin_price[:data]
  end
end
