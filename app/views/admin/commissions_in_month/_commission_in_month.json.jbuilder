json.extract! commission_in_month, :id, :created_at, :updated_at, :number_of_orders,
  :order_total, :commission_total, :pay_total, :ton_cuoi_ky, :number_of_pays, :seller_id

json.in_month commission_in_month.created_at.beginning_of_month 

show_details ||= false
if show_details
  json.commissions commission_in_month.commissions do |commission|
    json.partial! 'admin/commissions/commission', locals: { commission: commission }
    json.note commission.note
    json.order do 
      json.extract! commission.order, :id, :number, :total
    end
  end

  json.seller do 
    json.partial! 'admin/staffs/staff', locals: {staff: commission_in_month.seller}
  end

  json.pays commission_in_month.pays do |pay|
    json.partial! 'admin/commission_pays/commission_pay', locals: {commission_pay: pay}
  end

  json.permissions_for_list_of_pays do 
    json.partial! 'admin/shared/list_permission', locals: {resource_class: PayForCommission}
  end
end
