json.commissions_in_month do
  json.array!(@commissions_in_month) do |commission_in_month|
    json.partial! 'admin/commissions_in_month/commission_in_month', locals: {commission_in_month: commission_in_month}
  end
end

json.total_items @commissions_in_month.total_count
json.current_page @commissions_in_month.current_page
json.items_per_page per_page

json.partial! 'admin/shared/list_permission', locals: {resource_class: CommissionInMonth}
