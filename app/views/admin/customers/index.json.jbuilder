json.customers do
  json.array!(@customers) do |customer|
    json.partial! 'admin/customers/customer', locals: {customer: customer}
  end
end

if @all
  json.total_items @customers.count
  json.current_page 1
  json.items_per_page per_page
else
  json.total_items @customers.total_count
  json.current_page @customers.current_page
  json.items_per_page per_page
end

json.partial! 'admin/shared/list_permission', locals: {resource_class: Customer}
