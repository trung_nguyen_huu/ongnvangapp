json.extract! customer, :id, :number, :name, :phone, :address, :id_number, :email, :facebook, :job, :character, :created_at, :updated_at, :deleted_at
json.description customer.description
json.birthday customer.birthday.strftime("%d-%m-%Y") rescue nil

json.partial! 'admin/shared/permission', locals: {resource: customer}
