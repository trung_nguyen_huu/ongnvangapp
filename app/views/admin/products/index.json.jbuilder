json.products do
  json.array! @products, partial: 'admin/products/product',as: :product
end

json.total_items @products.total_count
json.current_page @products.current_page
json.items_per_page per_page

json.partial! 'admin/shared/list_permission', locals: {resource_class: Product}
