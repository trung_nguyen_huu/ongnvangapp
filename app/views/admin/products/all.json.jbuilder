json.products do
  json.array!(@products) do |product|
    json.extract! product, :id, :name, :list_option_values
  end
end
