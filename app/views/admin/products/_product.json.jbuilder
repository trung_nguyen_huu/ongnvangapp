json.extract! product, :id, :name, :description, :slug, :created_at, :updated_at

json.variants_attributes do
  json.partial! 'admin/variants/variant', collection: product.variants.order(created_at: :asc), as: :variant
end

json.partial! 'admin/shared/permission', locals: {resource: product}
