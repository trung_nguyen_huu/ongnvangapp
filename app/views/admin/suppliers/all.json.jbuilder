json.array!(@suppliers) do |supplier|
  if current_user.admin?
    json.extract! supplier, :id, :name, :coso, :address, :note, :list_phones, :created_at, :updated_at, :slug
  end

  if current_user.subadmin?
    json.extract! supplier, :id, :name
  end
end
