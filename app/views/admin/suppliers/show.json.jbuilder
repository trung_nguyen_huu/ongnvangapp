json.extract! @supplier, :id, :name, :coso, :address, :note, :created_at, :updated_at, :list_phones, :slug
json.products do
  json.partial! partial: 'admin/products/product', collection: @supplier.products, as: :product
end
json.phones_attributes do
  json.partial! partial: 'admin/phones/phone', collection: @supplier.phones, as: :phone
end
