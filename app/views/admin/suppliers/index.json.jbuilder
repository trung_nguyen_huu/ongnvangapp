json.suppliers do
  json.array!(@suppliers) do |supplier|
    json.extract! supplier, :id, :name, :coso, :address, :note, :list_phones, :created_at, :updated_at, :slug
  end
end

json.total_items @suppliers.total_count
json.current_page @suppliers.current_page
json.items_per_page per_page
