json.commissions do
  json.array!(@commissions) do |commission|
    json.partial! 'admin/commissions/commission', locals: {commission: commission}
  end
end

json.total_items @commissions.total_count
json.current_page @commissions.current_page
json.items_per_page per_page

json.partial! 'admin/shared/list_permission', locals: {resource_class: Commission}
