json.inventories @inventories do |inventory|
  json.extract! inventory, :id, :product_id, :name, :so_luong_ton_kho, :tong_gia_tri_ton_kho, :tong_von_ton_kho
end

json.tong_von_ton_kho @manager.tong_von_ton_kho
json.tong_gia_tri_ton_kho @manager.tong_gia_tri_ton_kho
json.so_luong_ton_kho @manager.so_luong_ton_kho

json.total_items @inventories.total_count
json.current_page @inventories.current_page
json.items_per_page per_page

json.partial! 'admin/shared/list_permission', locals: {resource_class: Inventory}
