json.extract! @inventory, :id, :product_id, :name, :so_luong_ton_kho, :tong_gia_tri_ton_kho, :tong_von_ton_kho

json.import_items @inventory.import_items_in_stock do |item|
  json.extract! item, :id, :created_at, :updated_at, :count_in_stock, :quantity
  json.import_history do 
    json.extract! item.import_history, :id, :created_at, :updated_at, :title
  end
end
