json.extract! variant, :id, :product_id, :sku, :option_values, :created_at, :updated_at, :name
json.price variant.price.to_f
json.discount_amount variant.discount_amount.to_f
json.count_in_stock variant.count_in_stock.to_i

json.sale_price variant.sale_price.to_f rescue 0
json.price_with_sale variant.price_with_sale.to_f rescue 0
json.on_sale variant.on_sale?
json.sale_from_date variant.sale_from_date.strftime("%Y-%m-%d") rescue nil
json.sale_to_date variant.sale_to_date.strftime("%Y-%m-%d") rescue nil


if current_user.admin?
  json.cost variant.cost.to_f
  json.newest_cost variant.newest_cost_price.to_f
  json.newest_import_history_id variant.newest_import_history.id rescue nil
end
