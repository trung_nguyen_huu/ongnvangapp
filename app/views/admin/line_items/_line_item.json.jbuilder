json.extract! line_item, :id, :order_id, :variant_id, :created_at, :updated_at
json.quantity line_item.quantity.to_i
json.price line_item.price.to_f
json.discount_amount line_item.discount_amount.to_f
json.origin_price_total line_item.origin_price_total.to_f

json.price_with_sale line_item.price_with_sale.to_f
json.on_sale line_item.on_sale?

json.variant do 
  json.partial! 'admin/variants/variant', locals: {variant: line_item.variant }
end
