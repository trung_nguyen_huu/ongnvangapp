json.partial! 'admin/line_items/line_item', locals: {line_item: @line_item}
json.order do
  json.partial! 'admin/orders/order', locals: {order: @line_item.order, included_line_item: false}
end
