json.extract! import_item, :id, :variant_id, :supplier_id, :import_history_id, :quantity, :count_in_stock, :deleted_at, :created_at, :updated_at

if current_user.admin?
  json.cost_per_item import_item.cost_per_item.to_f
end

json.price_per_item import_item.price_per_item.to_f
json.discount_per_item import_item.discount_per_item.to_f

json.variant do 
  json.id import_item.variant.id
  json.product_id import_item.variant.product_id
  json.name import_item.variant.name
end

json.supplier do
  json.id import_item.supplier.try(:id)
  json.name import_item.supplier.try(:name)
end
