json.partial! 'admin/import_items/import_item', locals: {import_item: @import_item}

json.import_history do 
  json.partial! 'admin/import_histories/import_history', locals: {import_history: @import_item.import_history}
end
