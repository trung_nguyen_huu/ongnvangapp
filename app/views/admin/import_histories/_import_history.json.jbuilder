json.extract! import_history, :id, :deleted_at, :created_at, :updated_at, :title

json.number_of_items import_history.number_of_items.to_i
json.delivery_fee_total import_history.delivery_fee_total.to_f

if current_user.admin?
  json.other_fee_total import_history.other_fee_total.to_f
  json.cost_total import_history.cost_total.to_f
  json.price_total import_history.price_total.to_f
  json.discount_total import_history.discount_total.to_f
  json.debit_total import_history.debit_total.to_f

  json.total_of_all_cost import_history.total_of_all_cost.to_f
end

json.partial! 'admin/shared/permission', locals: {resource: import_history}
