json.import_histories do
  json.partial! partial: 'admin/import_histories/import_history', collection: @import_histories, as: :import_history
end

json.total_items @import_histories.total_count
json.current_page @import_histories.current_page
json.items_per_page per_page

json.partial! 'admin/shared/list_permission', locals: {resource_class: ImportHistory}
