json.partial! 'admin/import_histories/import_history', locals: {import_history: @import_history}

json.import_items_attributes do
  json.partial! partial: 'admin/import_items/import_item', collection: @import_history.import_items.order(updated_at: :desc), as: :import_item
end

json.other_import_items_attributes do
  json.partial! partial: 'admin/other_import_items/other_import_item', collection: @import_history.other_import_items.order(updated_at: :desc), as: :other_import_item
end
