json.extract! other_import_item, :id, :note, :deleted_at, :created_at, :updated_at
json.name other_import_item.other_item.name
json.cost other_import_item.cost.to_f
