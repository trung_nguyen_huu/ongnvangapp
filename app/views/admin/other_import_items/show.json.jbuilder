json.partial! 'admin/other_import_items/other_import_item', locals: {other_import_item: @other_import_item}

json.import_history do 
  json.partial! 'admin/import_histories/import_history', locals: {import_history: @other_import_item.import_history}
end
