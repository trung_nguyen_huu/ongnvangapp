json.staffs do
  json.array!(@staffs) do |staff|
    json.partial! 'admin/staffs/staff', locals: {staff: staff}
  end
end

json.total_items @staffs.total_count
json.current_page @staffs.current_page
json.items_per_page per_page
