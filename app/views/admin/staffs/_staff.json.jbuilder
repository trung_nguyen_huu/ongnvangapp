json.extract! staff, :id, :email, :name, :created_at, :updated_at, :deleted_at, :note
json.role_ids staff.roles.map(&:id)
json.roles do 
  json.array! staff.roles.except_staff, :id, :name, :created_at, :updated_at, :vietnamese_name
end

if current_user.admin?
  json.tong_hoa_hong_trong_thang staff.tong_hoa_hong_trong_thang.to_f
end
