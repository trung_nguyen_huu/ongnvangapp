json.array!(@staffs) do |staff|
  json.extract! staff, :id, :email, :name, :created_at, :updated_at, :deleted_at
end
