json.refund_requests do
  json.array!(@refund_requests) do |refund_request|
    json.partial! 'admin/refund_requests/refund_request', locals: {refund_request: refund_request}
  end
end

json.total_items @refund_requests.total_count
json.current_page @refund_requests.current_page
json.items_per_page per_page

json.partial! 'admin/shared/list_permission', locals: {resource_class: Order}
