json.extract! refund_request, :id, :created_at, :updated_at, :state, :object_id, :object_type, :action, :requester_id, :parameters

json.is_accepted refund_request.accepted?
json.is_waitting refund_request.waitting?
json.is_denied refund_request.denied?

order = refund_request.object
json.order do 
  json.extract! order, :id, :number
end
json.requester do 
  json.partial! 'admin/users/user', locals: {user: refund_request.requester}
end
