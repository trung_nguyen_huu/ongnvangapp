json.orders do
  json.array!(@orders) do |order|
    json.partial! 'admin/orders/order', locals: {order: order, included_line_item: false}
  end
end

json.total_items @orders.total_count
json.current_page @orders.current_page
json.items_per_page per_page

json.partial! 'admin/shared/list_permission', locals: {resource_class: Order}
