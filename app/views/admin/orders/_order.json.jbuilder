json.extract! order, :id, :created_at, :updated_at, :number, :state, :customer_id, :delivery_date, :receive_date, :refund_date, :vietnamese_state, :seller_id, :total_of_wait_time, :note

json.total order.total.to_f
json.item_total order.item_total.to_f
json.discount_amount_total order.discount_amount_total.to_f
json.delivery_fee_total order.delivery_fee_total.to_f
json.other_fee_total order.other_fee_total.to_f
json.origin_price_total order.origin_price_total.to_f
json.debit_total order.debit_total.to_f
json.buyer_pay_for_delivery_fee order.buyer_pay_for_delivery_fee
json.is_sent_refund_request order.sent_refund_request?
json.is_refunded order.refunded?
json.is_refused_refund_request (order.sent_refund_request? && order.refund_requests.first.denied?)

if order.customer
  json.customer do 
    json.partial! 'admin/customers/customer', locals: {customer: order.customer}
  end
end

if order.seller
  json.seller do
    json.partial! 'admin/staffs/staff', locals: {staff: order.seller}
  end
end

if order.commission
  json.commission do 
    json.partial! 'admin/commissions/commission', {commission: order.commission}
  end
end

if included_line_item
  json.line_items_attributes do
    json.partial! partial: 'admin/line_items/line_item', collection: order.line_items.order(updated_at: :desc), as: :line_item
  end
end

json.can_edit_by_current_user order.can_edit_by?(current_user)

json.permissions do 
  json.can_create can?(:create, order)
  json.can_read can?(:read, order)
  json.can_update can?(:update, order)
  json.can_destroy can?(:destroy, order)
  json.can_ship can?(:ship, order)
  json.can_complete can?(:complete, order)
  json.can_refund can?(:refund, order)
end
