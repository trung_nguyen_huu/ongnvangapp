json.permissions do 
  json.can_create can?(:create, resource_class.new)
  json.can_get_excel can?(:get_excel, resource_class)
end
