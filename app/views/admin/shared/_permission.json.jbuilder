json.permissions do 
  json.can_create can?(:create, resource)
  json.can_read can?(:read, resource)
  json.can_update can?(:update, resource)
  json.can_destroy can?(:destroy, resource)
end
