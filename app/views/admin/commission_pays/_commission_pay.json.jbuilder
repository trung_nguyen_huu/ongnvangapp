json.extract! commission_pay, :id, :amount, :note, :description, :created_at, :updated_at

json.partial! 'admin/shared/permission', locals: {resource: commission_pay}
