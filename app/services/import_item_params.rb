class ImportItemParams
  attr_reader :params, :current_user

  def initialize(params, current_user)
    @params = params
    @current_user = current_user
  end

  def by_role
    @result = nil
    if current_user.admin?
      @result = params.permit(:variant_id, :cost_per_item, :price_per_item, :discount_per_item, :supplier_id, :quantity)
    end

    if current_user.subadmin?
      @result = params.permit(:variant_id, :price_per_item, :discount_per_item, :supplier_id, :quantity)
    end

    @result
  end
end
