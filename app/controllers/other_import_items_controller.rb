class OtherImportItemsController < ApplicationController
  before_action :set_other_import_item, only: [:show, :edit, :update, :destroy]

  # GET /other_import_items
  # GET /other_import_items.json
  def index
    @other_import_items = OtherImportItem.all
  end

  # GET /other_import_items/1
  # GET /other_import_items/1.json
  def show
  end

  # GET /other_import_items/new
  def new
    @other_import_item = OtherImportItem.new
  end

  # GET /other_import_items/1/edit
  def edit
  end

  # POST /other_import_items
  # POST /other_import_items.json
  def create
    @other_import_item = OtherImportItem.new(other_import_item_params)

    respond_to do |format|
      if @other_import_item.save
        format.html { redirect_to @other_import_item, notice: 'Other import item was successfully created.' }
        format.json { render :show, status: :created, location: @other_import_item }
      else
        format.html { render :new }
        format.json { render json: @other_import_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /other_import_items/1
  # PATCH/PUT /other_import_items/1.json
  def update
    respond_to do |format|
      if @other_import_item.update(other_import_item_params)
        format.html { redirect_to @other_import_item, notice: 'Other import item was successfully updated.' }
        format.json { render :show, status: :ok, location: @other_import_item }
      else
        format.html { render :edit }
        format.json { render json: @other_import_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /other_import_items/1
  # DELETE /other_import_items/1.json
  def destroy
    @other_import_item.destroy
    respond_to do |format|
      format.html { redirect_to other_import_items_url, notice: 'Other import item was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_other_import_item
      @other_import_item = OtherImportItem.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def other_import_item_params
      params.require(:other_import_item).permit(:name, :cost_price)
    end
end
