module Admin
  class OtherItemsController < BaseController
    before_action :set_other_item, only: [:show, :update, :destroy]

    # GET /other_items.json
    def index
      if params[:q] && params[:q][:with_deleted] == 'true'
        base_scope = OtherItem.with_deleted
      end

      base_scope ||= OtherItem.all
      base_scope = base_scope.ransack(params[:q]).result
      @other_items = base_scope.page(page).per(per_page)
    end

    def all
      @other_items = OtherItem.all.order(name: :asc)
    end

    private
    def set_other_item
      @other_item = OtherItem.with_deleted.find(params[:id])
    end

    def other_item_params
      params.require(:other_item).permit(:name)
    end
  end
end
