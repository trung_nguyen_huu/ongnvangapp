module Admin
  class StaffsController < BaseController
    before_action :set_staff, only: [:show, :update, :destroy]

    def index
      base_scope = filter_by_ability_and_search_params
      @staffs = base_scope.page(page).per(per_page).distinct
    end

    def all
      base_scope = User.except_customer
      if params[:q] && params[:q][:with_deleted] == 'true'
        base_scope = base_scope.with_deleted
      end

      @staffs = base_scope.order(name: :asc).distinct
    end

    # POST /staffs.json
    def create
      @staff = User.new(staff_params)
      authorize!(:create, @staff)

      respond_to do |format|
        if @staff.save
          add_staff_role_for_each_staff_in_NHT()
          format.json { render :show, status: :created }
        else
          format.json { render json: @staff.errors.full_messages, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /staffs/1.json
    def update
      respond_to do |format|
        if @staff.update(staff_params)
          add_staff_role_for_each_staff_in_NHT()
          format.json { render :show, status: :ok }
        else
          format.json { render json: @staff.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /staffs/1.json
    def destroy
      @staff.destroy unless @staff.deleted_at
      respond_to do |format|
        format.json { head :no_content }
      end
    end

    def roles
      @roles = Role.accessible_by(current_ability, :assign_roles).except_customer.except_staff
      respond_to do |format|
        format.json { render :roles, status: :ok }
      end
    end

    private
    def set_staff
      @staff = User.except_customer.with_deleted.find(params[:id])
      authorize!(params[:action].to_sym, @staff)
    end

    def staff_params
      @staff_params = params.require(:staff).permit(:name, :email, :password, :note, role_ids: [])
      remove_admin_and_subadmin_role unless current_user.admin?
      @staff_params
    end

    def remove_admin_and_subadmin_role
      @staff_params[:role_ids] ||= []
      @staff_params[:role_ids] = @staff_params[:role_ids] - [Role.admin.id, Role.subadmin.id]
    end

    def add_staff_role_for_each_staff_in_NHT
      unless @staff.staff?
        @staff.roles << Role.staff
      end
    end

    def filter_by_ability_and_search_params
      base_scope = filter_by_ability
      if with_deleted_param?
        base_scope = base_scope.with_deleted
      end

      base_scope = base_scope.except_customer.ransack(params[:q]).result.distinct
    end

    def filter_by_ability 
      return User.except_admin_and_subadmin if current_user.subadmin?
      return User.all if current_user.admin?
    end

    def with_deleted_param?
      params[:q] && params[:q][:with_deleted] == 'true'
    end
  end
end
