module Admin
  class ProductsController < BaseController
    before_action :set_product, only: [:show, :update, :destroy]

    # GET /products.json
    def index
      base_scope = filter_by_ability_and_search_params
      @products = base_scope.page(page).per(per_page)
    end

    def get_excel 
      authorize!(:get_execel, Product)

      base_scope = filter_by_ability_and_search_params
      @products = base_scope
      respond_to do |format|
        format.xlsx do 
          render xlsx: "get_excel", filename: "danh-sach-san-pham-#{Time.zone.now.strftime("%d-%m-%Y %H:%M")}.xlsx"
        end
      end
    end

    def search
      base_scope = Product.ransack(name_or_variants_option_values_cont_any: params[:search].split(' ')).result
      @products = base_scope.page(page).per(per_page).uniq
      render 'index.json.jbuilder'
    end

    # POST /products.json
    def create
      @product = Product.new(product_params)
      authorize!(:create, @product)

      respond_to do |format|
        if @product.save
          format.json { render :show, status: :created }
        else
          format.json { render json: @product.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /products/1.json
    def update
      respond_to do |format|
        if @product.update(product_params)
          format.json { render :show, status: :ok }
        else
          format.json { render json: @product.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /products/1.json
    def destroy
      @product.destroy
      respond_to do |format|
        format.json { head :no_content }
      end
    end

    def all
      @products = Product.all
    end

    private
    def set_product
      @product = Product.with_deleted.friendly.find(params[:id])
      authorize!(params[:action].to_sym, @product)
    end

    def filter_by_ability_and_search_params
      base_scope = filter_by_ability
      if with_deleted_param?
        base_scope = base_scope.with_deleted
      end

      base_scope = base_scope.includes(variants: [:import_items]).ransack(params[:q]).result.distinct
    end

    def filter_by_ability 
      Product.accessible_by(current_ability)
    end

    def with_deleted_param?
      params[:q] && params[:q][:with_deleted] == 'true'
    end

    def product_params
      variants_attrs = [:id, :_destroy, :sku, :option_values, :price, :discount_amount, :cost, :sale_price, :sale_from_date, :sale_to_date]
      params.require(:product).permit(:name, :description, variants_attributes: variants_attrs)
    end
  end
end
