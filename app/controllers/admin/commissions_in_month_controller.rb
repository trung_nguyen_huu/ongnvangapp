module Admin
  class CommissionsInMonthController < BaseController
    before_action :set_staff

    #staffs/:staff_id/commissions_in_month.json
    def index
      @commissions_in_month = filter_by_ability_and_search_params.includes(:orders, :commissions, :pays)
                                                                  .order(created_at: :desc)
                                                                  .page(page)
                                                                  .per(per_page).uniq
    end

    def show
      @commission_in_month = @staff.commissions_in_month.find(params[:id])
    end

    private
    def set_staff
      @staff = User.staff.find(params[:staff_id])
    end

    def filter_by_ability_and_search_params
      filter_by_ability.ransack(params[:q]).result
    end

    def filter_by_ability 
      @staff.commissions_in_month
    end
  end
end

