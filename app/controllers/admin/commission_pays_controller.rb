module Admin
  class CommissionPaysController < BaseController
    before_action :find_commission_in_month
    before_action :find_commission_pay, only: [:update, :destroy]

    #POST /commisions_in_month/:commission_in_month_id/commission_pays.json
    def create
      @commission_pay = @commission_in_month.pays.build(commission_pay_params)
      authorize!(:create, @commission_pay)

      respond_to do |format|
        if @commission_pay.save
          format.json { render :show, status: :created }
        else
          format.json { render json: @commission_pay.errors, status: :unprocessable_entity }
        end
      end
    end

    # # PATCH/PUT /commissions/1.json
    def update
      authorize!(:update, @commission_pay)

      respond_to do |format|
        if @commission_pay.update_attributes(commission_pay_params)
          format.json { render :show, status: :ok }
        else
          format.json { render json: @commission_pay.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /commissions/1.json
    def destroy
      authorize!(:destroy, @commission_pay)

      @commission_pay.destroy
      respond_to do |format|
        format.json { render :show, status: :ok }
      end
    end

    private

    def find_commission_in_month
      @commission_in_month ||= CommissionInMonth.find(params[:commissions_in_month_id])
    end

    def find_commission_pay
      @commission_pay = @commission_in_month.pays.find(params[:id])
    end

    def commission_pay_params
      params.require(:commission_pay).permit(:id, :amount, :note, :description)
    end
  end
end

