module Admin
  class UsersController < BaseController
    #users/current_user.json
    def get_current_user
      @current_user = current_user
      respond_to do |format|
        format.json { render :current_user, status: :ok }
      end
    end

    def update_info
      @user = User.find(current_user.id)
      respond_to do |format|
        if @user.update_with_password(user_params)
          # Sign in the user by passing validation in case their password changed
          sign_in @user, :bypass => true
          format.json { render :current_user, status: :ok }
        else
          format.json { render json: @user.errors.full_messages, status: :unprocessable_entity }
        end
      end
    end

    def update_password
      @user = User.find(current_user.id)
      respond_to do |format|
        if @user.update_with_password(user_params)
          # Sign in the user by passing validation in case their password changed
          sign_in @user, :bypass => true
          format.json { render :current_user, status: :ok }
        else
          format.json { render json: @user.errors.full_messages, status: :unprocessable_entity }
        end
      end
    end

    private

    def user_params
      # NOTE: Using `strong_parameters` gem
      params.require(:user).permit(:name, :current_password, :password, :password_confirmation)
    end
  end
end
