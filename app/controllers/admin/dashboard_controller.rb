module Admin
  class DashboardController < BaseController
    def staff
      @staff_chart = Charts::StaffChart.new(params)

      @number_of_orders_by_staff = @staff_chart.number_of_orders_by_staff
      @discount_amounts_by_staff = @staff_chart.discount_amounts_by_staff
      @debit_total_by_staff      = @staff_chart.debit_total_by_staff
      @number_of_selled_products_by_staff = @staff_chart.number_of_selled_products_by_staff
    end

    def order
      @order_chart = Charts::OrderChart.new(params)

      @number_of_orders = @order_chart.number_of_orders_by_state()
      @total_of_all_orders = @order_chart.total_of_all_orders_by_state()
      @total_of_debit = @order_chart.total_of_debit_by_state()
      @total_of_origin_price = @order_chart.total_of_origin_price_by_state()
    end
  end
end
