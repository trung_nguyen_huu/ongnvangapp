module Admin
  class OrdersController < BaseController
    before_action :set_order, only: [:show, :update, :destroy, :ship, :received, :refund, :update_note]

    # GET /orders.json
    def index
      @orders = filter_by_ability_and_search_params.includes(:customer, :seller)
                      .order(updated_at: :desc)
                      .page(page)
                      .per(per_page).uniq
    end


    def get_excel
      authorize!(:get_excel, Order)
      base_scope = filter_by_ability_and_search_params.order(updated_at: :desc).distinct
      @orders = base_scope

      respond_to do |format|
        format.xlsx do 
          render xlsx: "get_excel", filename: "danh-sach-don-hang-#{Time.zone.now.strftime("%d-%m-%Y %H:%M")}.xlsx"
        end
      end
    end

    # POST /orders.json
    def create
      @order = Order.new(order_params)
      authorize!(:create, @order)

      respond_to do |format|
        if @order.save
          @order.created_by(current_user)
          format.json { render :show, status: :created }
        else
          format.json { render json: @order.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /orders/1.json
    def update
      respond_to do |format|
        if @order.update(order_params)
          format.json { render :show, status: :ok }
        else
          format.json { render json: @order.errors, status: :unprocessable_entity }
        end
      end
    end

    def update_note
      respond_to do |format|
        if @order.update(params.require(:order).permit(:note))
          format.json { render :show, status: :ok }
        else
          format.json { render json: @order.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /orders/1.json
    def destroy
      @order.destroy
      respond_to do |format|
        format.json { head :no_content }
      end
    end

    def ship
      @order.ship!(params[:order][:delivery_date])
      respond_to do |format|
        format.json { render :show, status: :ok }
      end
    end

    def received
      @order.received!(params[:order][:received_date])
      respond_to do |format|
        format.json { render :show, status: :ok }
      end
    end

    def refund
      order_refund_params = params.require(:order).permit(:refund_date)
      if current_user.admin?
        @order.refund!(order_refund_params)
      end

      if current_user.subadmin?
        @order.request_refund!(current_user, order_refund_params)
      end

      respond_to do |format|
        format.json { render :show, status: :ok }
      end
    end

    private
    def set_order
      @order ||= Order.friendly.find(params[:id])
      authorize!(params[:action].to_sym, @order)
    end

    def order_params
      params.require(:order).permit(:delivery_fee_total,
                                    :other_fee_total,
                                    :buyer_pay_for_delivery_fee,
                                    :delivery_date,
                                    :receive_date,
                                    :customer_id,
                                    :seller_id,
                                     :note)
    end

    def filter_by_ability_and_search_params
      filter_by_ability.ransack(params[:q]).result
    end

    def filter_by_ability 
      Order.accessible_by(current_ability)
    end
  end
end
