module Admin
  class VariantsController < BaseController
    before_action :set_variant, only: [:show, :edit, :update, :destroy, :check_stock]

    # GET /variants.json
    # product_name_or_option_values_cont_any = ['', '']
    def index
      if params[:q] && params[:q][:with_deleted] == 'true'
        base_scope = Variant.with_deleted
      end

      base_scope ||= Variant.all
      base_scope = base_scope.ransack(params[:q]).result
      @variants = base_scope.page(page).per(per_page)
    end

    def to_xls
    end

    def all
      @variants = Variant.all.order(created_at: :desc)
    end

    # GET /variants/new
    def new
      @variant = Variant.new
    end

    # GET /variants/1/edit
    def edit
    end

    # POST /variants
    # POST /variants.json
    def create
      @variant = Variant.new(variant_params)

      respond_to do |format|
        if @variant.save
          format.html { redirect_to @variant, notice: 'Variant was successfully created.' }
          format.json { render :show, status: :created, location: @variant }
        else
          format.html { render :new }
          format.json { render json: @variant.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /variants/1
    # PATCH/PUT /variants/1.json
    def update
      respond_to do |format|
        if @variant.update(variant_params)
          format.html { redirect_to @variant, notice: 'Variant was successfully updated.' }
          format.json { render :show, status: :ok, location: @variant }
        else
          format.html { render :edit }
          format.json { render json: @variant.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /variants/1
    # DELETE /variants/1.json
    def destroy
      @variant.destroy
      respond_to do |format|
        format.html { redirect_to variants_url, notice: 'Variant was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    #GET /variants/1/check_stock.json
    def check_stock
      respond_to do |format|
        if @variant.in_stock?
          format.json { head :no_content }
        else
          format.json {
           render json: {message: 'Khong du so luong'}, status: :unprocessable_entity
          }
        end
      end

    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_variant
      @variant = Variant.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def variant_params
      params.require(:variant).permit(:product_id, :sku, :option_values, :price, :discount_amount, :sale_price)
    end
  end
end
