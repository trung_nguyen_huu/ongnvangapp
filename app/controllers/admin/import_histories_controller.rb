module Admin
  class ImportHistoriesController < BaseController
    before_action :set_import_history, only: [:show, :update, :destroy, :export_prices]

    # GET /import_histories.json
    def index
      base_scope = filter_by_ability_and_search_params
      @import_histories = base_scope.order(created_at: :desc).page(page).per(per_page).distinct
    end

    # GET /import_histories/1.json
    def show
    end

    # POST /import_histories.json
    def create
      @import_history = ImportHistory.new(import_history_params)
      authorize!(:create, @import_history)

      respond_to do |format|
        if @import_history.save
          @import_history.created_by(current_user)
          # binding.pry
          format.json { render :show, status: :created }
        else
          format.json { render json: @import_history.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /import_histories/1.json
    def update
      respond_to do |format|
        if @import_history.update(import_history_params)
          format.json { render :show, status: :ok }
        else
          format.json { render json: @import_history.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /import_histories/1.json
    def destroy
      @import_history.destroy
      respond_to do |format|
        format.json { head :no_content }
      end
    end

    #PUT /import_histories/15/export_prices.json
    def export_prices
      respond_to do |format|
        @import_history.export_price_to_variants
        format.json { render :show, status: :ok }
      end
    end

    def get_excel
      authorize!(:get_excel, ImportHistory)
      base_scope = filter_by_ability_and_search_params.order(created_at: :desc).distinct
      @import_histories = base_scope

      respond_to do |format|
        format.xlsx do 
          render xlsx: "get_excel", filename: "lich-su-nhap-kho-#{Time.zone.now.strftime("%d-%m-%Y %H:%M")}.xlsx"
        end
      end
    end

    private
    def set_import_history
      @import_history ||= ImportHistory.find(params[:id])
      authorize!(params[:action].to_sym, @import_history)
    end

    def import_history_params
      params.require(:import_history).permit(:delivery_fee_total, :title)
    end

    def filter_by_ability_and_search_params
      filter_by_ability.ransack(params[:q]).result
    end

    def filter_by_ability 
      ImportHistory.accessible_by(current_ability)
    end
  end
end
