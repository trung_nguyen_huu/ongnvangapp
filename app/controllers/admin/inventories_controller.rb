module Admin
  class InventoriesController < BaseController
    before_action :authorize_admin!

    def index
      @manager = Inventory::Manager.new(base_scope)
      @inventories = @manager.inventories.page(page).per(per_page)
    end

    def show
      @inventory = Inventory.includes(import_items_in_stock: :import_history).find(params[:id])
    end

    def get_excel
      authorize!(:get_excel, Inventory)
      @inventories = base_scope

      respond_to do |format|
        format.xlsx do 
          render xlsx: "get_excel", filename: "danh-sach-ton-kho-#{Time.zone.now.strftime("%d-%m-%Y %H:%M")}.xlsx"
        end
      end
    end

    def base_scope
      @base_scope ||= Inventory.all
                              .includes(:product)
                              .select("variants.*", "products.name")
                              .order("products.name")
                              .ransack(params[:q])
                              .result
                              .distinct
    end
  end
end
