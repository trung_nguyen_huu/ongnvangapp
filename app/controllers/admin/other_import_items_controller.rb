module Admin
  class OtherImportItemsController < BaseController
    before_action :find_import_history
    before_action :set_other_import_item, only: [:show, :update, :destroy]

    # POST /import_histories/1/other_import_items/2.json
    def create
      @other_import_item = @import_history.other_import_items.build(other_import_item_params)

      respond_to do |format|
        if @other_import_item.save
          format.json { render :show, status: :created }
        else
          format.json { render json: @other_import_item.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /import_histories/1.json
    def update
      respond_to do |format|
        if @other_import_item.update(other_import_item_params)
          format.json { render :show, status: :ok }
        else
          format.json { render json: @other_import_item.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /import_histories/1.json
    def destroy
      @other_import_item.destroy
      respond_to do |format|
        format.json { render :show, status: :ok }
      end
    end

    private
    def find_import_history
      @import_history = ImportHistory.find(params[:import_history_id])
      authorize!(params[:action].to_sym, @import_history)
    end

    def set_other_import_item
      @other_import_item = @import_history.other_import_items.find(params[:id])
    end

    def other_import_item_params
      params.require(:other_import_item).permit(:name, :cost, :note)
    end
  end
end
