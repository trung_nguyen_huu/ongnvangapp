module Admin
  class LineItemsController < BaseController
    before_action :find_order
    before_action :set_line_item, only: [:show, :update, :destroy]
    before_action :check_staff_if_they_can_do_action, only: [:create, :update, :destroy] 

    # POST /orders/1/line_items/2.json
    def create
      # binding.pry
      @line_item = @order.line_items.build(line_item_params)

      respond_to do |format|
        # binding.pry
        if @line_item.save
          format.json { render :show, status: :created }
        else
          format.json { render json: @line_item.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /orders/1/line_items/1.json
    def update
      respond_to do |format|
        if @line_item.update(line_item_params)
          format.json { render :show, status: :ok }
        else
          format.json { render json: @line_item.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /orders/1.json
    def destroy
      @line_item.destroy
      respond_to do |format|
        format.json { render :show, status: :ok }
      end
    end

    private
    def find_order
      @order = Order.find(params[:order_id])
      authorize!(params[:action].to_sym, @order)
    end

    def set_line_item
      @line_item = @order.line_items.find(params[:id])
    end

    def line_item_params
      result = params.require(:line_item).permit(:variant_id, :quantity)
      if @line_item && @line_item.persisted?
        result.delete(:variant_id)
      end

      result
    end

    def check_staff_if_they_can_do_action
      unless @order.can_edit_by?(current_user)
        raise CanCan::AccessDenied
      end
    end
  end
end
