module Admin
  class BaseController < ActionController::Base
    # Prevent CSRF attacks by raising an exception.
    # For APIs, you may want to use :null_session instead.
    protect_from_forgery with: :exception

    before_action :authenticate_user!
    before_action :authorize_admin_and_staff!
    before_action :handle_q_params, only: [:index, :get_excel]

    rescue_from ActionController::RoutingError do |exception|
      render :file => "#{Rails.root}/public/404", :layout => false, :status => :not_found
    end

    # rescue_from CanCan::AccessDenied do |exception|
    #   redirect_to not_found_page
    # end

    helper_method :page, :per_page

    def angular
      render 'layouts/admin'
    end

    protected
    def authorize_admin!
      unless current_user.admin?
        not_found()
      end
    end

    def authorize_admin_and_staff!
      if !current_user.admin? && !current_user.staff?
        not_found()
      end
    end

    def authorize_admin_and_subadmin!
      if !current_user.admin? && !current_user.subadmin?
        not_found()
      end
    end

    def authorize_admin!
      if !current_user.admin?
        not_found()
      end
    end

    def not_found
      raise ActionController::RoutingError.new('Not Found')
    end

    def page
      @page = params[:page] || 1
    end

    def per_page
      per = PageConfiguration::CONFIG[model_name.to_sym] || PageConfiguration::CONFIG[:default]
      @per_page = params[:per_page] || per
    end

    def model_name
      controller_name
    end

    def handle_q_params
      # binding.pry
      begin
        params[:q].each do |key, value|
          if key.to_s.include?("cont_any")
            params[:q][key.to_sym] = value.split(' ')
          end

          date_filter = ['created_at', 'updated_at']
          include_time_filter = date_filter.any? do |item|
            key.to_s.include?(item)
          end

          if include_time_filter && key.to_s.include?("gt")
            params[:q][key.to_sym] = value.to_datetime.beginning_of_day
          end

          if include_time_filter && key.to_s.include?("lt")
            params[:q][key.to_sym] = value.to_datetime.end_of_day
          end
        end
      rescue
      end
    end
  end
end
