module Admin
  class RefundRequestsController < BaseController
    before_action :set_supplier, only: [:show, :update, :destroy]

    # GET /suppliers.json
    def index
      authorize!(:read, :refund_requests)

      base_scope ||= NhtRequest.refund_requests.all.order(created_at: :desc)
      base_scope = base_scope.ransack(params[:q]).result
      @refund_requests = base_scope.page(page).per(per_page).distinct
    end

    def accept
      authorize!(:accept, refund_request)

      respond_to do |format|
        if refund_request.accept!
          format.json { render :show, status: :ok }
        else
          format.json { 
            render json: { errors: 'Da xet duyet hanh dong nay' }, status: :unprocessable_entity }
        end
      end
    end

    def deny
      authorize!(:deny, refund_request)

      respond_to do |format|
        if refund_request.deny!
          format.json { render :show, status: :ok }
        else
          format.json { render json: { errors: 'Da xet duyet hanh dong nay' }, status: :unprocessable_entity }
        end
      end
    end

    private
    def refund_request
      @refund_request ||= NhtRequest.refund_requests.find(params[:id])
    end
  end
end
