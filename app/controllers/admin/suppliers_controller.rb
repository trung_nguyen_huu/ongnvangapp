module Admin
  class SuppliersController < BaseController
    before_action :set_supplier, only: [:show, :update, :destroy]

    # GET /suppliers.json
    def index
      authorize!(:read, Supplier)
      base_scope ||= Supplier.all
      base_scope = base_scope.ransack(params[:q]).result
      @suppliers = base_scope.page(page).per(per_page).uniq
    end

    def all
      @suppliers = Supplier.all
    end

    # GET /suppliers/1.json
    def show
    end

    # POST /suppliers.json
    def create
      @supplier = Supplier.new(supplier_params)
      authorize!(:create, @supplier)

      respond_to do |format|
        if @supplier.save
          format.json { render :show, status: :created }
        else
          format.json { render json: @supplier.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /suppliers/1.json
    def update
      respond_to do |format|
        if @supplier.update(supplier_params)
          if !params[:supplier][:product_ids]
            @supplier.update_attributes(product_ids: [])
          end

          format.json { render :show, status: :ok }
        else
          format.json { render json: @supplier.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /suppliers/1.json
    def destroy
      @supplier.destroy
      respond_to do |format|
        format.json { head :no_content }
      end
    end

    private
    def set_supplier
      @supplier ||= Supplier.friendly.find(params[:id])
      authorize!(params[:action].to_sym, @supplier)
    end

    def supplier_params
      params.require(:supplier).permit(:name, :coso, :address, :note, phones_attributes: [:id, :number, :_destroy], product_ids: [])
    end
  end
end
