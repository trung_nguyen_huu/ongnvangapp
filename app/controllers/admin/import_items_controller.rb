module Admin
  class ImportItemsController < BaseController
    before_action :find_import_history
    before_action :set_import_item, only: [:show, :update, :destroy]

    # POST /import_histories/1/import_items/2.json
    def create
      # binding.pry
      @import_item = @import_history.import_items.build(import_item_params)

      respond_to do |format|
        if @import_item.save
          format.json { render :show, status: :created }
        else
          format.json { render json: @import_item.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /import_histories/1.json
    def update
      respond_to do |format|
        if @import_item.update(import_item_params)
          format.json { render :show, status: :ok }
        else
          format.json { render json: @import_item.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /import_histories/1.json
    def destroy
      @import_item.destroy
      respond_to do |format|
        format.json { render :show, status: :ok }
      end
    end

    private
    def find_import_history
      @import_history = ImportHistory.find(params[:import_history_id])
      authorize!(:update, @import_history)
    end

    def set_import_item
      @import_item = @import_history.import_items.find(params[:id])
    end

    def import_item_params
      result = ImportItemParams.new(params.require(:import_item), current_user).by_role
      result.delete(:variant_id) if @import_item && @import_item.persisted?
      result
    end
  end
end
