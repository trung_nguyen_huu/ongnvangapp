module Admin
  class NhtRequestsController < BaseController
    before_action :find_nht_request, only: [:accept, :deny]

    def index
      @nht_requests = StaffRequest.order(created_at: :desc)
                      .page(page)
                      .per(per_page)
    end

    def accept
      @nht_request.accept!

      respond_to do |format|
        format.json { render :show, status: :ok }
      end
    end

    def deny
      @nht_request.deny!
    end

    private
    def find_nht_request
      @nht_request = NhtRequest.find(params[:id])
    end
  end
end
