module Admin
  class CustomersController < BaseController
    before_action :set_customer, only: [:show, :update, :destroy]

    # GET /customers.json
    def index
      if get_all?
        @all = true
        @customers = filter_by_ability
      else
        base_scope = filter_by_ability_and_search_params
        @customers = base_scope.page(page).per(per_page)
      end
    end

    def get_excel
      authorize!(:get_execel, Customer)
      base_scope = filter_by_ability_and_search_params
      @customers = base_scope.reorder(name: :asc)
      respond_to do |format|
        format.xlsx do 
          render xlsx: "get_excel", filename: "danh-sach-khach-hang-#{Time.zone.now.strftime("%d-%m-%Y %H:%M")}.xlsx"
        end
      end
    end

    # POST /customers.json
    def create
      @customer = Customer.new(customer_params)
      authorize!(:create, @customer)

      respond_to do |format|
        if @customer.save
          @customer.created_by(current_user)
          format.json { render :show, status: :created }
        else
          format.json { render json: @customer.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /customers/1.json
    def update
      respond_to do |format|
        if @customer.update(customer_params)
          format.json { render :show, status: :ok }
        else
          format.json { render json: @customer.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /customers/1.json
    def destroy
      @customer.destroy
      respond_to do |format|
        format.json { head :no_content }
      end
    end

    private
    def set_customer
      @customer ||= Customer.with_deleted.friendly.find(params[:id])
      authorize!(params[:action].to_sym, @customer)
    end

    def customer_params
      params.require(:customer).permit(:name, :phone,
                                       :address, :birthday,
                                       :id_number, :email,
                                       :facebook, :job, :character)
    end

    def filter_by_ability_and_search_params
      base_scope = filter_by_ability
      if with_deleted_param?
        base_scope = base_scope.with_deleted
      end

      base_scope = base_scope.ransack(params[:q]).result.distinct
    end

    def filter_by_ability 
      Customer.accessible_by(current_ability)
    end

    def with_deleted_param?
      params[:q] && params[:q][:with_deleted] == 'true'
    end

    def get_all?
      params[:all] == 'true'
    end
  end
end
