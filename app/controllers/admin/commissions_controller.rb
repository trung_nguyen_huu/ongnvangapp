module Admin
  class CommissionsController < BaseController
    before_action :set_order
    before_action :set_commission, only: [:update, :destroy]

    # POST /commissions.json
    def create
      unless @order.seller_id
        respond_to do |format|
          format.json { render json: {error: 'bad request'}, status: :bad_request } 
          return
        end
      end

      @commission = Commission.new(commission_params) do |commission|
        commission.order = @order
      end
      authorize!(:create, @commission)

      respond_to do |format|
        if @commission.save
          format.json { render :show, status: :created }
        else
          format.json { render json: @commission.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /commissions/1.json
    def update
      authorize!(:update, @commission)

      respond_to do |format|
        if @commission.update(commission_params)
          format.json { render :show, status: :ok }
        else
          format.json { render json: @commission.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /commissions/1.json
    def destroy
      authorize!(:destroy, @commission)

      @commission.destroy
      respond_to do |format|
        format.json { head :no_content }
      end
    end

    private
    def set_order
      @order = Order.find(params[:order_id])
    end

    def set_commission
      @commission ||= @order.commission
      authorize!(params[:action].to_sym, @commission)
    end

    def commission_params
      params.require(:commission).permit(:id, :amount, :description)
    end
  end
end

