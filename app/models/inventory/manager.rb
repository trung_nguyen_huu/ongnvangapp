class Inventory::Manager
  attr_reader :inventories

  def initialize(inventories)
    @inventories = inventories
  end

  def tong_von_ton_kho
    inventories.to_a.sum(&:tong_von_ton_kho)
  end

  def tong_gia_tri_ton_kho
    inventories.to_a.sum(&:tong_gia_tri_ton_kho)
  end

  def so_luong_ton_kho
    inventories.to_a.sum(&:so_luong_ton_kho)
  end
end
