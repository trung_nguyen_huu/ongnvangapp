module MonetizeGenerator
  def monetize_attrs(*attrs)
    attrs.each do |attr|
      monetize attr.to_sym, :as => "#{attr}_money", allow_nil: true
      define_method("#{attr}_format") do
        send("#{attr}_money").send(:format)
      end
    end
  end

  def monetize_custom_attrs(*attrs)
    attrs.each do |attr|
      define_method("#{attr}_money") do
        Money.new(send(attr.to_s))
      end

      define_method("#{attr}_format") do
        send("#{attr}_money").format
      end
    end
  end
end
