class ImportUpdater
  attr_reader :import_history

  def initialize(import_history)
    @import_history = import_history
  end

  delegate :import_items, :other_import_items, :delivery_fee_total, :other_fee_total, to: :import_history

  def calculate_totals
    import_history.number_of_items = number_of_items
    import_history.cost_total = cost_total
    import_history.price_total = price_total
    import_history.discount_total = discount_total
    import_history.debit_total = debit_total
    import_history.other_fee_total = other_fee_total
  end

  def persist_data
    import_history.update_columns(
      number_of_items: import_history.number_of_items,
      cost_total: import_history.cost_total,
      price_total: import_history.price_total,
      discount_total: import_history.discount_total,
      debit_total: import_history.debit_total,
      other_fee_total: import_history.other_fee_total
    )
  end

  def number_of_items
    import_items.count
  end

  #HAM CHO CHI PHI
  def cost_total
    import_items.to_a.sum { |item| item.quantity * item.cost_per_item }
  end

  #HAM GIA BAN
  def price_total
    import_items.to_a.sum { |item| item.quantity * item.price_per_item }
  end

  def discount_total
    import_items.to_a.sum { |item| item.quantity * item.discount_per_item }
  end

  #HAM LOI NHUAN
  def debit_total
    price_total - (cost_total + delivery_fee_total + other_fee_total + discount_total) 
  end

  def other_fee_total
    other_import_items.sum(:cost)
  end
end
