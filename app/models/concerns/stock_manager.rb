class StockManager
  def initialize(line_item)
    @line_item = line_item
  end

  attr_reader :line_item
  delegate :variant, :export_items, :quantity, :old_quantity,
           :required_quantity, :import_items, to: :line_item

  def enough_stock?
    return true if required_quantity <= 0 # giam thi lun du stock

    (variant.count_in_stock - required_quantity) >= 0
  end

  def create_export_items
    #binding.pry
    raise(ActiveRecord::RecordInvalid, 'Not enough stock') if !enough_stock?

    if required_quantity == 0
      return;
    end

    if required_quantity < 0
      when_decrease_quantity()
      return;
    end

    left_quantity = 0
    if required_quantity > 0 
      left_quantity = when_increase_quantity()
    end

    if left_quantity > 0
      when_need_more_import_item(left_quantity)
    end
  end

  private
  def when_decrease_quantity
    #binding.pry
    decrease_amount = required_quantity.abs
    ex_items = export_items.order_desc_by_import_item

    ex_items.each do |ex_item|
      #binding.pry
      if decrease_amount >= ex_item.quantity
        decrease_amount = decrease_amount - ex_item.quantity
        ex_item.destroy
      else
        ex_item.quantity = ex_item.quantity - decrease_amount
        ex_item.save
        break
      end
    end
  end

  def when_increase_quantity
    #binding.pry
    ex_items = export_items.not_with_deleted_import_item.order_asc_by_import_item
    left_quantity = required_quantity
    ex_items.each do |ex_item|
      #binding.pry
      count_in_stock = ex_item.import_item.count_in_stock
      if left_quantity <= count_in_stock
        ex_item.quantity = ex_item.quantity + left_quantity
        ex_item.save
        left_quantity = 0
        break
      else
        left_quantity = left_quantity - count_in_stock
        ex_item.quantity = ex_item.quantity + count_in_stock
        ex_item.save
      end
    end

    left_quantity
  end

  def when_need_more_import_item(left_quantity)
    #binding.pry

    while left_quantity >= 1 do
      #binding.pry

      old_left_quantity = left_quantity
      import_item, left_quantity = get_import_item(left_quantity)
      if left_quantity > 0 
        line_item.export_items << ExportItem.new(import_item: import_item, quantity: import_item.count_in_stock)
      else
        line_item.export_items << ExportItem.new(import_item: import_item, quantity: old_left_quantity)
      end
    end
  end

  def get_import_item(needed_quantity)
    import_item = available_import_items.shift
    left_quantity = needed_quantity - import_item.count_in_stock

    if left_quantity <= 0
      left_quantity = 0
    end

    [import_item, left_quantity]
  end

  def available_import_items
    @available_items = variant.import_items.available.order(created_at: :asc).to_a
  end
end
