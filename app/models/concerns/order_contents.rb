class OrderContents
  def initialize(order)
    @order = order
  end

  attr_reader :order

  # def add(variant, quantity)
  # end

  def update_cart(order_params)
    #update nhung cai lien quan den order o day
  end

  def add_item(line_item_params)
    line_item = order.line_items.build(line_item_params)
    if line_item.save
      order.save
      [line_item, true]
    else
      [line_item, false]
    end
  end

  def update_item(line_item, line_item_params)
    if line_item.update_attributes(line_item_params)
      order.save
      true
    else
      false
    end
  end
end
