class Order::Statist 
  attr_reader :from_date, :to_date

  def initialize(from_date, to_date)
    @from_date = from_date.to_date rescue Time.now.beginning_of_day
    @to_date   = to_date.to_date rescue Time.now.end_of_day
  end

  def orders 
    @orders ||= Order.created_within(from_date, to_date).asc_by(:created_at)
  end

  def number_of_orders
    orders.count
  end

  def total 
    orders.sum(:total)
  end

  def debit_total 
    orders.sum(:debit_total)
  end
end
