class Order::CsvExporter
  attr_reader :order_scope, :current_user, :options

  def initialize(order_scope, current_user, options)
    @order_scope = order_scope
    @current_user = current_user
    @options = options
  end

  def to_csv()
    if current_user.admin?
      csv_for_admin()
    else
      csv_for_staff()
    end
  end

  private
  def csv_for_admin()
    csv_columns = ['MDH', 'TINH TRANG', 'SAN PHAM',
                   'SO LUONG', 'DON GIA', 'CHIET KHAU',
                   'THANH TIEN', 'GIA VON', 'PHI VAN CHUYEN', 'TONG CHIET KHAU',  'TONG GIA VON',
                   'TONG TIEN', 'LOI NHUAN', 'MAKH', 'BAN BOI',
                   'NGAY DAT HANG', 'NGAY GUI', 'NGAY NHAN']

    CSV.generate(options) do |csv|
      csv << csv_columns
      result = order_scope.includes(:customer, :seller, line_items: [variant: :product])
      result.find_each do |order|
        order.line_items.each do |li|
          csv << [order.number,
                  order.vietnamese_state,
                  li.variant.name,
                  li.quantity,
                  li.price_with_sale,
                  li.discount_amount_format,
                  li.item_total_format,
                  li.origin_price_total_format,
                  order.delivery_fee_total_format,
                  order.discount_amount_total_format,
                  order.origin_price_total_format,
                  order.total_format,
                  order.debit_total_format,
                  order.customer.number,
                  order.seller.try(:email),
                  order.created_at,
                  order.delivery_date,
                  order.receive_date
          ]
        end
      end
    end
  end

  def csv_for_staff()
    csv_columns = ['MDH', 'TINH TRANG', 'SAN PHAM',
                   'SO LUONG', 'DON GIA', 'CHIET KHAU',
                   'THANH TIEN', 'PHI VAN CHUYEN', 'TONG CHIET KHAU', 
                   'TONG TIEN', 'MAKH', 'BAN BOI',
                   'NGAY DAT HANG', 'NGAY GUI', 'NGAY NHAN']
    CSV.generate(options) do |csv|
      csv << csv_columns
      result = order_scope.includes(:customer, :seller, line_items: [variant: :product])
      result.find_each do |order|
        order.line_items.each do |li|
          csv << [order.number,
                  order.vietnamese_state,
                  li.variant.name,
                  li.quantity,
                  li.price,
                  li.discount_amount_format,
                  li.item_total_format,
                  order.delivery_fee_total_format,
                  order.discount_amount_total_format,
                  order.total_format,
                  order.customer.number,
                  order.seller.try(:email),
                  order.created_at,
                  order.delivery_date,
                  order.receive_date
          ]
        end
      end
    end
  end
end
