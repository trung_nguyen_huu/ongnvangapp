class Product::Statist
  attr_reader :from_date, :to_date

  def initialize(from_date, to_date)
    @from_date = from_date.to_date rescue Time.now.beginning_of_day
    @to_date   = to_date.to_date rescue Time.now.end_of_day
  end

  def in_stock_variants
    @in_stock_variants ||= Variant.in_stock
  end

  def cost_total
    ImportItem.for_variants(in_stock_variants.select('variants.id'))
              .sum("import_items.cost_per_item*import_items.count_in_stock")
  end

  def price_total
    in_stock_variants.sum(:price)
  end
end
