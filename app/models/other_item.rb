class OtherItem < ActiveRecord::Base
  acts_as_paranoid

  validates :name, presence: true, uniqueness: true
  has_many :other_import_items

  scope :imported_in_import_histories, ->(import_history_ids) {
    joins(other_import_items: :import_history)
    .where(import_histories: {
      id: import_history_ids
    })
    .distinct
  }
end
