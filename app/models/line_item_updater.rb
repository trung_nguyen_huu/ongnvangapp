class LineItemUpdater
  attr_reader :line_item
  delegate :export_items, to: :line_item

  def initialize(line_item)
    @line_item = line_item
  end

  def origin_price_total
    export_items.to_a.sum { |item| item.cost_per_item*item.quantity }
  end
end
