module Charts
  class Chart
    FREQUENCIES = ['month', 'week', 'day']

    attr_reader :frequency, :from_date, :to_date, :state

    def initialize(params) 
      @frequency = params[:frequency] || 'month'
      @frequency = 'month' unless FREQUENCIES.include?(params[:frequency])

      @state = params[:state] || 'completed'
      @state = 'completed' unless Order::STATES.include?(@state)

      @from_date = params[:from_date].to_date rescue Time.now.beginning_of_year
      @to_date = params[:to_date].to_date rescue Time.now.end_of_day
    end
  end
end
