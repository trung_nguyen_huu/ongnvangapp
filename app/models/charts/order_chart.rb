module Charts
  class OrderChart < Chart
    def number_of_orders_by_state
      result = Order.by_state(state).group_by_period(frequency, :created_at, range: from_date..to_date, dates: true).count
      chart_by_order(result)
    end

    #doanh thu trong thang (total cua order)
    def total_of_all_orders_by_state
      result = Order.by_state(state).group_by_period(frequency, :created_at, range: from_date..to_date, dates: true).sum(:total)
      result.each { |key, value| result[key] = value.to_f }
      chart_by_order(result)
    end

    #tong loi nhuan trong thang
    def total_of_debit_by_state
      result = Order.by_state(state).group_by_period(frequency, :created_at, range: from_date..to_date, dates: true).sum(:debit_total)
      result.each { |key, value| result[key] = value.to_f }
      chart_by_order(result)
    end

    #chi phi trong thang (gia goc bo ra)
    def total_of_origin_price_by_state
      result = Order.by_state(state).group_by_period(frequency, :created_at, range: from_date..to_date, dates: true).sum(:origin_price_total)
      result.each { |key, value| result[key] = value.to_f }
      chart_by_order(result)
    end
    
    private
    def chart_by_order(result)
      {
        x_axis: result.keys,
        data: result.values
      }
    end
  end
end
