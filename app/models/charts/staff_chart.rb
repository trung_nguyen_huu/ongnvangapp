module Charts
  class StaffChart < Chart
    def number_of_orders_by_staff
      chart_by_staff do |staff|
        staff.orders.by_state(state).group_by_period(frequency, :created_at, range: from_date..to_date, dates: true).count
      end
    end

    ##Chiet khau nhan vien theo ngay, tuan thang
    def discount_amounts_by_staff
      chart_by_staff do |staff|
        result = staff.orders.by_state(state).group_by_period(frequency, :created_at, range: from_date..to_date, dates: true)
                    .sum(:discount_amount_total)
        result.each { |key, value| result[key] = value.to_f }
      end
    end

    ##Loi nhuan ma nhan vien do dem lai theo ngay tuan thang
    def debit_total_by_staff
      chart_by_staff do |staff|
        result = staff.orders.by_state(state).group_by_period(frequency, :created_at, range: from_date..to_date, dates: true)
                    .sum(:debit_total)
        result.each { |key, value| result[key] = value.to_f }
      end
    end

    ##so luong mon hang nhan vien ban dc theo ngay tuan thang 
    def number_of_selled_products_by_staff
      chart_by_staff do |staff|
        staff.line_items.group_by_period(frequency, 'orders.created_at', range: from_date..to_date, dates: true)
                        .sum(:quantity)
      end
    end

    private
    def chart_by_staff(&algorithm)
      @result = User.staff.map do |staff|
        {
          name: staff.name,
          data: algorithm.call(staff)
        }
      end

      @result = {
        x_axis: @result[0][:data].keys,
        series: @result.each { |staff| staff[:data] = staff[:data].values }
      }
    end
  end
end
