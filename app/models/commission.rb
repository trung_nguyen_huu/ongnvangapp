class Commission < ActiveRecord::Base
  # acts_as_paranoid
  validates :amount, presence: true, numericality: { greater_than_or_equal_to: 0 }

  belongs_to :order
  belongs_to :commission_in_month

  after_create :check_create_and_assign_commission_in_month

  private
  def check_create_and_assign_commission_in_month
    commission_in_month = CommissionInMonth.in_current_month.of_seller(order.seller_id).first
    if commission_in_month.nil?
      commission_in_month = CommissionInMonth.create(seller_id: order.seller_id)
    end

    update_columns(commission_in_month_id: commission_in_month.id)
  end
end
