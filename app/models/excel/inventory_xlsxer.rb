module Excel
  class InventoryXlsxer < Exporter
    attr_reader :inventories_scope

    def initialize(inventories_scope, xlsx_package, current_user)
      @inventories_scope = inventories_scope
      super(xlsx_package, current_user)
    end

    def sheet_name
      'ton-kho'
    end

    def title
      'TỒN KHO'
    end

    def headers
      ['Stt', #0
       'San pham',
       'SL Ton', #2
       'Von ton kho',
       'Gia tri ton',
       'Lan nhap', #5
       'Ngay',
       'SL nhap',
       'SL ban', #8
       'SL ton',
       'Gia nhap',
       'Tong gia nhap ton']
    end

    def currency_cols
      [3, 4, 10, 11]
    end

    def datetime_cols
      [6]
    end

    def row_count
      count = 0
      inventories_scope.includes(:import_items_in_stock).each do |inventory|
        count += inventory.import_items_in_stock.to_a.count
      end

      count
    end

    def main_content_row_offset
      8
    end

    private
    def add_summary_info(sheet)
      sheet.add_row([nil, 'SL tồn kho:',
                     "=SUM(C#{main_content_row_offset + 1}:C#{main_content_row_offset + row_count})"],
      style: [nil, bold_text_style, bold_text_style])

      sheet.add_row([nil, 'Tổng vốn tồn kho:',
                     "=SUM(D#{main_content_row_offset + 1}:D#{main_content_row_offset + row_count})"],
      style: [nil, bold_text_style, currency_format_style])

      sheet.add_row([nil, 'Tổng giá trị tồn kho:',
                     "=SUM(E#{main_content_row_offset + 1}:E#{main_content_row_offset + row_count})"],
      style: [nil, bold_text_style, currency_format_style])
    end

    def add_main_content(sheet)
      inventories_per_page = 1000
      stt = 1
      total_pages = inventories_scope.page(1).per(inventories_per_page).total_pages

      (1..total_pages).each do |page_index|
        inventories_scope.includes(:product,
                                   import_items_in_stock: :import_history)
        .page(page_index)
        .per(inventories_per_page)
        .each do |inventory|
          is_first_item_in_inventory = true
          inventory.import_items_in_stock.each do |import_item|
            if is_first_item_in_inventory
              sheet.add_row([stt,
                             inventory.name,
                             inventory.so_luong_ton_kho,
                             inventory.tong_von_ton_kho,
                             inventory.tong_gia_tri_ton_kho,
                             import_item.import_history.title,
                             import_item.created_at,
                             import_item.quantity,
                             import_item.sold_quantity,
                             import_item.count_in_stock,
                             import_item.cost_per_item,
                             import_item.count_in_stock*import_item.cost_per_item],
                             style: border_cell_style)
            else
              sheet.add_row([nil,
                             nil,
                             nil,
                             nil,
                             nil,
                             import_item.import_history.title,
                             import_item.created_at,
                             import_item.quantity,
                             import_item.sold_quantity,
                             import_item.count_in_stock,
                             import_item.cost_per_item,
                             import_item.count_in_stock*import_item.cost_per_item],
                             style: border_cell_style)
            end
            is_first_item_in_inventory = false
          end
          stt += 1
        end
      end
    end

    def add_more_format(sheet)
      set_cols_width(sheet)
      format_stt_col(sheet)
    end

    def set_cols_width(sheet)
      sheet.column_widths(5, 40, 10, 15, 15, 20, 15, 15, 15, 15, 15, 15)
    end

    def format_stt_col(sheet)
      sheet.col_style(0, stt_style, row_offset: main_content_row_offset)
    end
  end
end
