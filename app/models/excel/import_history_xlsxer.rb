module Excel
  class ImportHistoryXlsxer < Exporter
    attr_reader :import_histories_scope

    def initialize(import_histories_scope, xlsx_package, current_user)
      @import_histories_scope = import_histories_scope
      super(xlsx_package, current_user)
    end

    def sheet_name
      'lich-su-nhap-kho'
    end

    def title
      'LICH SU NHAP KHO'
    end

    def headers
      ['Stt', #0
       'Tieu de',
       'Ngay nhap', #2
       'Tong so muc',
       'Tong gia nhap',
       'Phi v/c', #5
       'Tong chi phi khac',
       'Tong chi phi',
       'San pham', #8
       'SL',
       'Gia nhap',
       'Tong gia nhap', #11
       'Gia ban (du kien)',
       'Ngay tao',
       'Ngay chinh sua'] #14
    end

    def currency_cols
      [4, 5, 6, 7, 10, 11, 12]
    end

    def datetime_cols
      [2, 13, 14]
    end

    def row_count
      ImportItem.where(import_history_id: import_histories_scope.unscope(:order).select(:id).distinct).count
    end

    def main_content_row_offset
      6
    end

    private
    def add_summary_info(sheet)
      sheet.add_row([nil, 'Tong chi phi:', "=SUM(H#{main_content_row_offset + 1}:H#{main_content_row_offset + row_count})"],
                    style: [nil, bold_text_style, currency_format_style])
    end

    def add_main_content(sheet)
      import_histories_per_page = 2
      stt = 1
      total_pages = import_histories_scope.page(1).per(import_histories_per_page).total_pages

      (1..total_pages).each do |page_index|
        import_histories_scope.includes(import_items: [variant: :product])
        .page(page_index)
        .per(import_histories_per_page)
        .each do |import_history|
          is_first_item_in_import_history = true
          import_history.import_items.each do |import_item|
            if is_first_item_in_import_history
              sheet.add_row([stt,
                             import_history.title,
                             import_history.created_at,
                             import_history.number_of_items,
                             import_history.cost_total,
                             import_history.delivery_fee_total,
                             import_history.other_fee_total,
                             import_history.total_of_all_cost,
                             import_item.variant.name,
                             import_item.quantity,
                             import_item.cost_per_item,
                             import_item.cost,
                             import_item.price_per_item,
                             import_history.created_at,
                             import_history.updated_at],
                             style: border_cell_style)
            else
              begin
              sheet.add_row([nil,
                             nil,
                             nil,
                             nil,
                             nil,
                             nil,
                             nil,
                             nil,
                             import_item.variant.name,
                             import_item.quantity,
                             import_item.cost_per_item,
                             import_item.cost,
                             import_item.price_per_item,
                             import_history.created_at,
                             import_history.updated_at],
                             style: border_cell_style)
              rescue 
                # binding.pry
                i = 10
              end
            end
            is_first_item_in_import_history = false
          end
          stt += 1
        end
      end
    end

    def add_more_format(sheet)
      set_cols_width(sheet)
      format_stt_col(sheet)
    end

    def set_cols_width(sheet)
      sheet.column_widths(5, 20, 15, 15, 15, 15, 15, 15, 35, 5, 15, 15, 15, 15, 20)
    end

    def format_stt_col(sheet)
      sheet.col_style(0, stt_style, row_offset: main_content_row_offset)
    end
  end
end
