module Excel
  class OrderXlsxer < Exporter
    attr_reader :orders_scope

    def initialize(orders_scope, xlsx_package, current_user)
      @orders_scope = orders_scope
      super(xlsx_package, current_user)
    end

    def sheet_name
      'danh-sach-don-hang'
    end

    def title
      'DANH SACH ĐƠN HÀNG'
    end

    def headers
      ['Stt', #0
       'MDH',
       'MKH', #2
       'KH',
       'SDT',
       'Ban boi',
       'TT',
       'Phi v/c', #7
       'Tong tien cua cac item',
       'Tong gia tri don hang',
       'Tong gia von', #10
       'Loi nhuan',
       'Chiết khấu',
       'Diễn giải',
       'San pham',
       'SL', #15
       'Gia ban',
       'Thanh tien',
       'Gia von', #18
       'Ngay dat hang',
       'Ngay gui',
       'Ngay nhan']
    end

    def currency_cols
      [7, 8, 9, 10, 11, 12, 16, 17, 18]
    end

    def datetime_cols
      [19, 20, 21]
    end

    def row_count
      LineItem.where(order_id: orders_scope.select(:id).unscope(:order)).distinct.count
    end

    def main_content_row_offset
      9
    end

    private
    def add_summary_info(sheet)
      sheet.add_row([nil, 'So luong don hang: ', orders_scope.count],
                    style: [nil, bold_text_style, border_cell_style])

      sheet.add_row([nil, 'Tong tien ban duoc:', "=SUM(J#{main_content_row_offset + 1}:J#{main_content_row_offset + row_count})"],
                    style: [nil, bold_text_style, currency_format_style])

      sheet.add_row([nil, 'Tong gia von', "=SUM(K#{main_content_row_offset + 1}:K#{main_content_row_offset + row_count})"],
                    style: [nil, bold_text_style, currency_format_style])

      sheet.add_row([nil, 'Tong loi nhuan', "=SUM(L#{main_content_row_offset + 1}:L#{main_content_row_offset + row_count})"],
                    style: [nil, bold_text_style, currency_format_style])
    end

    def add_main_content(sheet)
      orders_per_page = 1000
      stt = 1
      total_pages = orders_scope.page(1).per(orders_per_page).total_pages

      (1..total_pages).each do |page_index|
        orders_scope.includes(:customer, :seller, line_items: [variant: :product])
                    .page(page_index)
                    .per(orders_per_page)
                    .each do |order|
                      is_first_item_in_order = true
                      order.line_items.each do |line_item|
                        if is_first_item_in_order
                          commission = order.commission.amount rescue 0
                          commission_note = order.commission.note rescue ''
                          sheet.add_row([stt,
                                         order.number,
                                         order.customer.try(:number),
                                         order.customer.try(:name),
                                         order.customer.try(:phone),
                                         order.seller.try(:name),
                                         order.vietnamese_state,
                                         order.delivery_fee_total,
                                         order.item_total,
                                         order.total,
                                         order.origin_price_total,
                                         order.debit_total,
                                         commission,
                                         commission_note,
                                         line_item.variant.name,
                                         line_item.quantity,
                                         line_item.price_with_sale,
                                         line_item.item_total,
                                         line_item.origin_price_total,
                                         order.created_at,
                                         order.delivery_date,
                                         order.receive_date], style: border_cell_style)
                        else
                          sheet.add_row([nil,
                                         nil,
                                         nil,
                                         nil,
                                         nil,
                                         nil,
                                         nil,
                                         nil,
                                         nil,
                                         nil,
                                         nil,
                                         nil,
                                         nil,
                                         nil,
                                         line_item.variant.name,
                                         line_item.quantity,
                                         line_item.price_with_sale,
                                         line_item.item_total,
                                         line_item.origin_price_total,
                                         nil,
                                         nil,
                                         nil], style: border_cell_style)
                        end
                        is_first_item_in_order = false
                      end
                      stt += 1
                    end
      end
    end

    def add_more_format(sheet)
      set_cols_width(sheet)
      format_stt_col(sheet)
    end

    def set_cols_width(sheet)
      sheet.column_widths(5, 10, 10, 20, 20, 20, 10, 15, 15, 15, 15, 15, 15, 15, 30, 5, 15, 15, nil, nil, nil)
    end

    def format_stt_col(sheet)
      sheet.col_style(0, stt_style, row_offset: main_content_row_offset)
    end
  end
end
