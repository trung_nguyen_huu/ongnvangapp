module Excel
  class WbAdapter
    attr_reader :wb

    def initialize(wb)
      @wb = wb
    end

    def add_style(styles)
      wb_styles.add_style(styles.merge(general_style))
    end

    def add_style_without_general_style(styles)
      wb_styles.add_style(styles)
    end

    private
    def general_style
      @general_style = {border: { style: :thin,
                                  :color =>"000000"}
      }
    end

    def wb_styles
      wb.styles
    end
  end
end
