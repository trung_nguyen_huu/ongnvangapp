module Excel
  class CustomerXlsxer < Exporter
    attr_reader :customers_scope

    def initialize(customers_scope, xlsx_package, current_user)
      @customers_scope = customers_scope
      super(xlsx_package, current_user)
    end

    def sheet_name
      'danh-sach-khach-hang'
    end

    def title
      'DANH SACH KHÁCH HÀNG'
    end

    def headers
      ['Stt',
       'MKH',
       'Khách hàng',
       'Phone',
       'Địa chỉ',
       'Ngày sinh',
       'CMND',
       'Email',
       'Facebook',
       'Nghề nghiệp',
       'Tính cách',
       'Tao vao',
       'Sua vao']
    end

    def currency_cols
      []
    end

    def record_count
      customers_scope.count
    end

    def main_content_row_offset
      6
    end

    private
    def add_summary_info(sheet)
      sheet.add_row([nil, 'So luong khach hang: ', "=ROWS(D#{main_content_row_offset}:D#{record_count})"],
                    style: [nil, bold_text_style, border_cell_style])

    end

    def add_main_content(sheet)
      customers_per_page = 2
      stt = 1
      total_pages = customers_scope.page(1).per(customers_per_page).total_pages

      (1..total_pages).each do |page_index|
        customers_scope.page(page_index).per(customers_per_page).each do |customer|
          customer_phone = "'" + customer.phone rescue ''
          sheet.add_row([stt,
                         customer.number,
                         customer.name,
                         customer_phone,
                         customer.address,
                         customer.birthday,
                         customer.id_number,
                         customer.email,
                         customer.facebook,
                         customer.job,
                         customer.character,
                         customer.created_at,
                         customer.updated_at], style: border_cell_style)
          stt += 1
        end
      end
    end

    def add_more_format(sheet)
      set_cols_width(sheet)
      format_stt_col(sheet)
      format_birthday_col(sheet)
      format_address_col(sheet)
      format_created_at_and_updated_at_col(sheet)
    end

    def set_cols_width(sheet)
      sheet.column_widths(5, 30, nil, nil, 50, nil, nil, nil, nil, nil, nil, nil, nil)
    end

    def format_stt_col(sheet)
      sheet.col_style(0, stt_style, row_offset: main_content_row_offset)
    end

    def format_birthday_col(sheet)
      sheet.col_style(5, date_format_style, row_offset: main_content_row_offset)
    end

    def format_address_col(sheet)
      sheet.col_style(4, wrap_text_style, row_offset: main_content_row_offset)
    end

    def format_created_at_and_updated_at_col(sheet)
      sheet.col_style(11, datetime_format_style, row_offset: main_content_row_offset)
      sheet.col_style(12, datetime_format_style, row_offset: main_content_row_offset)
    end
  end
end
