module Excel
  class Exporter
    attr_reader :xlsx_package, :current_user, :main_content_row_offset

    def initialize(xlsx_package, current_user)
      @xlsx_package = xlsx_package
      @current_user = current_user
      @main_content_row_offset = 0
    end

    def to_xlsx
      wb.add_worksheet(name: sheet_name) do |sheet|
        add_logo(sheet)
        add_title_row(sheet)
        add_datetime_row(sheet)
        add_summary_info(sheet)
        add_empty_row(sheet)
        add_header_row(sheet)
        add_main_content(sheet)
        format_currency_cols(sheet)
        format_date_cols(sheet)
        format_datetime_cols(sheet)
        add_more_format(sheet)
      end
    end

    def sheet_name
      raise Exception, 'Must define method when generating xlsx'
    end

    def title
      raise Exception, 'Must define method when generating xlsx'
    end

    def headers
      raise Exception, 'Must define method when generating xlsx'
    end

    def currency_cols
      raise Exception, 'Must define method when generating xlsx'
    end

    def date_cols
      []
    end

    def datetime_cols
      []
    end

    private

    def add_logo(sheet)
      img = File.expand_path('../../../assets/images/excel_logo.png', __FILE__)
      sheet.add_image(:image_src => img, :noSelect => true,
                      :noMove => true, :hyperlink=>"ongvangnht.herokuapp.com") do |image|
        image.width=300
        image.height=72
        image.hyperlink.tooltip = "ONG VANG NHT WEBSITE"
        image.start_at(0, 0)
      end

      sheet.add_row([], height: 50)
      sheet.merge_cells("A1:I1")
    end

    def add_summary_info(sheet)
    end

    def add_title_row(sheet)
      sheet.add_row([title], style: title_style)
      sheet.merge_cells("A2:I2")

      increment_main_content_row_offset()
    end

    def add_datetime_row(sheet)
      sheet.add_row([nil, 'Xuất vào:', Time.now], style: [nil, bold_text_style, datetime_format_style])

      increment_main_content_row_offset()
    end

    def add_empty_row(sheet)
      sheet.add_row([])
      increment_main_content_row_offset()
    end

    def add_header_row(sheet)
      sheet.add_row(headers, style: header_style)

      increment_main_content_row_offset()
    end

    def add_main_content(sheet)
    end

    def format_currency_cols(sheet)
      currency_cols.each do |col_number|
        sheet.col_style(col_number, currency_format_style, row_offset: main_content_row_offset)
      end
    end

    def format_date_cols(sheet)
      date_cols.each do |col_number|
        sheet.col_style(col_number, date_format_style, row_offset: main_content_row_offset)
      end
    end

    def format_datetime_cols(sheet)
      datetime_cols.each do |col_number|
        sheet.col_style(col_number, datetime_format_style, row_offset: main_content_row_offset)
      end
    end

    def add_more_format(sheet)
    end

    def increment_main_content_row_offset
      @main_content_row_offset += 1
    end

    def currency_format_style
      @currency_format_style ||= wb_adapter.add_style(format_code: '#,##0.00 [$₫-42A];[RED]-#,##0.00 [$₫-42A]')
    end

    def border_cell_style
      @border_cell_style ||= wb_adapter.add_style({})
    end

    def bold_text_style
      @bold_text_style ||= wb_adapter.add_style(sz: 12,
                                                b: true)
    end

    def date_format_style
      @date_format_style ||= wb_adapter.add_style(format_code: 'dd-mm-yyyy')
    end

    def datetime_format_style
      @datetime_format_style ||= wb_adapter.add_style(format_code: 'dd-mm-yyyy H:M')
    end

    def header_style
      @header_style ||= wb_adapter.add_style(bg_color: 'FBD60B',
                                             fg_color: '000000',
                                             sz: 12,
                                             b: true,
                                             alignment: { :horizontal=> :center })
    end

    def title_style
      @title_style ||= wb_adapter.add_style(sz: 14,
                                            b: true,
                                            alignment: { :horizontal=> :center })

    end

    def stt_style
      @stt_style ||= wb_adapter.add_style(bg_color: 'FBD60B',
                                          fg_color: '000000',
                                          alignment: { :horizontal=> :center, vertical: :center  })
    end

    def wrap_text_style
      @wrap_text_style ||= wb_adapter.add_style(alignment: { wrap_text: true })
    end

    def wb_adapter
      @wb_adapter ||= WbAdapter.new(wb)
    end

    def wb
      xlsx_package.workbook
    end
  end
end

