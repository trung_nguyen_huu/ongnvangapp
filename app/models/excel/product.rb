module Excel
  class Product < Exporter
    attr_reader :product_scope

    def initialize(product_scope, xlsx_package, current_user)
      @product_scope = product_scope
      super(xlsx_package, current_user)
    end

    def sheet_name
      'san-pham'
    end

    def title
      'DANH MUC SAN PHAM'
    end

    def headers
      ['STT',
       'SAN PHAM',
       'LOAI',
       'GIA BAN',
       'GIA SALE',
       'GIA NHAP',
       'GIA NHAP GAN NHAT',
       'CHIET KHAU',
       'TON KHO',
       'MO TA']
    end

    def currency_cols
      [2, 3, 4, 5, 6, 7]
    end

    def record_count
      count = 0
      product_scope.includes(:variants).find_each do |product|
        count += product.variants.count
      end

      count
    end

    def main_content_row_offset
      8
    end

    private
    def add_summary_info(sheet)
      sheet.add_row([nil, 'Tong so luong san pham', "=ROWS(D#{main_content_row_offset}:D#{record_count})"],
                    style: [nil, bold_text_style, border_cell_style])

      sheet.add_row([nil, 'Tong gia ban', "=SUM(D#{main_content_row_offset}:D#{record_count})"],
                    style: [nil, bold_text_style, currency_format_style])

      sheet.add_row([nil, 'Tong so luong ton kho', "=SUM(I#{main_content_row_offset}:I#{record_count})"],
                    style: [nil, bold_text_style, border_cell_style])
    end

    def add_main_content(sheet)
      stt = 1
      product_scope.all.includes(variants: :import_items).reorder(name: :asc).each do |product|
        product.variants.each do |variant|
          sheet.add_row([stt,
                         product.name,
                         variant.option_values,
                         variant.price_money,
                         variant.sale_price_considering_sale_money,
                         variant.cost_money,
                         variant.newest_cost_price_money,
                         variant.discount_amount_money,
                         variant.count_in_stock,
                         product.description], style: border_cell_style)
          stt += 1
        end
      end
    end

    def add_more_format(sheet)
      format_description_col(sheet)
      format_stt_col(sheet)
    end

    def format_description_col(sheet)
      description_style = wb_adapter.add_style(alignment: { wrap_text: true })
      sheet.col_style(9, description_style, row_offset: main_content_row_offset)
      sheet.column_widths(5, nil, nil, nil,nil, nil, nil, nil, nil, 50)
    end

    def format_stt_col(sheet)
      sheet.col_style(0, stt_style, row_offset: main_content_row_offset)
    end
  end
end
