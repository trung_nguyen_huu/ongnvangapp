class ImportHistory < ActiveRecord::Base
  acts_as_paranoid

  has_many :import_items, class_name: 'ImportItem',
                          inverse_of: :import_history, dependent: :destroy

  has_many :other_import_items, class_name: 'OtherImportItem',
                          inverse_of: :import_history, dependent: :destroy
  belongs_to :creator, class_name: 'User'

  accepts_nested_attributes_for :import_items, allow_destroy: true

  def updater
    @updater ||= ImportUpdater.new(self)
  end

  validates :other_fee_total, numericality: { greater_than_or_equal_to: 0 }
  validates :delivery_fee_total, numericality: { greater_than_or_equal_to: 0 }

  after_save :recalculate_and_persist_data

  scope :within, ->(from_date, to_date) { where(created_at: from_date..to_date) }

  def total_of_all_cost
    cost_total + other_fee_total + delivery_fee_total;
  end

  def recalculate_and_persist_data
    updater.calculate_totals
    updater.persist_data
  end

  def export_price_to_variants
    import_items.each do |item|
      item.export_price_to_variant
    end
  end

  def created_by(user)
    update_columns(creator_id: user.id)
  end
end
