class ExportItem < ActiveRecord::Base
  SOLD = 'sold'
  REFUNDED = 'refunded'

  belongs_to :import_item, -> { with_deleted }
  belongs_to :line_item

  #Validates
  validates :state, inclusion: { in: [SOLD, REFUNDED] }

  #scope
  scope :order_asc_by_import_item, -> { joins(:import_item).order("import_items.created_at asc") }
  scope :order_desc_by_import_item, -> { joins(:import_item).order("import_items.created_at desc") }
  scope :can_add_more_quantity, -> { joins(:import_item).where("import_items.count_in_stock > ?", 0) }
  scope :not_with_deleted_import_item, -> { joins(:import_item).where("import_items.deleted_at IS NULL") }
  scope :for_sell, -> { where(state: SOLD) }
  scope :for_refund, -> { where(state: REFUNDED) }

  #gia von
  delegate :cost_per_item, to: :import_item
  after_save :persist_count_in_stock_on_import_item_and_variant
  after_destroy :persist_count_in_stock_on_import_item_and_variant


  def refund!
    self.update_attributes(state: REFUNDED)
  end

  private
  def persist_count_in_stock_on_import_item_and_variant
    if !import_item.deleted?
      import_item.recalculate_and_persist_count_in_stock
    end
  end
end
