class Product < ActiveRecord::Base
  extend FriendlyId
  friendly_id :slug_candidates, use: :slugged

  acts_as_paranoid

  has_many :variants, class_name: 'Variant', dependent: :destroy
  has_and_belongs_to_many :suppliers, dependent: :destroy

  validates :name, presence: true
  validates :slug, presence: true, uniqueness: true
  validate :must_have_at_least_one_variants
  
  default_scope { order(name: :asc) }

  accepts_nested_attributes_for :variants, allow_destroy: true

  def list_option_values
    variants.pluck(:option_values).join(',')
  end

  def variant_by_option_values(option_values)
    variants.find_by(option_values: option_values)
  end

  private
  def slug_candidates
    [
      :name,
      [:name, :description]
    ]
  end

  def must_have_at_least_one_variants
    errors.add(:base, "Vui lòng tạo ra ít nhất 1 biến thể cho sản phẩm") if variants.empty?
  end

  def self.csv_column_names
    ['SAN PHAM', 'LOAI', 'GIA', 'CHIET KHAU', 'TRONG KHO']
  end
end
