class Variant < ActiveRecord::Base
  acts_as_paranoid

  belongs_to :product, -> { with_deleted }
  has_many :import_items

  validates :option_values, presence: true
  validates :price, presence: true
  validates :cost, numericality: { greater_than_or_equal_to: 0 }
  validates :sale_price, numericality: { greater_than_or_equal_to: 0 }

  before_validation :check_price_and_discount_amount

  scope :imported_within, ->(from_date, to_date) {
    joins(import_items: :import_history)
    .where(import_histories: { created_at: from_date..to_date }).distinct
  }

  scope :in_stock, -> { where("count_in_stock > ?", 0) }

  extend MonetizeGenerator
  monetize_attrs :price,
    :discount_amount,
    :price_with_sale,
    :sale_price_considering_sale,
    :cost,
    :newest_cost_price

  def in_stock?
    count_in_stock >= 1
  end

  def name
    product.name + " (" + option_values + ")"
  end

  def newest_import_history
    import_items.order(created_at: :desc).first.try(:import_history)
  end

  def newest_cost_price
    import_items.order(created_at: :desc).first.cost_per_item rescue 0
  end

  #METHOD FOR PERSISTENT DATA
  def persist_count_in_stock
    count_in_stock = import_items.sum(:count_in_stock)
    self.update_columns(count_in_stock: count_in_stock)
  end

  def on_sale?
    return false unless sale_price
    return false if sale_from_date.nil? || sale_to_date.nil?

    sale_from_date <= Time.now.to_date && sale_to_date >= Time.now.to_date
  end

  def price_with_sale
    return sale_price if on_sale?
    price
  end

  def sale_price_considering_sale
    on_sale? ? sale_price : 0
  end

  private
  def check_price_and_discount_amount
    self.price = 0 if price && price < 0
    self.discount_amount = 0 if discount_amount && discount_amount < 0
  end

end
