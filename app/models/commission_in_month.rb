class CommissionInMonth < ActiveRecord::Base
  self.table_name = 'commission_in_months'

  validates :seller_id, presence: true
  has_many :pays, class_name: 'PayForCommission'
  has_many :commissions
  has_many :orders, through: :commissions
  belongs_to :seller, class_name: 'User'

  scope :in_current_month, -> {
    where(created_at: Time.now.beginning_of_month..Time.now.end_of_month)
  }

  scope :of_seller, ->(id){
    where(seller_id: id)
  }

  def number_of_orders
    orders.count
  end

  def order_total
    orders.sum(:total)
  end

  def commission_total
    commissions.sum(:amount)
  end

  def number_of_pays
    pays.count
  end

  def pay_total
    pays.sum(:amount)
  end

  def ton_cuoi_ky
    commission_total - pay_total
  end
end
