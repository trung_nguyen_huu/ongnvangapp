class LineItem < ActiveRecord::Base
  belongs_to :order
  belongs_to :variant
  has_many :export_items, dependent: :destroy
  #CAn check moi quan he nay dung ko
  has_many :import_items, through: :export_items

  #Validations
  validates :quantity, numericality: {only_integer: true, greater_than_or_equal_to: 0}
  validates :sale_price, numericality: {greater_than_or_equal_to: 0}
  validate :must_enough_count_in_stock

  #Callbacks
  before_create :set_prices_from_variant
  after_save :create_export_items, unless: Proc.new { |li| li.order.refunded? }
  after_save :recalculate_origin_price_from_export_items
  after_save :persist_data_on_order
  after_destroy :persist_data_on_order
  before_validation :set_old_quantity
  before_create :set_sale_price, if: Proc.new { |line_item| line_item.variant.on_sale? }


  extend MonetizeGenerator
  monetize_attrs :price, :discount_amount, :origin_price_total, :price_with_sale
  monetize_custom_attrs :item_total

  def updater
    @line_item_updater = LineItemUpdater.new(self)
  end

  def stock_manager
    @stock_manager = StockManager.new(self)
  end

  attr_reader :old_quantity

  def set_old_quantity
    return @old_quantity = 0 if self.new_record?
    @old_quantity = self.quantity_was
  end

  #<=0 giam, >=0 can them
  def required_quantity
    @required_quantity = quantity - old_quantity
    @required_quantity
  end

  def recalculate_origin_price_from_export_items
    self.update_columns(origin_price_total: updater.origin_price_total)
  end

  def persist_data_on_order
    order.reload
    order.calculate_and_persist_totals
  end

  def item_total
    price*quantity
  end

  def price_with_sale
    return sale_price if on_sale?
    price
  end

  def on_sale?
    sale_price && sale_price != 0
  end

  private
  def must_enough_count_in_stock
    if !stock_manager.enough_stock?
      errors.add(:base, "Not enough quantity")
    end
  end

  def set_prices_from_variant
    self.price = variant.price
    self.discount_amount = 0
  end

  def create_export_items
    stock_manager.create_export_items
    self.reload
  end

  def set_sale_price
    self.sale_price = variant.sale_price if variant.on_sale?
  end
end

