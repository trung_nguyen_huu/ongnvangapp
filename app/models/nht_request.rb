class NhtRequest < ActiveRecord::Base
  WAITING = 'waiting'
  ACCEPTED = 'accepted'
  DENIED = 'denied'

  store :parameters

  belongs_to :object, polymorphic: true
  belongs_to :requester, -> { with_deleted }, class_name: 'User'

  validates :state, inclusion: { in: [WAITING, ACCEPTED, DENIED] }

  scope :refund_requests, -> { where(action: 'refund!') }
  scope :not_examined, -> { where(state: WAITING) }


  def accept!
    return false if examined?

    if refund_request?
      object.refund!(parameters)
    end
    self.update_attributes(state: ACCEPTED)
  end

  def deny!
    return false if examined?
    self.update_attributes(state: DENIED)
  end

  def waitting?
    state == WAITING
  end

  def accepted?
    state == ACCEPTED
  end

  def denied?
    state == DENIED
  end

  def examined?
    state == ACCEPTED || state == DENIED
  end

  def refund_request?
    object.class == Order && action == 'refund!'
  end
end
