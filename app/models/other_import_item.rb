class OtherImportItem < ActiveRecord::Base
  attr_accessor :name

  acts_as_paranoid
  belongs_to :import_history, -> { with_deleted }, class_name: 'ImportHistory',
                              inverse_of: :other_import_items, touch: true,
                              foreign_key: 'import_history_id'
  belongs_to :other_item, -> { with_deleted }, class_name: 'OtherItem',
                          inverse_of: :other_import_items,
                          foreign_key: 'other_item_id'

  validates :name, presence: true
  validates :cost, presence: true, numericality: { greater_than_or_equal_to: 0 }

  before_save :fetch_other_item_by_name
  after_save :persist_data_on_import_history
  after_destroy :persist_data_on_import_history

  private
  def persist_data_on_import_history
    import_history.recalculate_and_persist_data
  end

  def fetch_other_item_by_name
    self.other_item_id = other_item_by_name.id
  end

  def other_item_by_name
    OtherItem.where('name ILIKE ?', name.squish!).first || OtherItem.create(name: name)
  end
end
