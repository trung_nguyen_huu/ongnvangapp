class Order < ActiveRecord::Base
  include NumberGenerator
  def generate_number(options = {})
    options[:prefix] ||= 'OV'
    options[:length] ||= 3
    super(options)
  end

  extend FriendlyId
  friendly_id :number, slug_column: :number, use: :slugged

  STATES = ['waiting', 'delivered', 'completed', 'refunded']

  has_many :line_items, dependent: :destroy
  has_many :export_items, through: :line_items
  has_many :staff_requests, class_name: 'StaffRequest', as: :object
  has_many :refund_requests, -> { refund_requests },
    class_name: 'NhtRequest',
    as: :object
  has_one :commission

  belongs_to :customer, -> { with_deleted }, class_name: 'Customer', foreign_key: 'customer_id'
  belongs_to :seller, -> { with_deleted } , class_name: 'User', foreign_key: 'seller_id'

  scope :created_within, ->(from_date, to_date){
    where(created_at: from_date..to_date)
  }

  scope :asc_by, ->(attr) { order("#{attr} ASC")}
  scope :by_state, ->(state) { where(state: state) }

  extend MonetizeGenerator
  monetize_attrs :item_total, :origin_price_total, :delivery_fee_total, :discount_amount_total, :total, :debit_total

  def self.to_csv(current_user, options = {})
    CsvExporter.new(self.all, current_user, options).to_csv()
  end

  def updater
    @order_updater ||= OrderUpdater.new(self)
  end

  delegate :calculate_and_persist_totals, to: :updater
  after_save :calculate_and_persist_totals

  def ship!(delivery_date)
    delivery_date ||= Time.now
    self.update_attributes(state: 'delivered', delivery_date: delivery_date)
  end

  def received!(receive_date)
    receive_date ||= Time.now
    self.update_attributes(state: 'completed', receive_date: receive_date)
  end

  def refund!(params)
    return if self.refunded?

    self.update_attributes(state: 'refunded', refund_date: params[:refund_date])
    export_items.to_a.each(&:refund!)
    refund_requests.update_all(state: NhtRequest::ACCEPTED)
  end

  def request_refund!(current_user, order_params)
    return refund_requests.first if sent_refund_request?

    NhtRequest.create(object: self,
                      requester: current_user,
                      parameters: order_params,
                      action: 'refund!')
  end

  def sent_refund_request?
    refund_requests.any?
  end

  def waiting?
    state == 'waiting'
  end

  def not_shipped?
    state == 'waiting'
  end

  def shipped?
    state == 'delivered'
  end

  def completed?
    state == 'completed'
  end

  def refunded?
    state == 'refunded'
  end

  def passed_delivered_step?
    self.state == 'delivered' || self.state == 'completed' || self.state == 'refunded'
  end

  def can_edit_by?(current_user)
    return true if current_user.admin?
    return false if current_user.staff? && passed_delivered_step?
    return true
  end 

  def vietnamese_state
    case state
    when 'waiting' then 'Chốt sale'
    when 'delivered' then 'Đã gửi hàng'
    when 'completed' then 'Hoàn tất'
    when 'refunded' then 'Hoàn trả'
    else 'N/A'
    end
  end

  def created_by(user)
    update_columns(creator_id: user.id)
  end

  def total_of_wait_time
    (receive_date.to_date - created_at.to_date).to_i rescue nil
  end
end
