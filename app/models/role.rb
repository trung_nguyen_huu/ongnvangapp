class Role < ActiveRecord::Base
  ROLES = [:admin, :subadmin, :staff, :customer, :seller]

  validates :name, presence: true, uniqueness: true

  has_and_belongs_to_many :users

  #class methods
  class << self
    ROLES.each do |role|
      define_method("#{role}") do 
        self.find_by_name(role)
      end
    end
  end

  scope :except_customer, -> { where.not(name: 'customer') }
  scope :except_staff, -> { where.not(name: 'staff') }
  scope :except_admin, -> { where.not(name: 'admin') }
  scope :except_subadmin, -> { where.not(name: 'subadmin') }

  ROLES.each do |role|
    define_method("#{role}?") do 
      name == "#{role}"
    end
  end

  def vietnamese_name
    case name
    when 'admin' then 'Admin'
    when 'staff' then 'Nhan vien'
    when 'subadmin' then 'Quan ly'
    when 'seller' then 'Nhan vien ban hang'
    when 'customer' then 'Khach hang'
    end
  end
end
