class Supplier < ActiveRecord::Base
  extend FriendlyId
  friendly_id :slug_candidates, use: :slugged

  has_many :phones, class_name: 'Phone', as: :phoneable
  accepts_nested_attributes_for :phones, allow_destroy: true, reject_if: :reject_if_phone_blank

  has_and_belongs_to_many :products, dependent: :destroy
  accepts_nested_attributes_for :products, allow_destroy: true
  has_many :import_items, dependent: :nullify

  validates :name, presence: true, length: {in: 2..255}
  validates_length_of :coso, in: 2..255, allow_nil: true
  validates_length_of :address, in: 2..255, allow_nil: true
  validates_length_of :note, in: 2..10000, allow_nil: true

  #SCOPES
  default_scope { order(updated_at: :desc) }

  #METHODs
  def list_phones
    phones.pluck(:number).join(", ")
  end

  private
  def slug_candidates
    [
      :name,
      [:name, :coso]
    ]
  end

  def reject_if_phone_blank(attributes)
    attributes[:number].blank?
  end
end
