class Ability
  include CanCan::Ability
  def initialize(user)
      user ||= User.new # guest user (not logged in)

      alias_action :new, to: :create
      alias_action :edit, to: :update

      if user.admin?
        can :manage, :all
      end

      if user.seller?
        can :read, Product

        can :create, Customer
        can :read, Customer, orders: { seller_id: user.id }
        can :read, Customer, creator_id: user.id
        can :update, Customer do |customer|
          customer.creator_id == user.id && customer.created_at >= 1.hours.ago
        end

        can :read, Order, ["orders.seller_id = ? OR orders.creator_id = ?", user.id, user.id] do |order|
          order.seller_id == user.id || order.creator_id == user.id
        end
        can :create, Order
        can :update, Order do |order|
          order.seller_id == user.id && order.waiting?
        end
        can :ship, Order do |order|
          order.seller_id == user.id && order.waiting?
        end
        can :received, Order do |order|
          order.seller_id == user.id  && order.shipped?
        end
        can :complete, Order do |order|
          order.seller_id == user.id  && order.shipped?
        end
      end

      if user.subadmin?
        can :read, Product
        cannot :destroy, Product

        can :read, Order
        can :create, Order
        can :update_note, Order
        can [:update, :destroy], Order do |order|
          order.waiting?
        end 
        cannot [:update, :destroy], Order do |order|
          order.shipped?
        end
        can :ship, Order do |order|
          order.waiting?
        end
        can :received, Order do |order|
          order.shipped?
        end
        can :complete, Order do |order|
          order.shipped?
        end
        can :refund, Order do |order|
          !order.refunded?
        end

        can [:read, :create, :update], Customer
        can :destroy, Customer do |customer|
          customer.creator_id == user.id && customer.created_at >= 1.hours.ago
        end

        can :create, User
        can :read, User
        can [:update, :destroy], User do |user|
          user.seller? || (user.roles.count == 1 && user.staff?)
        end

        can :assign_roles, Role, ['roles.name <> ? AND roles.name <> ?', 'admin', 'subadmin'] do |role|
          !role.admin? && !role.subadmin?
        end

        can :create, ImportHistory
        can :read, ImportHistory
        can [:update, :destroy], ImportHistory do |import_history|
          import_history.creator_id == user.id && import_history.created_at >= 24.hours.ago
        end

        can :all, Supplier

        can [:read, :create], Commission
        can [:update, :destroy], Commission do |commission|
          commission.created_at >= 1.days.ago
        end
      end

      if user.staff?
        # can :read, Commission, staff_id: user.id
      end
  end
end
