class ImportItem < ActiveRecord::Base
  acts_as_paranoid

  belongs_to :variant, -> { with_deleted }
  
  belongs_to :supplier
  belongs_to :import_history, -> { with_deleted }, class_name: 'ImportHistory',
                              inverse_of: :import_items, touch: true,
                              foreign_key: 'import_history_id'

  #Xem xet moi quan he
  has_many :export_items, class_name: 'ExportItem'
  has_many :export_items_for_sell, -> { for_sell }, class_name: 'ExportItem'
  has_many :line_items, class_name: 'LineItem', through: :export_items

  # VALIDATIONS
  validates :variant, presence: true
  validates :cost_per_item, numericality: { greater_than_or_equal_to: 0 }, allow_nil: true
  validates :price_per_item, numericality: { greater_than_or_equal_to: 0 }
  validates :discount_per_item, numericality: { greater_than_or_equal_to: 0 }
  validates :quantity, numericality: {only_integer: true, greater_than_or_equal_to: 0 }
  
  #SCOPES
  scope :available, -> { where("count_in_stock >= ?", 1) }
  scope :for_variants, ->(variant_ids) { where(variant_id: variant_ids) }

  #Callbacks
  before_save :recalculate_count_in_stock
  after_save :persist_count_in_stock_on_variant
  after_destroy :persist_count_in_stock_on_variant
  after_save :persist_data_on_import_history
  after_destroy :persist_data_on_import_history
  after_save :persist_origin_price_on_line_item_and_order
  before_save :set_default_cost_per_item


  def cost
    cost_per_item * quantity
  end

  def sold_quantity
    quantity - count_in_stock
  end

  def set_default_cost_per_item
    if !cost_per_item || cost_per_item == 0
      self.cost_per_item = variant.cost
    end
  end

  #called by export item (when line item is updated) to persist count in stock
  def recalculate_and_persist_count_in_stock
    recalculate_count_in_stock()
    self.update_columns(count_in_stock: self.count_in_stock)

    persist_count_in_stock_on_variant()
  end
  
  def export_price_to_variant
    variant.update_attributes(price: self.price_per_item)
  end

  private
  #Khi nguoi dung cap nhat quantity cua import item, ta phai tinh lai count_in_stock
  def recalculate_count_in_stock
    self.count_in_stock = self.quantity - export_items_for_sell.sum(:quantity)
    if count_in_stock < 0
      self.count_in_stock = 0
    end
  end

  def persist_data_on_import_history
    import_history.recalculate_and_persist_data
  end
  
  def persist_count_in_stock_on_variant
    variant.persist_count_in_stock unless variant.destroyed?
  end

  def persist_origin_price_on_line_item_and_order
    line_items.each do |li|
      li.recalculate_origin_price_from_export_items
      li.persist_data_on_order
    end
  end
end
