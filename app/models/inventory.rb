class Inventory < ActiveRecord::Base
  self.table_name = 'variants'
  acts_as_paranoid

  has_many :import_items_in_stock, -> { where("import_items.count_in_stock > ?", 0) } ,
    foreign_key: 'variant_id',
    class_name: 'ImportItem'
  belongs_to :product, -> { with_deleted }

  default_scope -> { includes(:import_items_in_stock).where("variants.count_in_stock > ?", 0) }

  def name
    product.name + " (" + option_values + ")"
  end

  def tong_von_ton_kho
    import_items_in_stock.to_a.sum do |import_item|
      import_item.cost_per_item * import_item.count_in_stock
    end
  end

  def tong_gia_tri_ton_kho
    price * count_in_stock
  end

  def so_luong_ton_kho
    count_in_stock
  end
end
