class Customer < ActiveRecord::Base
  include NumberGenerator

  extend FriendlyId
  friendly_id :number, slug_column: :number, use: :slugged

  acts_as_paranoid

  #SCOPES
  default_scope { order(updated_at: :desc) }

  def generate_number(options = {})
    options[:prefix] ||= 'KH'
    options[:length] ||= 3
    super(options)
  end

  validates :name, presence: true, length: {in: 2..255}
  validates_length_of :phone, in: 2..255, allow_nil: true
  validates_length_of :address, in: 2..255, allow_nil: true
  validates_length_of :id_number, in: 2..255, allow_nil: true
  validates_length_of :email, in: 2..255, allow_nil: true
  validates_length_of :facebook, in: 2..500, allow_nil: true
  validates_length_of :job, in: 2..255, allow_nil: true
  validates_length_of :character, in: 2..10000, allow_nil: true

  has_many :orders
  belongs_to :creator, class_name: 'User'

  def self.to_csv(options = {})
    csv_columns = ['MKH', 'KHACH HANG', 'PHONE', 'DIA CHI',
                    'NGAY SINH', 'CMND', 'EMAIL', 'FACEBOOK',
                    'NGHE NGHIEP', 'TINH CACH', 'CREATED AT']
    CSV.generate(options) do |csv|
      csv << csv_columns
      all.find_each do |customer|
        csv << customer.attributes.values_at('number', 'name', 'phone',
                                            'address', 'birthday', 'id_number',
                                            'email', 'facebook', 'job', 'character', 'created_at')
      end
    end
  end

  def description
    "#{name} - #{phone} (#{number})"
  end

  def created_by(user)
    update_columns(creator_id: user.id)
  end
end
