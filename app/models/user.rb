class User < ActiveRecord::Base
  acts_as_paranoid
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_and_belongs_to_many :roles
  has_many :orders, class_name: 'Order', foreign_key: 'seller_id'
  has_many :line_items, through: :orders
  # has_many :commissions, foreign_key: 'staff_id', dependent: :destroy
  has_many :commissions, through: :orders
  has_many :commissions_in_month, foreign_key: 'seller_id', class_name: 'CommissionInMonth'

  validates :name, presence: true

  scope :staff, -> { joins(:roles).where("roles.name = ?", 'staff')}
  scope :except_customer, -> { joins(:roles).where.not("roles.name = ?", 'customer')}
  scope :except_admin_and_subadmin, -> {
    where("users.id NOT IN (?)", User.joins(:roles)
                                      .unscope(:order)
                                      .where("roles.name = ? OR roles.name = ?", 'admin', 'subadmin')
                                      .select("users.id")
                                      .distinct)
  }

  default_scope { order('created_at DESC') }

  def tong_hoa_hong_trong_thang
    # commissions.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month).sum(:amount)
    0
  end

  def admin?
    roles.any? do |role|
      role.admin?
    end
  end

  def staff?
    roles.any? do |role|
      role.staff?
    end
  end

  def subadmin?
    roles.any? do |role|
      role.subadmin?
    end
  end

  def seller?
    roles.any? do |role|
      role.seller?
    end
  end

  def customer?
    roles.any? do |role|
      role.customer?
    end
  end

  def highest_role
   return 'admin' if admin?
   return 'Quan ly' if subadmin?
   return 'Nhan vien ban hang' if seller?
   return 'Nhan vien' if staff?
   return 'N/A'
  end
end
