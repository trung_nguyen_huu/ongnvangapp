class PageConfiguration
  CONFIG = {
    products: 20,
    variants: 20,
    customers: 20,
    suppliers: 20,
    default: 20
  }
end
