class OrderUpdater
  attr_reader :order
  delegate :line_items, :delivery_fee_total, :other_fee_total, to: :order

  def initialize(order)
    @order = order
  end

  def calculate_and_persist_totals
    calculate_totals
    persist_total
  end

  def calculate_totals
    order.item_total = item_total
    order.discount_amount_total = discount_amount_total
    order.total = total
    order.debit_total = debit_total

    order.origin_price_total = origin_price_total
  end

  def persist_total
    order.update_columns(item_total: order.item_total,
                        discount_amount_total: order.discount_amount_total,
                        delivery_fee_total: order.delivery_fee_total,
                        total: order.total,
                        origin_price_total: order.origin_price_total,
                        debit_total: order.debit_total)
  end
  private

  def total
    item_total + delivery_fee_total + other_fee_total
  end

  def debit_total
    result = item_total - origin_price_total - other_fee_total 
    if !order.buyer_pay_for_delivery_fee
      result = result - delivery_fee_total
    end

    result
  end

  def item_total
    line_items.to_a.sum do |item|
      item.price_with_sale*item.quantity
    end
  end

  def discount_amount_total
    line_items.to_a.sum do |item|
      item.discount_amount*item.quantity
    end
  end

  def origin_price_total
    line_items.to_a.sum { |item| item.origin_price_total }
  end
end
