class ImportHistory::Statist
  attr_reader :from_date, :to_date

  def initialize(from_date, to_date)
    @from_date = from_date.to_date rescue Time.now.beginning_of_day
    @to_date   = to_date.to_date rescue Time.now.end_of_day
  end

  def import_histories
    @import_histories ||= ImportHistory.within(from_date, to_date)
  end

  def variants 
    @variants ||= Variant.imported_within(from_date, to_date)
                         .select('variants.*',
                                 'SUM(import_items.quantity) AS import_quantity')
                         .group('variants.id')
  end

  def other_items
    OtherItem.imported_in_import_histories(import_histories.select('import_histories.id'))
  end

  def import_histories_count
    import_histories.count
  end

  def number_of_items
    import_histories.sum(:number_of_items)
  end

  def cost_total
    import_histories.sum(:cost_total)
  end

  def price_total 
    import_histories.sum(:price_total)
  end

  def discount_total
    import_histories.sum(:discount_total)
  end

  def debit_total
    import_histories.sum(:debit_total)
  end

  def other_fee_total
    import_histories.sum(:other_fee_total)
  end

  def total_cost_of_inventory
    ImportItem.where(import_history_id: import_histories.select(:id)).sum("cost_per_item*count_in_stock")
  end

  class << self
    def within(from_date, to_date)
      new(from_date, to_date)
    end
  end
end
