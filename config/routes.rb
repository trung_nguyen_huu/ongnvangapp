Rails.application.routes.draw do
  resources :other_import_items
  resources :suppliers
  # resources :customers
  devise_for :users
  root 'admin/base#angular'
  get 'home/welcome'

  namespace :admin do 
    resources :products, only: [:index, :show, :create, :update, :destroy] do
      collection do
        get :search
        get :all
        get :get_excel
      end
      # resources :variants
    end
    resources :variants do 
      collection do 
        get :all
      end

      member do 
        get :check_stock
      end
    end
    resources :customers do
      collection do
        get :get_excel
      end
    end
    resources :suppliers do
      collection do
        get :all
      end
    end
    resources :import_histories do
      resources :import_items
      resources :other_import_items
      member do 
        put :export_prices
      end

      collection do 
        get :get_excel
      end
    end
    resources :orders do
      resources :line_items
      member do
        put :ship
        put :received
        put :refund
        put :update_note
      end
      collection do
        get :get_excel
      end

      resources :commissions
    end

    resources :users, only: [] do
      collection do 
        get :get_current_user
        post :update_password
        post :update_info
      end

    end

    resources :other_items, only: [] do
      collection do 
        get :all
      end
    end

    resources :staffs do
      collection do 
        get :all
        get :roles
      end

      resources :commissions_in_month
    end

    resources :commissions_in_month, only: [] do 
      resources :commission_pays, only: [:create, :update, :destroy]
    end

    resources :inventories do 
      collection do 
        get :get_excel
      end
    end

    resources :nht_requests do 
      member do 
        put :accept
        put :deny
      end
    end
    
    resources :refund_requests do 
      member do 
        put :accept
        put :deny
      end
    end

    get 'dashboard/staff', to: 'dashboard#staff'
    get 'dashboard/order', to: 'dashboard#order'
  end
end
