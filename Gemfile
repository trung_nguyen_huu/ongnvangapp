source 'https://rubygems.org'

ruby '2.2.4'
# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.5'
# Use postgresql as the database for Active Record
gem 'pg', '~> 0.15'
# See https://github.com/rails/execjs#readme for more supported runtimes
gem 'therubyracer', platforms: :ruby

group :development do
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'guard-ctags-bundler'
end

group :production do
  gem 'rails_12factor'
  gem "unicorn"
end

group :development, :test do
  gem 'pry-rails'
  gem 'awesome_print'
  gem "better_errors"
  gem 'binding_of_caller'
  gem 'pry-byebug'
  gem 'meta_request'
  gem 'rspec-rails', '~> 3.4'
  gem 'factory_girl_rails'
  gem 'database_cleaner'
  gem 'parallel_tests'
  gem 'zeus-parallel_tests'
end

group :development, :production do 
  gem 'puma'
  gem 'angular-rails-templates'
  gem 'angular_rails_csrf'
  # Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
  gem 'turbolinks'
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', '~> 0.4.0', group: :doc

  # Use Uglifier as compressor for JavaScript assets
  gem 'uglifier', '>= 1.3.0'
  # Use CoffeeScript for .coffee assets and views
  gem 'coffee-rails', '~> 4.1.0'

  # Use jquery as the JavaScript library
  gem 'jquery-rails'
end

# gem 'figaro'
gem 'devise'
# gem 'acts_as_paranoid', github: 'ActsAsParanoid/acts_as_paranoid', branch: 'master'
gem "paranoia", :github => "rubysherpas/paranoia", :branch => "rails4"
gem 'friendly_id', '~> 5.1.0'
gem 'kaminari'
gem 'ransack'
gem 'cancancan', '~> 1.10'
gem 'groupdate' 
gem 'money-rails'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'

gem 'rubyzip', '= 1.0.0'
gem 'axlsx', '= 2.0.1'
gem 'axlsx_rails'
gem 'acts_as_xlsx'
