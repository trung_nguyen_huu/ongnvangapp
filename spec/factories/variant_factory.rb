FactoryGirl.define do
  factory :variant do 
    option_values '500ml'
    cost 100
    price 150

    transient do 
      origin_price 101
      count_in_stock 10
      cost_per_item nil
    end

    trait :in_stock do 
      after(:create) do |variant, evaluator|
        create_list(:import_item, 1,
                    variant: variant,
                    quantity: evaluator.count_in_stock,
                    cost_per_item: evaluator.origin_price)
        variant.reload
      end
    end

  end
end
