FactoryGirl.define do

  sequence :email do |n|
    "staff_#{n}@gmail.com"
  end

  factory :user do 
    name 'Ong Vang NHT'
    email
    password '123123123'


    trait :admin_role do 
      after(:create) do |user, eval|
        user.roles << Role.find_or_create_by(name: 'admin')
        user.roles << Role.find_or_create_by(name: 'staff')
        user.reload
      end
    end

    trait :subadmin_role do 
      after(:create) do |user, eval|
        user.roles << Role.find_or_create_by(name: 'subadmin')
        user.roles << Role.find_or_create_by(name: 'staff')
        user.reload
      end
    end

    trait :staff_role do 
      after(:create) do |user, eval|
        user.roles << Role.find_or_create_by(name: 'staff')
        user.reload
      end
    end

    trait :seller_role do 
      after(:create) do |user, eval|
        user.roles << Role.find_or_create_by(name: 'seller')
        user.roles << Role.find_or_create_by(name: 'staff')
        user.reload
      end
    end
  end
end
