FactoryGirl.define do 
  factory :import_history do

    transient do 
      import_items_count 1
      cost_per_item 100
      price_per_item 150
      discount_per_item 15
      quantity 1
    end

    transient do 
      other_import_items_count 1
      cost_of_other_import_item 100
    end

    trait :with_import_items do 
      after(:create) do |import_history, evaluator|
        create_list(:import_item, evaluator.import_items_count,
                    cost_per_item: evaluator.cost_per_item,
                    discount_per_item: evaluator.discount_per_item,
                    price_per_item: evaluator.price_per_item,
                    quantity: evaluator.quantity,
                    import_history: import_history)
        import_history.reload
      end
    end

    trait :with_other_import_items do 
      after(:create) do |import_history, evaluator|
        create_list(:other_import_item, evaluator.other_import_items_count,
                    cost: evaluator.cost_of_other_import_item,
                    import_history: import_history )
        import_history.reload
      end
    end

    factory :import_history_with_items, traits: [:with_import_items, :with_other_import_items]

  end
end
