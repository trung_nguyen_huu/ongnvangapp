FactoryGirl.define do 
  factory :customer do 
    email
    name 'Ong vang'
    association :creator, factory: :user

    transient do 
      order_seller nil
    end

    after(:create) do |customer, eva|
      create(:order, seller: eva.order_seller, customer: customer) if eva.order_seller
      customer.reload
    end
  end
end
