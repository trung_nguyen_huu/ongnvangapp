FactoryGirl.define do
  factory :line_item do 
    quantity 1
    order
    variant

    transient do
      origin_price 100
    end

    trait :with_variant do 
      after(:build) do |line_item, evaluator|
       line_item.variant = create(:variant, :in_stock,
                                   count_in_stock: 10,
                                   origin_price: evaluator.origin_price,
                                   price: line_item.price,
                                   discount_amount: line_item.discount_amount)
      end
    end
  end
end
