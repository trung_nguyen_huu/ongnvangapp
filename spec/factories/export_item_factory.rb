FactoryGirl.define do 
  factory :export_item do
    state ExportItem::SOLD
    quantity 1

    import_item
  end
end
