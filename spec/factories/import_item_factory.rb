FactoryGirl.define do 
  factory :import_item do
    cost_per_item 100
    discount_per_item 12
    price_per_item 120
    quantity 1
    variant
    import_history

    transient do 
      export_items_count 1
      export_items_quantity 1
      import_history_created_at nil
    end

    after(:create) do |import_item, evaluator|
      if evaluator.import_history_created_at
        import_item.import_history.update_columns(created_at: evaluator.import_history_created_at)
      end
    end

    trait :with_export_items do 
      after(:create) do |import_item, evaluator|
        create_list(:export_item, evaluator.export_items_count, quantity: evaluator.export_items_quantity, import_item: import_item)
        import_item.reload
      end
    end

    transient do 
      variant_price 100
    end

    trait :custom_variant do 
      after(:create) do |import_item, evaluator|
        import_item.variant.update_attributes(price: evaluator.variant_price)
      end
    end

  end
end
