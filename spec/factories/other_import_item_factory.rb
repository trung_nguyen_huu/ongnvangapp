FactoryGirl.define do
  factory :other_import_item do
    name 'Ban ghe'
    cost 100
    import_history

    transient do
      imported_at nil
    end

    after(:create) do |other_import_item, evaluator|
      if evaluator.imported_at
        other_import_item.import_history
                   .update_columns(created_at: evaluator.imported_at)
      end
    end
  end
end
