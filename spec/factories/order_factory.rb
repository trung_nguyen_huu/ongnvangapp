FactoryGirl.define do
  factory :order do

    transient do 
      line_items_count 1
      line_items_price 100
      line_items_discount_amount 10
      line_items_origin_price 50
      line_items_quantity 1
      manual_created_at nil
    end
    
    after(:create) do |order, evaluator|
      if evaluator.manual_created_at
        order.update_columns(created_at: evaluator.manual_created_at)
      end
    end

    trait :order_with_line_items do 
      after(:create) do |order, evaluator|
        create_list(:line_item, evaluator.line_items_count, 
                    :with_variant,
                    quantity: evaluator.line_items_quantity,
                    price: evaluator.line_items_price,
                    discount_amount: evaluator.line_items_discount_amount,
                    origin_price: evaluator.line_items_origin_price,
                    order: order)
        order.reload
      end
    end

  end
end

# order has_many line_items, line_item has_many export_items
# export_item belongs_to import_item
#
# CHI CAN MOCK PRICE TRNE LINE ITEM VA ORDER
# item_total -> line_item.price
# total -> item_total + delivery_fee_total
# discount_amount_total
#
