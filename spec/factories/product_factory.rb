FactoryGirl.define do 
  factory :product do 
    name 'Mat ong'

    after(:build) do |product, evaluator|
      if product.variants.count < 1
        product.variants << build(:variant, product: nil)
      end
    end

    transient do 
      option_values ['500ml']
    end

    trait :with_variants do 
      after(:create) do |product, evaluator|
        evaluator.option_values.each do |option_value| 
          create(:variant, option_values: option_value, product: product)
        end
        product.reload
      end
    end

    factory :product_with_variants, traits: [:with_variants]
  end
end
