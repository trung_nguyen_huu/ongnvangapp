FactoryGirl.define do
  factory :nht_request do
  end

  factory :refund_request, parent: :nht_request do 
    association :object, factory: :order
    action :refund!
  end
end
