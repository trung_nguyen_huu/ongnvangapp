require 'spec_helper'
require 'active_record'
require 'support/database_cleaner'
require 'yaml'

connection_info = YAML.load_file("config/database.yml")["test_without_parallel"]
ActiveRecord::Base.establish_connection(connection_info)

# RSpec.configure do |config|
#   config.around do |example|
#     ActiveRecord::Base.transaction do
#       example.run
#       raise ActiveRecord::Rollback
#     end
#   end
# end
