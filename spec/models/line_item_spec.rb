require 'rails_helper'

RSpec.describe LineItem do 

  context 'get sale price from variant correctly' do
    let(:variant) { create(:variant, :in_stock, price: 100, sale_price: 80) }
    let(:line_item) { build(:line_item, variant: variant) }

    it 'when variant on sale' do
      allow(variant).to receive(:on_sale?).and_return(true)
      line_item.save
      expect(line_item.sale_price).to eq(80)
    end

    it 'when variant not on sale' do
      allow(variant).to receive(:on_sale?).and_return(false)
      line_item.save
      expect(line_item.sale_price).to eq(0)
    end
  end

  context '#price_with_sale' do
    let(:line_item) { build(:line_item, price: 100, sale_price: 80) }

    it 'with sale price' do 
      expect(line_item.price_with_sale).to eq(80)
    end

    it 'sale price = 0' do 
      line_item.sale_price = 0
      expect(line_item.price_with_sale).to eq(100)
    end

    it 'sale price nil' do 
      line_item.sale_price = nil
      expect(line_item.price_with_sale).to eq(100)
    end
  end
end
