require 'rails_helper'

RSpec.describe OtherImportItem, type: :model do
  context 'validations' do
    context 'invalid' do
      it 'if name blank?' do 
        item = build(:other_import_item, name: '')
        expect(item.valid?).to be false
      end

      it 'if cost_price nil' do 
        item = build(:other_import_item, cost: nil)
        expect(item.valid?).to be false
      end

      it 'if cost_price < 0' do 
        item = build(:other_import_item, cost: -1)
        expect(item.valid?).to be false
      end
    end

    context 'valid' do
      it 'if name present and cost_price > 0' do
        item = build(:other_import_item, cost: 10, name: 'ban ghe')
        expect(item.valid?).to be true
      end
    end
  end

  context 'find or create other item correctly' do 
    let!(:item) { build(:other_import_item, name: 'ban ghe') }

    it 'creat new other item' do 
      expect {
        item.save
      }.to change { OtherItem.count }.by(1) 
    end

    it 'use existing item' do
      create(:other_item, name: 'ban ghe')
      expect {
        item.save
      }.to change { OtherItem.count }.by(0) 
    end

    it 'handle string in name when find other item' do
      create(:other_item, name: 'ban ghe')
      item = build(:other_import_item, name: '  Ban   ghe  ')
      expect {
        item.save
      }.to change { OtherItem.count }.by(0) 
    end
  end
end
