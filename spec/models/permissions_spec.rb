require 'rails_helper'
require "cancan/matchers"

describe "User" do
  describe "abilities" do
    subject(:ability){ Ability.new(user) }
    let(:user){ nil }

    context "when is an seller" do
      let(:user){ create(:user, :seller_role) }

      context 'product permissions' do
        it 'can read product' do
          should be_able_to(:read, Product)
        end

        it 'can not update product' do
          should_not be_able_to(:update, Product)
        end

        it 'can not destroy product' do
          should_not be_able_to(:destroy, Product)
        end
      end

      context 'Customer permissions' do
        context 'update' do
          it "can not update customer that was created by other" do
            customer = build(:customer, creator: create(:user))
            should_not be_able_to(:update, customer)
          end

          it "only can update customer that was create by myself and within 1 hours" do
            customer = build(:customer, creator: user, created_at: Time.now)
            should be_able_to(:update, customer)
          end

          it "can not update customer that was created by myself and exceed 1 hours" do
            customer = build(:customer, creator: user, created_at: 2.hours.ago)
            should_not be_able_to(:update, customer)
          end
        end

        context 'create' do
          it 'can create customer' do
            should be_able_to(:create, Customer)
          end
        end

        context 'read' do
          let(:my_customer) { create(:customer, name: 'My customer') }
          let(:other_customer) { create(:customer, name: 'Other customer') }

          before(:each) do
            create(:order, customer: my_customer, seller: user)
            create(:order, customer: other_customer)
          end

          it 'can read only my customer' do
            should be_able_to(:read, my_customer)
          end

          it 'can not read other customer' do
            should_not be_able_to(:read, other_customer)
          end

          it 'can read customer that created by me' do
            customer = build(:customer, name: 'create by me', creator: user)
            should be_able_to(:read, customer)
          end

          it 'cannot read customer that created by other' do
            customer = build(:customer, name: 'create by me', creator: create(:user))
            should_not be_able_to(:read, customer)
          end
        end

        context 'destroy' do
          it 'can not destroy customer' do
            should_not be_able_to(:destroy, Customer.new)
          end
        end
      end

      context 'order permissions' do
        context 'create' do
          it 'can create order' do
            should be_able_to(:create, Order.new)
          end
        end

        context 'update order' do
          it "can update my order if it haven't delivery yet" do
            order = build(:order, number: 'MYORDER', seller: user)
            should be_able_to(:update, order)
          end

          it "can not update my order if it's delivered" do
            order = create(:order, number: 'MYORDER', seller: user)
            order.ship!(Time.now)
            should_not be_able_to(:update, order)
          end

          it "can not update other's order" do
            order = build(:order, number: 'OTHER', seller: create(:user))
            should_not be_able_to(:update, order)
          end
        end

        context 'read' do
          it 'only read my order' do
            my_order = build(:order, number: 'MY', seller: user)
            order_of_other = build(:order, number: 'OTHER', seller: create(:user))
            should be_able_to(:read, my_order)
            should_not be_able_to(:read, order_of_other)
          end
        end

        context 'destroy' do
          it 'can not destroy order' do
            should_not be_able_to(:destroy, Order.new)
          end
        end

        context 'other actions' do
          context '#ship' do
            it 'can ship my order' do 
              order = create(:order, number: 'MYORDER', seller: user)
              should be_able_to(:ship, order)
            end

            it 'can not ship other order' do 
              order = build(:order, number: 'OTHER', seller: create(:user))
              should_not be_able_to(:ship, order)
            end
          end

          context '#received' do
            it 'can check received my order' do 
              order = create(:order, number: 'MYORDER', seller: user)
              should be_able_to(:received, order)
            end

            it 'can not check received on other order' do 
              order = build(:order, number: 'OTHER', seller: create(:user))
              should_not be_able_to(:received, order)
            end
          end
        end
      end
      #END ORDER PERMISSION

      context 'can not do any action on ImportHistory, Supplier, User' do
        it '' do
          should_not be_able_to(:read, Supplier)
          should_not be_able_to(:update, Supplier)
          should_not be_able_to(:destroy, Supplier)

          should_not be_able_to(:read, ImportHistory)
          should_not be_able_to(:update, ImportHistory)
          should_not be_able_to(:destroy, ImportHistory)

          should_not be_able_to(:read, User)
          should_not be_able_to(:update, User)
          should_not be_able_to(:destroy, User)
        end
      end
    end

    
    #BEGIN SUBADMIN PERMISSIONS
    context "when is an subadmin (manager)" do
      let(:user){ create(:user, :subadmin_role) }

      context 'products permissions' do
        context 'read' do
          it 'can read all product' do
            should be_able_to(:read, Product.new)
          end
        end

        context 'create' do
          it 'can not create product' do
            should_not be_able_to(:create, Product.new)
          end
        end

        context 'update' do
          it 'can not update product' do
            should_not be_able_to(:update, create(:product))
          end
        end

        context 'destroy' do
          it 'can not destroy product' do
            should_not be_able_to(:destroy, create(:product))
          end
        end
      end

      context 'order permissions' do
        context 'read' do
          it 'can read all orders' do
            should be_able_to(:read, build(:order))
          end
        end

        context 'create' do
          it 'can create order' do
            should be_able_to(:create, build(:order))
          end
        end

        context 'update' do
          it 'can update order that havent shipped yet' do 
            order = build(:order, number: 'NOTSHIPPED')
            should be_able_to(:update, order)
          end

          it 'can not update order that had shipped' do 
            order = create(:order, number: 'SHIPPED')
            order.ship!(Time.now)
            should_not be_able_to(:update, order)
          end
        end

        context 'destroy' do
          it 'can destroy order that not shipped' do
            order = build(:order, number: 'NOTSHIPPED')
            should be_able_to(:destroy, order)
          end

          it 'can not destroy order that had shipped' do 
            order = create(:order, number: 'SHIPPED')
            order.ship!(Time.now)
            should_not be_able_to(:destroy, order)
          end
        end
      end

      context 'customer permissions' do
        it 'can read all customer' do
          should be_able_to(:read, Customer.new)
        end

        it 'can create customer' do
          should be_able_to(:create, Customer.new)
        end

        it 'can update all customer' do
          should be_able_to(:update, Customer.new)
        end

        it 'can destroy all customer' do
          should be_able_to(:destroy, Customer.new)
        end
      end

      context 'staff permissions' do
      end

      context 'other permissions' do
        it 'can not access Supplier' do
            should_not be_able_to(:read, Supplier.new)
            should_not be_able_to(:update, Supplier.new)
            should_not be_able_to(:destroy, Supplier.new)
            should_not be_able_to(:create, Supplier.new)
        end
      end
    end
    #END SUBADMIN PERMISSIONS
  end
end
