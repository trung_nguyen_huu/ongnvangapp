require 'rails_helper'

RSpec.describe ImportHistory do
  it 'init with valid data when having no import items' do
    import_history = create(:import_history)
    expect(import_history.delivery_fee_total).to eq(0)
    expect(import_history.other_fee_total).to eq(0)
    expect(import_history.number_of_items).to eq(0)
    expect(import_history.cost_total).to eq(0)
    expect(import_history.price_total).to eq(0)
    expect(import_history.discount_total).to eq(0)
    expect(import_history.debit_total).to eq(0)
  end


  let(:import_history) { create(:import_history) }

  it 'recalculate_and_persist_data after saving' do
    allow(import_history).to receive(:recalculate_and_persist_data)
    import_history.save
    expect(import_history).to have_received(:recalculate_and_persist_data)
  end

  context 'export price to variants' do 
    let(:import_history) {
      create(:import_history, :with_import_items,
             import_items_count: 1,
             price_per_item: 200)
    }

    let(:variant){ import_history.import_items.first.variant }

    it '#export_price_to_variants' do
      import_history.export_price_to_variants()
      variant.reload
      expect(variant.price).to eq(200)
    end
  end
end
