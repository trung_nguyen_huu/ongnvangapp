require 'rails_helper'

RSpec.describe ImportItem do 
  context 'validations' do
    it 'invalid if variant nil' do
      import_item = build(:import_item, variant: nil)
      expect(import_item.valid?).to be false
    end

    it 'valid if variant presence' do
      import_item = build(:import_item, variant: create(:variant))
      expect(import_item.valid?).to be true
    end

    context 'not invalid if cost,price or quantity < 0' do
      let(:import_item) { build(:import_item) }

      specify do 
        import_item.cost_per_item = -1
        expect(import_item.valid?).to be false
      end

      specify do 
        import_item.price_per_item = -1
        expect(import_item.valid?).to be false
      end

      specify do 
        import_item.discount_per_item = -1
        expect(import_item.valid?).to be false
      end

      specify do 
        import_item.quantity = -1
        expect(import_item.valid?).to be false
      end
    end
  end

  context 'persist count in stock on variant' do
    let(:import_item) { create(:import_item) }

    it 'when updating' do
      allow(import_item).to receive(:persist_count_in_stock_on_variant)
      import_item.save
      expect(import_item).to have_received(:persist_count_in_stock_on_variant)
    end

    it 'when destroying' do
      allow(import_item).to receive(:persist_count_in_stock_on_variant)
      import_item.destroy
      expect(import_item).to have_received(:persist_count_in_stock_on_variant)
    end
  end

  context 'persist data on import history' do
    let(:import_item) { create(:import_item) }

    it 'when updating' do
      allow(import_item).to receive(:persist_data_on_import_history)
      import_item.save
      expect(import_item).to have_received(:persist_data_on_import_history)
    end

    it 'when destroying' do
      allow(import_item).to receive(:persist_data_on_import_history)
      import_item.destroy
      expect(import_item).to have_received(:persist_data_on_import_history)
    end
  end

  context 'calculate count in stock correctly' do
    it 'when have no any export items' do
      import_item = create(:import_item, quantity: 5)
      expect(import_item.count_in_stock).to eq(5)
    end

    it 'when have export item' do 
      import_item = create(:import_item, :with_export_items,
                           quantity: 5,
                           export_items_count: 2,
                           export_items_quantity: 2)
      expect(import_item.count_in_stock).to eq(1)
    end

    it "ensure count in stock always = 0 if it's negative" do 
      import_item = create(:import_item, :with_export_items,
                           quantity: 5,
                           export_items_count: 1,
                           export_items_quantity: 7)
      expect(import_item.count_in_stock).to eq(0)
    end
  end

  context 'export price to variant' do 
    let(:import_item) {
      create(:import_item, :custom_variant,
            variant_price: 100, price_per_item: 200)
    }

    let(:variant) do
      import_item.variant
    end

    it '#export_price_to_variant' do
      import_item.export_price_to_variant()
      variant.reload
      expect(variant.price).to eq(import_item.price_per_item)
    end
  end

  context 'set default cost_per_item' do
    let(:variant) { build_stubbed(:variant, cost: 150) }
    let(:import_item) { build(:import_item, variant: variant) }

    it 'get cost of variant if cost_per_item nil' do
      import_item.cost_per_item = nil 
      import_item.save
      expect(import_item.cost_per_item).to eq(150)
    end

    it 'dont set if cost_per_item not nil' do
      import_item.cost_per_item = 10
      import_item.save
      expect(import_item.cost_per_item).to eq(10)
    end
  end
end
