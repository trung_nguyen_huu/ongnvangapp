require 'rails_helper'

RSpec.describe OrderUpdater do 
  context "should calculate totals correctly," do
    let(:order) do 
      create(:order, :order_with_line_items,
             delivery_fee_total: 20,
             line_items_count: 2,
             line_items_price: 100,
             line_items_discount_amount: 10,
             line_items_origin_price: 50,
             line_items_quantity: 2)
    end

    let(:updater) { OrderUpdater.new(order) }

    before(:each) do 
      updater.calculate_and_persist_totals
      order.reload
    end

    it 'item total' do
      expect(order.item_total).to eq(400)
    end

    it 'discount amount total' do
      expect(order.discount_amount_total).to eq(40)
    end

    it 'origin price total' do
      expect(order.origin_price_total).to eq(200)
    end

    it 'total' do
      expect(order.total).to eq(420)
    end

    it 'debit total' do
      expect(order.debit_total).to eq(420 - 200 - 40 - 20)
    end
  end
end

