require 'rails_helper'

RSpec.describe Order::Statist do 
  context 'within date range' do 
    before(:each) do
      @order1 = create(:order, :order_with_line_items,
                       number: 'OV111',
                       manual_created_at: 2.days.ago,
                       delivery_fee_total: 20,
                       line_items_count: 1,
                       line_items_price: 100,
                       line_items_discount_amount: 10,
                       line_items_origin_price: 50,
                       line_items_quantity: 1)

      @order2 = create(:order, :order_with_line_items,
                       number: 'OV112',
                       manual_created_at: 1.days.ago,
                       delivery_fee_total: 20,
                       line_items_count: 1,
                       line_items_price: 100,
                       line_items_discount_amount: 10,
                       line_items_origin_price: 50,
                       line_items_quantity: 1)

      @order3 = create(:order, :order_with_line_items,
                       number: 'OV113',
                       manual_created_at: 1.days.from_now,
                       delivery_fee_total: 20,
                       line_items_count: 1,
                       line_items_price: 100,
                       line_items_discount_amount: 10,
                       line_items_origin_price: 50,
                       line_items_quantity: 1)
    end

    let(:statist) do
      Order::Statist.new(2.days.ago, Time.now)
    end

    it '#number_of_orders' do 
      expect(statist.number_of_orders).to eq(2)
      # expect(statist.orders.map(&:number)).to match_array(%(OV123 OV124))
    end

    it '#total' do 
      expect(statist.total).to eq(@order1.total + @order2.total)
    end

    it '#debit_total' do 
      expect(statist.debit_total).to eq(@order1.debit_total + @order2.debit_total)
    end
    
    it 'correctly orders and order by created_at' do
      expect(statist.orders.map(&:number)).to match_array(['OV111', 'OV112'])
    end

  end
end
