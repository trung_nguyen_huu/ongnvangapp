require 'rails_helper'

RSpec.describe Order do
  let(:order) { create(:order) }

  it 'call calculate_and_persist_totals after saving to update totals' do
    allow(order).to receive(:calculate_and_persist_totals)
    order.save
    expect(order).to have_received(:calculate_and_persist_totals)
  end

  describe "#refund!" do
    let(:order_params) { { refund_date: Time.now } }
    let(:order) { create(:order) }
    let(:export_items) { order.export_items }

    before(:each) do
      export_item_1 = create(:export_item, state: ExportItem::SOLD)
      export_item_2 = create(:export_item, state: ExportItem::SOLD)
      allow(order).to receive(:export_items).and_return([export_item_1,
                                                         export_item_2])
    end

    it do
      order.refund!(order_params)
      expect(order.refund_date).to eq(order_params[:refund_date])
    end

    it do 
      order.refund!(order_params)
      expect(order).to be_refunded
    end

    it do 
      order.refund!(order_params)
      expect(export_items.map(&:state)).to match_array([ExportItem::REFUNDED,
                                                              ExportItem::REFUNDED])
    end
  end

  describe '#request_refund!' do 
    let(:current_user) { create(:user) }
    let(:order_params) { { 'refund_date' => Time.now } }
    let(:order) { create(:order) }
    let(:request) { order.request_refund!(current_user, order_params) }


    specify { expect(request.class).to eq(NhtRequest) }

    specify { expect(request.object).to eq(order) }

    specify { expect(request.requester).to eq(current_user) }

    specify { expect(request.action).to eq('refund!') }

    specify { expect(request.parameters).to eq(order_params) }
  end
end
