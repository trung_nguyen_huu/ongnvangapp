require 'rails_helper'

RSpec.describe ImportHistory::Statist do 
  context 'thong ke, tinh toan chinh xac cac chi phi' do 
    before(:each) do
      2.times do 
        create(:import_history_with_items,
               import_items_count: 1,
               cost_per_item: 100,
               price_per_item: 150,
               discount_per_item: 15,
               quantity: 1,
               other_import_items_count: 1,
               cost_of_other_import_item: 50,
               created_at: 2.days.ago)
      end

      create(:import_history_with_items,
             import_items_count: 1,
             cost_per_item: 100,
             price_per_item: 150,
             discount_per_item: 15,
             quantity: 1,
             other_import_items_count: 1,
             cost_of_other_import_item: 50,
             created_at: 2.days.from_now)
    end

    let(:statist) do 
      ImportHistory::Statist.within(2.days.ago, Time.now)
    end

    specify {
      expect(statist.number_of_items).to eq(2)
    }

    specify {
      expect(statist.cost_total).to eq(200)
    }

    specify {
      expect(statist.price_total).to eq(300)
    }

    specify {
      expect(statist.discount_total).to eq(30)
    }

    specify {
      expect(statist.debit_total).to eq(-30)
    }

    specify {
      expect(statist.other_fee_total).to eq(100)
    }
  end

  context 'things imported in date range.' do 
    context 'list import histories' do
      before(:each) do
        2.times do 
          create(:import_history_with_items,
                 import_items_count: 1,
                 cost_per_item: 100,
                 price_per_item: 150,
                 discount_per_item: 15,
                 quantity: 1,
                 other_import_items_count: 1,
                 cost_of_other_import_item: 50,
                 created_at: 2.days.ago)
        end

        create(:import_history_with_items,
               import_items_count: 1,
               cost_per_item: 100,
               price_per_item: 150,
               discount_per_item: 15,
               quantity: 1,
               other_import_items_count: 1,
               cost_of_other_import_item: 50,
               created_at: 2.days.from_now)
      end

      let(:statist) do 
        ImportHistory::Statist.within(2.days.ago, Time.now)
      end

      let(:import_histories) { statist.import_histories }

      it 'tinh chinh xac so luong import histories in range' do
        expect(import_histories.count).to eq(2)
      end

      it 'have ability to paginate' do 
        expect(import_histories).to be_respond_to(:page)
      end
    end


    context 'list variants' do
      let!(:product) do
        create(:product_with_variants,
               name: 'Mat ong',
               option_values: ['1l', '2l', '3l'])
      end
      let!(:variant_1l) { product.variant_by_option_values('1l') }
      let!(:variant_2l) { product.variant_by_option_values('2l') }
      let!(:variant_3l) { product.variant_by_option_values('3l') }

      before(:each) do
        create(:import_item,
               variant: variant_1l,
               quantity: 3,
               import_history_created_at: 2.days.ago)

        create(:import_item,
               variant: variant_2l,
               quantity: 3,
               import_history_created_at: 2.days.ago)

        create(:import_item,
               variant: variant_1l,
               quantity: 3,
               import_history_created_at: 1.days.ago)

        create(:import_item,
               variant: variant_3l,
               quantity: 3,
               import_history_created_at: 1.days.from_now)
      end

      let(:statist) do 
        ImportHistory::Statist.within(2.days.ago, Time.now)
      end

      subject { static.variants }

      it 'get correct variants in import histories range' do 
        expect(statist.variants.map(&:option_values)).to match_array(%w(1l 2l))
      end

      it 'lay chinh xac so luog nhap trong range' do
        expect(statist.variants.map(&:import_quantity)).to match_array([6, 3])
      end
    end


    context 'list other items' do
      before(:each) do
        create(:other_import_item,
               name: 'ban ghe',
               imported_at: 2.days.ago)

        create(:other_import_item,
               name: 'ban ghe',
               imported_at: 2.days.ago)

        create(:other_import_item,
               name: 'thung ong',
               imported_at: 1.days.ago)

        create(:other_import_item,
               name: 'xe tai',
               imported_at: 1.days.from_now)
      end

      let(:statist) do 
        ImportHistory::Statist.within(2.days.ago, Time.now)
      end

      it 'list other items' do 
        expect(statist.other_items.map(&:name)).to match_array(%w(ban\ ghe thung\ ong))
      end
    end
  end
end
