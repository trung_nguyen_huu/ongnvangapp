require 'rails_helper'

RSpec.describe ImportUpdater do
  context 'should calculate correctly,' do 
    before(:each) do
      @import_history = create(:import_history)
      2.times do 
        create(:import_item,
               cost_per_item: 100,
               discount_per_item: 15,
               price_per_item: 150,
               quantity: 10,
               import_history: @import_history)

      end
      @import_history.reload
    end

    before(:each) do
      2.times do 
        create(:other_import_item,
               cost: 50,
               import_history: @import_history)
      end
      @import_history.reload
    end

    before(:each) do 
      @updater = ImportUpdater.new(@import_history)
      @updater.calculate_totals
    end

    subject { @import_history }

    it '#number_of_items' do
      expect(subject.number_of_items).to eq(2)
    end

    it '#cost_total' do
      expect(subject.cost_total).to eq(100*10 + 100*10)
    end

    it '#price_total' do
      expect(subject.price_total).to eq(150*10 + 150*10)
    end

    it '#discount_total' do
      expect(subject.discount_total).to eq(15*10 + 15*10)
    end

    it '#debit_total' do
      price_total = 150*10 + 150*10
      cost_total = 100*10 + 100*10
      discount_total = 15*10 + 15*10

      expect(subject.debit_total).to eq(price_total - (cost_total + 
                                                       subject.delivery_fee_total + 
                                                       subject.other_fee_total + 
                                                       discount_total))
    end

    it '#other_fee_total' do
      expect(subject.other_fee_total).to eq(100)
    end
  end
end
