require 'rails_helper'

RSpec.describe User do 

  context 'staff' do
    let!(:admin) { create(:user, :admin_role) }
    let!(:staff) { create(:user, :staff_role) }

    it '.staff' do
      expect(User.staff.map(&:email)).to match_array([staff.email, admin.email])
    end
  end

  let(:user) { create(:user, :admin_role) }

  it "mark deleted_at when destroy" do 
    user.destroy
    user.reload

    expect(user.deleted_at).not_to be_nil
    expect(User.only_deleted.find(user.id)).not_to be_nil
  end

end
