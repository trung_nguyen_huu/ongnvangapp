require 'rails_helper'

RSpec.describe Product::Statist do
  let(:statist) do
    Product::Statist.new(2.days.ago, Time.now)
  end

  before(:each) do
    create(:variant, :in_stock,
           price: 150,
           origin_price: 100,
           option_values: '1l',
           count_in_stock: 2)

    create(:variant, :in_stock,
           price: 150,
           origin_price: 100,
           option_values: '2l',
           count_in_stock: 2)

    create(:variant,
           price: 150,
           origin_price: 100,
           option_values: '3l')
  end

  it '#variants_in_stock' do
    expect(statist.in_stock_variants.map(&:option_values)).to match_array(['1l', '2l'])
  end

  it '#cost_total' do 
    expect(statist.cost_total).to eq(400)
  end

  it '#price_total' do
    expect(statist.price_total).to eq(300)
  end
end
