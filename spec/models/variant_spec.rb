require 'rails_helper'

RSpec.describe Variant do
  context 'calculate count_in_stock correctly' do 
    let(:variant) { create(:variant) }

    it 'with new import items' do 
      variant.import_items = [create(:import_item, quantity: 5), create(:import_item, quantity: 2)]
      expect(variant.reload.count_in_stock).to eq(7)
    end
  end

  context 'on sale' do
    it 'should on sale' do
      variant = build(:variant, price: 100, sale_price: 80, sale_from_date: Time.now, sale_to_date: 3.days.from_now)
      expect(variant.on_sale?).to be true
    end

    it 'sale in day' do
      variant = build(:variant, price: 100, sale_price: 80, sale_from_date: Time.now, sale_to_date: Time.now)
      expect(variant.on_sale?).to be true
    end
    
    context 'should not on sale' do
      it 'when sale price nil' do
        variant = build(:variant, price: 100, sale_price: nil, sale_from_date: Time.now, sale_to_date: 3.days.from_now)
        expect(variant.on_sale?).to be false
      end

      it 'should not on sale when sale day expire' do
        variant = build(:variant, price: 100, sale_price: 80, sale_from_date: 3.days.ago, sale_to_date: 2.days.ago)
        expect(variant.on_sale?).to be false
      end

      it 'should not on sale when sale from day in future' do
        variant = build(:variant, price: 100, sale_price: 80, sale_from_date: 1.days.from_now, sale_to_date: 2.days.from_now)
        expect(variant.on_sale?).to be false
      end
    end

    context '#price_with_sale' do
      let(:variant) { build(:variant, price: 100, sale_price: 80) }

      it 'price should eq sale price if on sale' do
        allow(variant).to receive(:on_sale?).and_return(true)
        expect(variant.price_with_sale).to eq(80)
      end

      it 'price should eq price if not on sale' do
        allow(variant).to receive(:on_sale?).and_return(false)
        expect(variant.price_with_sale).to eq(100)
      end
    end
  end
end
