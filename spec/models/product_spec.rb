require 'rails_helper'

RSpec.describe Product do 
  it "invalid if product don't have any variant" do
    product = Product.new(name: 'Product 1')
    expect(product.valid?).to be false
  end

  it 'valid product if product contain at least one variant' do 
    product = Product.new(name: 'Product 1')
    product.variants << Variant.new(option_values: '500ml', price: 12.5)

    expect(product.valid?).to be true
  end

  it '#variant_by_option_values' do
    product = create(:product_with_variants,
                     name: 'Mat ong',
                     option_values: ['300ml', '1l'])
    variant = product.variant_by_option_values('300ml')

    expect(variant.option_values).to eq('300ml')
  end
end
