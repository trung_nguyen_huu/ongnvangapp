require 'rails_helper'

RSpec.describe ImportItemParams do 
  let(:params) {
    ActionController::Parameters.new(attributes_for(:import_item))
  }

  let!(:parser) {
    ImportItemParams.new(params, current_user)
  }

  context 'admin' do
    let(:current_user) { create(:user, :admin_role) }
    it '#by_role' do
      expect(parser.by_role).to have_key(:cost_per_item)
    end
  end

  context 'subadmin' do
    let(:current_user) { create(:user, :subadmin_role) }

    it '#by_role' do
      expect(parser.by_role).not_to have_key(:cost_per_item)
    end
  end


end
