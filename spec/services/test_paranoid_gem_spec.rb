require 'rails_helper'
RSpec.describe 'testing paranoid gem' do
  let(:product) { create(:product) }
  let(:variant) { create(:variant, product: product) }

  before(:each) do
    product.destroy
    variant.reload
  end

  it 'not really delete object' do 
    expect(product.deleted_at).not_to be_nil
  end

  it 'can access deleted association' do
    expect(variant.product).not_to be_nil
  end

  it 'can access deleted association when includes' do 
    deleted_product = Variant.all.includes(:product).first.product
    expect(deleted_product).not_to be_nil
  end
end
