require 'rails_helper'

RSpec.describe Admin::RefundRequestsController, type: :controller do 
  before do 
    request.headers['Accept'] = 'application/json'
  end

  let(:current_user) { create(:user, :admin_role) }

  before(:each) do
    allow(controller).to receive(:authenticate_user!).and_return(true)
    allow(controller).to receive(:current_user).and_return(current_user)
  end

  describe "#accept" do
    it 'kiem tra ket qua tra ve khi da accept rui' do
      refund_request = create(:refund_request, state: NhtRequest::ACCEPTED)
      put :accept, { id: refund_request.id }
      expect(response.body).to eq({errors: 'Da xet duyet hanh dong nay'}.to_json)
    end
  end


  describe '#index' do 
    context 'admin user' do
      let(:current_user) { create(:user, :admin_role) }

      it 'admin co the duyet danh sach refund' do 
        expect { get :index }.not_to raise_error(CanCan::AccessDenied)
      end
    end

    context 'other users' do
      let(:current_user) { create(:user, :subadmin_role) }

      it 'ngoai admin khong ai truy cap duoc danh sach refund can duyet' do 
        expect { get :index }.to raise_error(CanCan::AccessDenied)
      end

    end

  end
end
