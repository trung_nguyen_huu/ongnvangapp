require 'rails_helper'

RSpec.describe Admin::CustomersController, type: :controller do 
  let(:current_user) { create(:user, :admin_role) }

  before(:each) do
    allow(controller).to receive(:authenticate_user!).and_return(true)
    allow(controller).to receive(:current_user).and_return(current_user)
  end

  describe 'index' do
    let!(:customer_1) { create(:customer,
                               name: 'my customer',
                               order_seller: current_user) }
    let!(:customer_2) { create(:customer,
                               name: 'customer created by me',
                               creator: current_user) }
    let!(:customer_3) { create(:customer) }

    context 'Im seller' do
      let(:current_user) { create(:user, :seller_role) }

      it 'can get only my customer' do 
        get :index, format: :json
        customer_names = assigns(:customers).map(&:name)
        expect(customer_names).to match_array([customer_1.name, customer_2.name])
      end
    end

    context 'Im subadmin' do
      let(:current_user) { create(:user, :subadmin_role) }

      it 'can get all customer' do 
        get :index, format: :json
        customer_names = assigns(:customers).map(&:name)
        expect(customer_names).to match_array([customer_1.name, customer_2.name, customer_3.name])
      end
    end

    context 'Im admin' do
      let(:current_user) { create(:user, :admin_role) }

      it 'can get all customer' do 
        get :index, format: :json
        customer_names = assigns(:customers).map(&:name)
        expect(customer_names).to match_array([customer_1.name, customer_2.name, customer_3.name])
      end
    end
  end
end
