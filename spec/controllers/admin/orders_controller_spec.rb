require 'rails_helper'

RSpec.describe Admin::OrdersController, type: :controller do 
  describe "#refund" do
    let(:current_user) { create(:user, :admin_role) }

    before(:each) do
      allow(controller).to receive(:authenticate_user!).and_return(true)
      allow(controller).to receive(:current_user).and_return(current_user)
    end

    let(:order) { create(:order) }

    before(:each) do
      allow(Order).to receive(:friendly).and_return(Order)
      allow(Order).to receive(:find).and_return(order)
    end

    context 'neu user la admin' do
      let(:current_user) { create(:user, :admin_role) }

      it 'thi goi refund truc tiep' do
        allow(order).to receive(:refund!)
        put :refund,
          id: order.id,
          order: { refund_date: Time.now },
          format: :json

        expect(order).to have_received(:refund!)
      end
    end

    context 'neu user la subadmin' do
      let(:current_user) { create(:user, :subadmin_role) }

      it 'thi tao refund request to admin duyet' do
        allow(order).to receive(:request_refund!)
        put :refund,
          id: order.id,
          order: { refund_date: Time.now },
          format: :json

        expect(order).to have_received(:request_refund!)
      end
    end
  end
end
