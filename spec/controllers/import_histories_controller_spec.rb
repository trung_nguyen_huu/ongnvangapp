require 'rails_helper'

RSpec.describe Admin::ImportHistoriesController do 
  describe '#export_prices' do
    let!(:user) { create(:user, :admin_role) }

    let(:import_history) { 
      build_stubbed(:import_history)
    }

    before(:each) do
      allow(controller).to receive(:current_user).and_return(user)
      allow(controller).to receive(:authenticate_user!).and_return(true)
    end

    it 'must call export prices on import history' do
      allow(ImportHistory).to receive(:find).and_return(import_history)
      allow(import_history).to receive(:export_price_to_variants).and_return(true)
      
      put :export_prices, id: import_history.id, format: 'json'

      expect(assigns(:import_history)).to have_received(:export_price_to_variants)
    end
  end
end
