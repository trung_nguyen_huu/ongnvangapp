// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//= require backend/assets-minified/js-core.js
//= require backend/assets-minified/widgets/dropdown/dropdown.js
//= require backend/assets-minified/widgets/tooltip/tooltip.js
//= require backend/assets-minified/widgets/popover/popover.js
//= require backend/assets-minified/widgets/button/button.js
//= require backend/assets-minified/widgets/collapse/collapse.js
//= require backend/assets-minified/widgets/uniform/uniform.js
//= require backend/assets-minified/widgets/superclick/superclick.js
//= require backend/assets-minified/widgets/nicescroll/nicescroll.js
//= require backend/assets-minified/widgets/autocomplete/autocomplete.js
//= require backend/assets-minified/widgets/autocomplete/menu.js
//= require backend/assets-minified/widgets/tabs/tabs.js
//= require backend/assets-minified/widgets/slidebars/slidebars.js
//= require backend/assets-minified/widgets-init.js
//= require backend/assets-minified/themes/supina/js/layout.js
//= require backend/assets-minified/widgets/input-switch/inputswitch.js
//= require bootstrap/dist/js/bootstrap.js
//= require underscore/underscore-min.js
//= require angular
//= require angular-rails-templates
//= require angular-ui-router
//= require angular-bootstrap
//= require ui-select/dist/select.js
//= require angucomplete-alt/angucomplete-alt.js
//= require spin.js/spin.min.js
//= require angular-spinner.js
//= require angular-messages
//= require ngMask/dist/ngMask.min.js
//= require chosen/chosen.jquery.js
//= require angular-chosen-localytics/dist/angular-chosen.js
//= require angular-xeditable/dist/js/xeditable.js
//= require angular-animate/angular-animate.js
//= require angular-ui-notification/dist/angular-ui-notification.js
//= require angular-loading-bar/build/loading-bar.js
//= require AngularDevise/lib/devise-min.js
//= require highcharts-ng/dist/highcharts-ng.min.js
//= require backend/jquery.maskMoney.min.js
//= require backend/angular.maskMoney.js
//= require checklist-model/checklist-model.js
//= require date.js/build/date.js
//= require_tree .
//= require backend/application.js
//= require admin/application.js
