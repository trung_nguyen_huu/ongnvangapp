class CreateLineItems < ActiveRecord::Migration
  def change
    create_table :line_items do |t|
      t.references :order, index: true
      t.references :variant, index: true
      #so luong nguoi dung mua
      t.integer :quantity
      #track gia de tranh bi thay doi boi product khi chinh sua
      t.decimal :price, default: 0
      t.decimal :discount_amount, default: 0
      #gia goc noi suy tu export items
      t.decimal :origin_price_total, default: 0


      t.timestamps null: false
    end
  end
end
