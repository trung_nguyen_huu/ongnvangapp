class CreateNhtRequests < ActiveRecord::Migration
  def change
    # sudo su postgres -c "psql db_name -c 'CREATE EXTENSION hstore;'"
    
    create_table :nht_requests do |t|
      t.references :object, polymorphic: true, index: true
      t.string :state, default: 'waiting'

      t.string :action
      t.references :requester, index: true
      t.text :parameters

      t.timestamps null: false
    end
  end
end
