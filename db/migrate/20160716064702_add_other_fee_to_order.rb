class AddOtherFeeToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :other_fee_total, :decimal, default: 0
    add_column :orders, :buyer_pay_for_delivery_fee, :boolean, default: true
  end
end
