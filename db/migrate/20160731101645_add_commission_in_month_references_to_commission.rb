class AddCommissionInMonthReferencesToCommission < ActiveRecord::Migration
  def change
    add_reference :commissions, :commission_in_month, index: true
  end
end
