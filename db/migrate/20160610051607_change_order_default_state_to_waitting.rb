class ChangeOrderDefaultStateToWaitting < ActiveRecord::Migration
  def change
    change_column_default(:orders, :state, 'waiting')
  end
end
