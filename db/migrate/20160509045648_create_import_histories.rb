class CreateImportHistories < ActiveRecord::Migration
  def change
    create_table :import_histories do |t|
      t.decimal :delivery_fee_total, default: 0
      t.decimal :other_fee_total, default: 0

      #COLUMNS NOI SUY
      t.integer :number_of_items, default: 0
      t.decimal :cost_total, default: 0
      t.decimal :price_total, default: 0
      t.decimal :discount_total, default: 0
      t.decimal :debit_total, default: 0


      t.time :deleted_at
      t.timestamps null: false
    end
  end
end
