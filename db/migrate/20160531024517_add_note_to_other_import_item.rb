class AddNoteToOtherImportItem < ActiveRecord::Migration
  def change
    add_column :other_import_items, :note, :text, default: ''
  end
end
