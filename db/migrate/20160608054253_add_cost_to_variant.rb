class AddCostToVariant < ActiveRecord::Migration
  def change
    add_column :variants, :cost, :decimal, default: 0
  end
end
