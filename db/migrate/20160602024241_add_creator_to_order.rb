class AddCreatorToOrder < ActiveRecord::Migration
  def change
    add_reference :orders, :creator, index: true
  end
end
