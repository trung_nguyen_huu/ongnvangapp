class CreateOtherImportItems < ActiveRecord::Migration
  def change
    create_table :other_import_items do |t|
      t.string :name, default: ''
      t.decimal :cost, default: 0
      t.references :import_history, index: true

      t.time :deleted_at
      t.timestamps null: false
    end
  end
end
