class AddCreatorToImportHistory < ActiveRecord::Migration
  def change
    add_reference :import_histories, :creator, index: true
  end
end
