class AddDebitToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :debit_total, :decimal, default: 0
  end
end
