class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :number
      t.string :state, default: 'completed' #1. completed, delivered, :received, :refunded
      t.references :customer, index: true
      t.datetime :delivery_date
      t.datetime :receive_date
      t.datetime :refund_date

      t.decimal :total, default: 0
      t.decimal :item_total, default: 0
      t.decimal :discount_amount_total, default: 0
      t.decimal :delivery_fee_total, default: 0

      t.decimal :origin_price_total, default: 0

      t.timestamps null: false
    end
  end
end
