class RemoveNameFromOtherImportItem < ActiveRecord::Migration
  def change
    remove_column :other_import_items, :name, :string
  end
end
