class CreateImportItems < ActiveRecord::Migration
  def change
    create_table :import_items do |t|
      t.references :variant, index: true
      t.decimal :cost_per_item, default: 0
      t.decimal :discount_per_item, default: 0
      t.decimal :price_per_item, default: 0
      t.references :supplier, index: true
      t.references :import_history, index: true

      t.integer :quantity
      #so luong con trong lan nhap nay
      t.integer :count_in_stock, default: 0

      t.time :deleted_at
      t.timestamps null: false
    end
  end
end
