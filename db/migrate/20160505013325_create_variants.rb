class CreateVariants < ActiveRecord::Migration
  def change
    create_table :variants do |t|
      t.references :product, index: true
      t.string :sku, default: ''
      t.string :option_values, default: ''
      t.decimal :price, default: 0, null: false
      t.decimal :discount_amount, default: 0
      t.integer :count_in_stock, default: 0

      t.timestamps null: false

      t.time :deleted_at
    end
  end
end
