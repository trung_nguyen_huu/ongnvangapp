class AddTitleToImportHistory < ActiveRecord::Migration
  def change
    add_column :import_histories, :title, :string, default: ''
  end
end
