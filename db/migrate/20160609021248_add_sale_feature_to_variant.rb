class AddSaleFeatureToVariant < ActiveRecord::Migration
  def change
    add_column :variants, :sale_price, :decimal, default: 0
    add_column :variants, :sale_from_date, :date
    add_column :variants, :sale_to_date, :date
  end
end
