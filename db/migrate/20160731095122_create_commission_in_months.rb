class CreateCommissionInMonths < ActiveRecord::Migration
  def change
    create_table :commission_in_months do |t|
      t.decimal :ton_cuoi_ki, default: 0
      t.references :seller, index: true

      t.timestamps null: false
    end
  end
end
