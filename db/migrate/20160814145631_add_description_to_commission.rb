class AddDescriptionToCommission < ActiveRecord::Migration
  def change
    add_column :commissions, :description, :text, default: ''
  end
end
