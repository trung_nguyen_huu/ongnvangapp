class CreatePayForCommissions < ActiveRecord::Migration
  def change
    create_table :pay_for_commissions do |t|
      t.decimal :amount
      t.text :description
      t.text :note
      t.references :commission_in_month, index: true

      t.timestamps null: false
    end
  end
end
