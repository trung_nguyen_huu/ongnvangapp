class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :number, null: false, limit: 255
      t.string :name, limit: 255
      t.string :phone, limit: 255
      t.string :address, limit: 255
      t.date :birthday
      t.string :id_number, limit: 255
      t.string :email, limit: 255
      t.string :facebook, limit: 500
      t.string :job, limit: 255
      t.text :character, limit: 10000

      t.time :deleted_at
      t.timestamps null: false
    end

    add_index :customers, :number, unique: true
  end
end
