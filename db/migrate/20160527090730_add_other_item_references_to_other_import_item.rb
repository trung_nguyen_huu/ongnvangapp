class AddOtherItemReferencesToOtherImportItem < ActiveRecord::Migration
  def change
    add_reference :other_import_items, :other_item, index: true
  end
end
