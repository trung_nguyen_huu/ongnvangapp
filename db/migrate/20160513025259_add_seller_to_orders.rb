class AddSellerToOrders < ActiveRecord::Migration
  def change
    add_reference :orders, :seller, index: true
  end
end
