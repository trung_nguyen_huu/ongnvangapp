class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name, default: '', null: false
      t.string :description
      t.string :slug, null: false, uniq: true

      t.timestamps null: false

      t.time :deleted_at
    end
  end
end
