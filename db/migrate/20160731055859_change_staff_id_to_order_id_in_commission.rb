class ChangeStaffIdToOrderIdInCommission < ActiveRecord::Migration
  def change
    remove_column(:commissions, :staff_id)

    add_column(:commissions, :order_id, :integer)
    add_index(:commissions, :order_id)
  end
end
