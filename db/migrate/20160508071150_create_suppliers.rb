class CreateSuppliers < ActiveRecord::Migration
  def change
    create_table :suppliers do |t|
      t.string :name, null: false, default: ''
      t.string :coso
      t.string :address
      t.text :note

      t.string :slug, null: false, uniq: true
      t.timestamps null: false
    end
  end
end
