class CreateCommissions < ActiveRecord::Migration
  def change
    create_table :commissions do |t|
      t.references :staff, index: true
      t.decimal :amount, default: 0
      t.text :note

      t.time :deleted_at
      t.timestamps null: false
    end
  end
end
