class CreateExportItems < ActiveRecord::Migration
  def change
    create_table :export_items do |t|
      t.references :import_item, index: true
      t.references :line_item, index: true
      #So luong xuat 
      t.integer :quantity

      t.timestamps null: false
    end
  end
end
