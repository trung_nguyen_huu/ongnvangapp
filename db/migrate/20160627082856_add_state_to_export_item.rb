class AddStateToExportItem < ActiveRecord::Migration
  def change
    add_column :export_items, :state, :string, default: 'sold'
  end
end
