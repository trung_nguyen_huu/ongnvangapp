class AddDeletedAtToOtherItem < ActiveRecord::Migration
  def change
    add_column :other_items, :deleted_at, :time
  end
end
