# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160814145631) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "commission_in_months", force: :cascade do |t|
    t.decimal  "ton_cuoi_ki", default: 0.0
    t.integer  "seller_id"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "commission_in_months", ["seller_id"], name: "index_commission_in_months_on_seller_id", using: :btree

  create_table "commissions", force: :cascade do |t|
    t.decimal  "amount",                 default: 0.0
    t.text     "note"
    t.time     "deleted_at"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.integer  "order_id"
    t.integer  "commission_in_month_id"
    t.text     "description",            default: ""
  end

  add_index "commissions", ["commission_in_month_id"], name: "index_commissions_on_commission_in_month_id", using: :btree
  add_index "commissions", ["order_id"], name: "index_commissions_on_order_id", using: :btree

  create_table "customers", force: :cascade do |t|
    t.string   "number",     limit: 255, null: false
    t.string   "name",       limit: 255
    t.string   "phone",      limit: 255
    t.string   "address",    limit: 255
    t.date     "birthday"
    t.string   "id_number",  limit: 255
    t.string   "email",      limit: 255
    t.string   "facebook",   limit: 500
    t.string   "job",        limit: 255
    t.text     "character"
    t.time     "deleted_at"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "creator_id"
  end

  add_index "customers", ["creator_id"], name: "index_customers_on_creator_id", using: :btree
  add_index "customers", ["number"], name: "index_customers_on_number", unique: true, using: :btree

  create_table "export_items", force: :cascade do |t|
    t.integer  "import_item_id"
    t.integer  "line_item_id"
    t.integer  "quantity"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "state",          default: "sold"
  end

  add_index "export_items", ["import_item_id"], name: "index_export_items_on_import_item_id", using: :btree
  add_index "export_items", ["line_item_id"], name: "index_export_items_on_line_item_id", using: :btree

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "import_histories", force: :cascade do |t|
    t.decimal  "delivery_fee_total", default: 0.0
    t.decimal  "other_fee_total",    default: 0.0
    t.integer  "number_of_items",    default: 0
    t.decimal  "cost_total",         default: 0.0
    t.decimal  "price_total",        default: 0.0
    t.decimal  "discount_total",     default: 0.0
    t.decimal  "debit_total",        default: 0.0
    t.time     "deleted_at"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.string   "title",              default: ""
    t.integer  "creator_id"
  end

  add_index "import_histories", ["creator_id"], name: "index_import_histories_on_creator_id", using: :btree

  create_table "import_items", force: :cascade do |t|
    t.integer  "variant_id"
    t.decimal  "cost_per_item",     default: 0.0
    t.decimal  "discount_per_item", default: 0.0
    t.decimal  "price_per_item",    default: 0.0
    t.integer  "supplier_id"
    t.integer  "import_history_id"
    t.integer  "quantity"
    t.integer  "count_in_stock",    default: 0
    t.time     "deleted_at"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  add_index "import_items", ["import_history_id"], name: "index_import_items_on_import_history_id", using: :btree
  add_index "import_items", ["supplier_id"], name: "index_import_items_on_supplier_id", using: :btree
  add_index "import_items", ["variant_id"], name: "index_import_items_on_variant_id", using: :btree

  create_table "line_items", force: :cascade do |t|
    t.integer  "order_id"
    t.integer  "variant_id"
    t.integer  "quantity"
    t.decimal  "price",              default: 0.0
    t.decimal  "discount_amount",    default: 0.0
    t.decimal  "origin_price_total", default: 0.0
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.decimal  "sale_price",         default: 0.0
  end

  add_index "line_items", ["order_id"], name: "index_line_items_on_order_id", using: :btree
  add_index "line_items", ["variant_id"], name: "index_line_items_on_variant_id", using: :btree

  create_table "nht_requests", force: :cascade do |t|
    t.integer  "object_id"
    t.string   "object_type"
    t.string   "state",        default: "waiting"
    t.string   "action"
    t.integer  "requester_id"
    t.text     "parameters"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "nht_requests", ["object_type", "object_id"], name: "index_nht_requests_on_object_type_and_object_id", using: :btree
  add_index "nht_requests", ["requester_id"], name: "index_nht_requests_on_requester_id", using: :btree

  create_table "orders", force: :cascade do |t|
    t.string   "number"
    t.string   "state",                      default: "waiting"
    t.integer  "customer_id"
    t.datetime "delivery_date"
    t.datetime "receive_date"
    t.datetime "refund_date"
    t.decimal  "total",                      default: 0.0
    t.decimal  "item_total",                 default: 0.0
    t.decimal  "discount_amount_total",      default: 0.0
    t.decimal  "delivery_fee_total",         default: 0.0
    t.decimal  "origin_price_total",         default: 0.0
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
    t.integer  "seller_id"
    t.decimal  "debit_total",                default: 0.0
    t.integer  "creator_id"
    t.decimal  "other_fee_total",            default: 0.0
    t.boolean  "buyer_pay_for_delivery_fee", default: true
    t.text     "note",                       default: ""
  end

  add_index "orders", ["creator_id"], name: "index_orders_on_creator_id", using: :btree
  add_index "orders", ["customer_id"], name: "index_orders_on_customer_id", using: :btree
  add_index "orders", ["seller_id"], name: "index_orders_on_seller_id", using: :btree

  create_table "other_import_items", force: :cascade do |t|
    t.decimal  "cost",              default: 0.0
    t.integer  "import_history_id"
    t.time     "deleted_at"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.integer  "other_item_id"
    t.text     "note",              default: ""
  end

  add_index "other_import_items", ["import_history_id"], name: "index_other_import_items_on_import_history_id", using: :btree
  add_index "other_import_items", ["other_item_id"], name: "index_other_import_items_on_other_item_id", using: :btree

  create_table "other_items", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.time     "deleted_at"
  end

  create_table "pay_for_commissions", force: :cascade do |t|
    t.decimal  "amount"
    t.text     "description"
    t.text     "note"
    t.integer  "commission_in_month_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "pay_for_commissions", ["commission_in_month_id"], name: "index_pay_for_commissions_on_commission_in_month_id", using: :btree

  create_table "phones", force: :cascade do |t|
    t.string   "number",         null: false
    t.integer  "phoneable_id"
    t.string   "phoneable_type"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "phones", ["phoneable_type", "phoneable_id"], name: "index_phones_on_phoneable_type_and_phoneable_id", using: :btree

  create_table "products", force: :cascade do |t|
    t.string   "name",        default: "", null: false
    t.string   "description"
    t.string   "slug",                     null: false
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.time     "deleted_at"
  end

  create_table "products_suppliers", id: false, force: :cascade do |t|
    t.integer "product_id",  null: false
    t.integer "supplier_id", null: false
  end

  add_index "products_suppliers", ["product_id", "supplier_id"], name: "index_products_suppliers_on_product_id_and_supplier_id", using: :btree
  add_index "products_suppliers", ["supplier_id", "product_id"], name: "index_products_suppliers_on_supplier_id_and_product_id", using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles_users", id: false, force: :cascade do |t|
    t.integer "role_id", null: false
    t.integer "user_id", null: false
  end

  add_index "roles_users", ["role_id", "user_id"], name: "index_roles_users_on_role_id_and_user_id", using: :btree
  add_index "roles_users", ["user_id", "role_id"], name: "index_roles_users_on_user_id_and_role_id", using: :btree

  create_table "suppliers", force: :cascade do |t|
    t.string   "name",       default: "", null: false
    t.string   "coso"
    t.string   "address"
    t.text     "note"
    t.string   "slug",                    null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name",                   default: ""
    t.time     "deleted_at"
    t.text     "note",                   default: ""
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "variants", force: :cascade do |t|
    t.integer  "product_id"
    t.string   "sku",             default: ""
    t.string   "option_values",   default: ""
    t.decimal  "price",           default: 0.0, null: false
    t.decimal  "discount_amount", default: 0.0
    t.integer  "count_in_stock",  default: 0
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.time     "deleted_at"
    t.decimal  "cost",            default: 0.0
    t.decimal  "sale_price",      default: 0.0
    t.date     "sale_from_date"
    t.date     "sale_to_date"
  end

  add_index "variants", ["product_id"], name: "index_variants_on_product_id", using: :btree

end
